<?php


namespace Zen\IgrooveBundle\Exception;

use IMAG\LdapBundle\User\LdapUserInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Throwable;

class NeedPasswordChangeAuthenticationException extends AuthenticationException {

    protected $ldapUser;

    public function __construct(LdapUserInterface $ldapUser, string $message = "", int $code = 0, Throwable $previous = NULL) {
        $this->ldapUser = $ldapUser;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return LdapUserInterface
     */
    public function getLdapUser(): LdapUserInterface {
        return $this->ldapUser;
    }

}