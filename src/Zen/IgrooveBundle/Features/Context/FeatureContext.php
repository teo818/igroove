<?php

namespace Zen\IgrooveBundle\Features\Context;

use Symfony\Component\HttpKernel\KernelInterface;
use Behat\Mink\Exception\UnsupportedDriverActionException;
use Behat\Mink\Driver\Selenium2Driver;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Process\Process;
use adLDAP\adLDAP;
use Zen\IgrooveBundle\Manager\ConfigurationManager;

//
// Require 3rd-party libraries here:
//
require_once 'PHPUnit/Autoload.php';
require_once 'PHPUnit/Framework/Assert/Functions.php';
//

/**
 * Feature context.
 */
class FeatureContext extends MinkContext implements KernelAwareContext
{
    private $kernel;
    private $parameters;
    private $dumpBefore;
    private $configurationManager;
    private $configurationAD;


    /**
     * adLDAP manager
     *
     * @var adLDAP
     */
    protected $adldap;

    /**
     * Entity manager
     *
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * Initializes context with parameters from behat.yml.
     *
     */
    public function __construct()
    {
    }

    /**
     * Sets HttpKernel instance.
     * This method will be automatically called by Symfony2Extension ContextInitializer.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
        $this->em = $this->kernel->getContainer()->get('doctrine.orm.entity_manager');
        $this->configurationManager = $this->kernel->getContainer()->get('zen.igroove.configuration');

        $this->configurationAD = $this->configurationManager->getActiveDirectoryConfiguration();
        $this->adldapManager = $this->kernel->getContainer()->get('zen.igroove.adldap');
        $this->adldap =$this->adldapManager->getAdLdap();

    }

    /**
     * Returns the Doctrine entity manager.
     *
     * @return Doctrine\ORM\EntityManager
     */
    protected function getManager()
    {
        return $this->kernel->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * Take screenshot when step fails.
     * Works only with Selenium2Driver.
     *
     * @AfterStep
     */
    public function takeScreenshotAfterFailedStep($event)
    {
        /*      if (4 === $event->getResult()) {
                  $driver = $this->getSession()->getDriver();
                  if (!($driver instanceof Selenium2Driver)) {
                      throw new UnsupportedDriverActionException('Taking screenshots is not supported by %s, use Selenium2Driver instead.', $driver);
                  }
                  $directory = 'build/behat/' . $event->getLogicalParent()->getFeature()->getTitle();
                  if (!is_dir($directory)) {
                      mkdir($directory, 0777, true);
                  }
                  $filename = sprintf('%s_%s_%s_%s.%s', $event->getLogicalParent()->getTitle(), $this->browserName, date('YmdHis'), uniqid('', true), 'png');
                  file_put_contents($directory . '/' . $filename, $driver->getScreenshot());
              }
        */
    }

    /**
     * Checks that the specified table's  match the given data
     *
     * @Then /^data of "([^"]*)" table should match:$/
     */
    public function dataOfTableShouldMatch($element, TableNode $table)
    {
        $rows = $this->getSession()->getPage()->findById($element)->findAll('css', 'table tbody tr');

        $values = array();
        $j = 0;
        foreach ($rows as $row) {
            $cols = $row->findAll('css', 'td');
            $i = 0;
            foreach ($cols as $col) {
                $values[$j][$i] = $cols[$i]->getText();
                $i++;
            }
            $j++;
        }
        $rows = $table->getRows();
        $j = 0;
        $tableMatrix = array();
        foreach ($rows as $i => $row) {
            $i = 0;
            foreach ($row as $cell) {
                $tableMatrix[$j][$i] = $cell;
                $i++;
            }
            $j++;
        }
        $j = 0;
        foreach ($values as $row) {
            $i = 0;
            foreach ($row as $cell) {
                assertEquals($cell, @$tableMatrix[$j][$i], sprintf(
                    'Expecting to see %s actually saw %s', (@$tableMatrix[$j][$i]) ? (@$tableMatrix[$j][$i]) : 'nothing', $cell));
                $i++;
            }
            $j++;
        }
    }

    /**
     * @Given /^I am logged as user "([^"]*)" with password "([^"]*)"$/
     */
    public function iAmLoggedAsUserWithPassword($username, $password)
    {

        $this->visit("/");
        $this->fillField("username", $username);
        $this->fillField("password", $password);
        $this->pressButton("Accedi");
    }

    /**
     * @Then /^I should see the link "(?P<link>[^"]*)"$/
     */
    public function assertLinkVisible($link)
    {
        $element = $this->getSession()->getPage();
        $result = $element->findLink($link);
        if (empty($result)) {
            throw new \Exception(sprintf("No link to '%s' on the page %s", $link, $this->getSession()->getCurrentUrl()));
        }
    }

    /**
     * @Then /^I should not see the link "(?P<link>[^"]*)"$/
     */
    public function assertNotLinkVisible($link)
    {
        $element = $this->getSession()->getPage();
        $result = $element->findLink($link);
        if ($result) {
            throw new \Exception(sprintf("The link '%s' was present on the page %s and was not supposed to be", $link, $this->getSession()->getCurrentUrl()));
        }
    }

    /**
     * @Given /^focus out "([^"]*)"$/
     */
    public function focusOut($field)
    {
        $session = $this->getMink()->getSession();
        $field = $this->fixStepArgument($field);
        $selector = new \Behat\Mink\Selector\NamedSelector();
        $xpath = $selector->translateToXPath(array('field', $field));
        $this->getSession()->getDriver()->blur($xpath);
    }

    /**
     * @Given /^I wait for (\d+) seconds$/
     */
    public function iWaitForSeconds($seconds)
    {
        $this->getSession()->wait($seconds * 1000);
    }

    /**
     * @Given /^my world is "([^"]*)"$/
     */
    public function myWorldIs($configuration)
    {
        $path = $this->kernel->getRootDir() . '/../src/Zen/IgrooveBundle/Features/DataFixtures';
        $this->dumpBefore = $path . '/' . time() . '.sql';
        $dump = $path . '/world_' . $configuration . '.sql';
        if (!file_exists($dump)) {
            throw  new \Exception('Unable to find world ' . $configuration);
        }
        $this->dumpDB($this->dumpBefore);
        $this->restoreDB($dump);
        $d = dir($path);
        while (false !== ($entry = $d->read())) {
            if (($entry == '.') OR ($entry == '..')) {
                continue;
            }
            if (substr($entry, 0, strlen('world_')) != 'world_') {
                unlink($path . '/' . $entry);
            }
        }
        $d->close();

    }

    private function dumpDB($filename)
    {

        $host = $this->kernel->getContainer()->getParameter('database_host');
        $port = $this->kernel->getContainer()->getParameter('database_port');
        $database = $this->kernel->getContainer()->getParameter('database_name');
        $user = $this->kernel->getContainer()->getParameter('database_user');
        $password = $this->kernel->getContainer()->getParameter('database_password');

        $command = sprintf("mysqldump --host='%s' --port='%d' --user='%s' --password='%s' %s > %s", $host, $port, $user, $password, $database, $filename);
        $process = new Process(null);
        $process->setCommandLine($command);
        $process->start();
        $process->wait();
    }

    private function restoreDB($filename)
    {

        $host = $this->kernel->getContainer()->getParameter('database_host');
        $port = $this->kernel->getContainer()->getParameter('database_port');
        $database = $this->kernel->getContainer()->getParameter('database_name');
        $user = $this->kernel->getContainer()->getParameter('database_user');
        $password = $this->kernel->getContainer()->getParameter('database_password');

        $command = sprintf("mysql --host='%s' --port='%d' --user='%s' --password='%s' %s < %s", $host, $port, $user, $password, $database, $filename);
        $process = new Process(null);
        $process->setCommandLine($command);
        $process->start();
        $process->wait();
    }


    /**
     * Remove terms that we probably created. Nodes
     * are handled because when a user is deleted their content
     * is deleted as well. This not true for terms
     * that they create though.
     *
     * @AfterScenario
     */
    public function cleanupDB()
    {

        if (file_exists($this->dumpBefore)) {
            $this->restoreDB($this->dumpBefore);
            unlink($this->dumpBefore);
        }

    }


    /**
     * @Given /^my active directory is "([^"]*)"$/
     */
    public function myActiveDirectoryIs($world)
    {
        if ($world == 'empty') {
            $users = $this->adldap->user()->all(
                array("samaccountname", 'description')
            );
            foreach ($users as $user) {
                $i=$this->adldap->user()->info($user['samaccountname'],'*');
                var_dump($i);die;
                if ($user['description'] == 'Creato da igroove') {
                    echo "Delete: ".$user['samaccountname'];
                    $this->adldap->user()->delete($user['samaccountname']);
                }
            }
        } else {

            $path = $this->kernel->getRootDir() . '/../src/Zen/IgrooveBundle/Features/DataFixtures';
            $dump = $path . '/world_' . $world . '.csv';
            if (!file_exists($dump)) {
                throw  new \Exception('Unable to find world ' . $world);
            }


            foreach ($dump as $row) {
                $attributes = array(
                    "username" => strtolower($student->getUsername()),
                    "firstname" => trim($student->getFirstname()),
                    "logon_name" => strtolower($student->getUsername()) . $config['account_suffix'],
                    "surname" => trim($student->getLastName()),
                    "description" => "Creato da igroove",
                    "department" => strtoupper($student->getDistinguishedId()),
                    "enabled" => 1,
                    "password" => $student->getStartingPassword(),
                    "container" => array(),
                    "home_drive" => strtoupper($config['home_drive'] . ':'),
                    "home_directory" => $home_directory,
                    "change_password" => 0
                );
                $result = $adldap->user()->create($attributes);
            }
        }
    }

    /**
     * @Given /^I run app "([^"]*)"$/
     */
    public function iRunApp($app)
    {

        throw new \Exception('To do!');
    }


    /**
     * Checks that the specified table's  match the given data
     *
     * @Then /^I have these users in active directory:$/
     */
    public function iHaveTheseUsersInActiveDirectory(TableNode $table)
    {
        $rows = $table->getRows();
        foreach ($rows as $i => $row) {
            print_r($row);
        }
        throw new \Exception('To do!');

    }

    /**
     * Checks that the specified table's  match the given data
     *
     * @Then /^I have these groups in active directory:$/
     */
    public function iHaveTheseGroupsInActiveDirectory(TableNode $table)
    {
        $rows = $table->getRows();
        foreach ($rows as $i => $row) {
            print_r($row);
        }

        throw new \Exception('To do!');
    }


    /**
     * Checks that the specified table's  match the given data
     *
     * @Then /^I have these memberships in active directory:$/
     */
    public function iHaveTheseMembershipsInActiveDirectory(TableNode $table)
    {
        $rows = $table->getRows();
        foreach ($rows as $i => $row) {
            print_r($row);
        }

        throw new \Exception('To do!');
    }


    /**
     * Checks that the specified table's  match the given data
     *
     * @Then /^I don't have these memberships in active directory:$/
     */
    public function iDonTHaveTheseMembershipsInActiveDirectory(TableNode $table)
    {
        $rows = $table->getRows();
        foreach ($rows as $i => $row) {
            print_r($row);
        }
        throw new \Exception('To do!');
    }


}
