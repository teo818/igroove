-- MySQL dump 10.13  Distrib 5.5.37, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: igroove
-- ------------------------------------------------------
-- Server version	5.5.37-0+wheezy1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Configuration`
--

DROP TABLE IF EXISTS `Configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Configuration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Configuration`
--

LOCK TABLES `Configuration` WRITE;
/*!40000 ALTER TABLE `Configuration` DISABLE KEYS */;
INSERT INTO `Configuration` VALUES (37,'onlyReception','1','2014-09-23 12:22:51','2014-09-23 11:56:46'),(49,'homework','',NULL,'2014-09-23 12:22:51'),(50,'userCanChangeOwnPassword','',NULL,'2014-09-23 12:22:51'),(51,'teacherCanResetPassword','',NULL,'2014-09-23 12:22:51'),(52,'teacherCanSeeList','',NULL,'2014-09-23 12:22:51'),(53,'consegna_folder',NULL,NULL,'2014-09-23 12:22:51'),(54,'consegna_email',NULL,NULL,'2014-09-23 12:22:51'),(55,'consegna_password',NULL,NULL,'2014-09-23 12:22:51'),(56,'consegna_imap_host',NULL,NULL,'2014-09-23 12:22:51'),(57,'consegna_imap_ssl','',NULL,'2014-09-23 12:22:51'),(58,'dizda_cloud_backup_username',NULL,NULL,'2014-09-23 12:22:51'),(59,'dizda_cloud_backup_password',NULL,NULL,'2014-09-23 12:22:51');
/*!40000 ALTER TABLE `Configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ConfigurationStorage`
--

DROP TABLE IF EXISTS `ConfigurationStorage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ConfigurationStorage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ConfigurationStorage`
--

LOCK TABLES `ConfigurationStorage` WRITE;
/*!40000 ALTER TABLE `ConfigurationStorage` DISABLE KEYS */;
INSERT INTO `ConfigurationStorage` VALUES (26,'onlyReception',NULL,NULL,'2014-10-03 15:19:42'),(27,'homework',NULL,NULL,'2014-10-03 15:19:42'),(28,'userCanChangeOwnPassword','1',NULL,'2014-10-03 15:19:42'),(29,'teacherCanResetPassword','1',NULL,'2014-10-03 15:19:42'),(30,'teacherCanSeeList','1',NULL,'2014-10-03 15:19:42'),(31,'showBadge','1',NULL,'2014-10-03 15:19:42'),(32,'consegna_folder',NULL,NULL,'2014-10-03 15:19:42'),(33,'consegna_email',NULL,NULL,'2014-10-03 15:19:42'),(34,'consegna_password',NULL,NULL,'2014-10-03 15:19:42'),(35,'consegna_imap_host',NULL,NULL,'2014-10-03 15:19:42'),(36,'consegna_imap_ssl',NULL,NULL,'2014-10-03 15:19:42'),(37,'dizda_cloud_backup_username',NULL,NULL,'2014-10-03 15:19:42'),(38,'dizda_cloud_backup_password',NULL,NULL,'2014-10-03 15:19:42'),(39,'active_directory_account_suffix','test',NULL,'2014-10-03 15:19:42'),(40,'active_directory_base_dn','CN=Users,DC=test,DC=network',NULL,'2014-10-03 15:19:42'),(41,'active_directory_domain_controller','10.10.0.8',NULL,'2014-10-03 15:19:42'),(42,'active_directory_admin_username','administrator',NULL,'2014-10-03 15:19:42'),(43,'active_directory_admin_password','password',NULL,'2014-10-03 15:19:42'),(44,'active_directory_use_ssl','1',NULL,'2014-10-03 15:19:42'),(45,'active_directory_generated_group_prefix','_',NULL,'2014-10-03 15:19:42'),(46,'active_directory_home_folder','\\\\10.10.0.8\\home\\',NULL,'2014-10-03 15:19:42'),(47,'active_directory_home_drive','H',NULL,'2014-10-03 15:19:42'),(48,'active_directory_password_min_char','8',NULL,'2014-10-03 15:19:42'),(49,'active_directory_password_complexity','1',NULL,'2014-10-03 15:19:42'),(50,'active_directory_username_style','NOME.COGNOME',NULL,'2014-10-03 15:19:42');
/*!40000 ALTER TABLE `ConfigurationStorage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Cron`
--

DROP TABLE IF EXISTS `Cron`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Cron` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `script` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latestRun` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Cron`
--

LOCK TABLES `Cron` WRITE;
/*!40000 ALTER TABLE `Cron` DISABLE KEYS */;
/*!40000 ALTER TABLE `Cron` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GroupComposition`
--

DROP TABLE IF EXISTS `GroupComposition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GroupComposition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `imported` tinyint(1) DEFAULT NULL,
  `rootGroup` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subGroup` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_295A2627CB944F1A` (`student_id`),
  CONSTRAINT `FK_295A2627CB944F1A` FOREIGN KEY (`student_id`) REFERENCES `Student` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20302 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GroupComposition`
--

LOCK TABLES `GroupComposition` WRITE;
/*!40000 ALTER TABLE `GroupComposition` DISABLE KEYS */;
/*!40000 ALTER TABLE `GroupComposition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Guest`
--

DROP TABLE IF EXISTS `Guest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Guest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `identified_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `starting_from` date DEFAULT NULL,
  `ending_to` date DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Guest`
--

LOCK TABLES `Guest` WRITE;
/*!40000 ALTER TABLE `Guest` DISABLE KEYS */;
INSERT INTO `Guest` VALUES (129,'Paolino','Qui','1500','Paperino','2013-01-01','2013-02-03',NULL,'2014-09-30 18:10:24'),(130,'Paolino','Quo','4994','Paperino','2013-01-01','2013-02-03',NULL,'2014-09-30 18:10:24'),(131,'Paolino','Qua','5828','Paperino','2013-01-01','2013-02-03',NULL,'2014-09-30 18:10:24'),(132,'Dog','Pluto','7543','Pippo','2013-01-01','2013-02-03',NULL,'2014-09-30 18:10:24');
/*!40000 ALTER TABLE `Guest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Homework`
--

DROP TABLE IF EXISTS `Homework`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Homework` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `teacherUsername` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rootGroup` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subGroup` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `startingDate` datetime NOT NULL,
  `closingDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Homework`
--

LOCK TABLES `Homework` WRITE;
/*!40000 ALTER TABLE `Homework` DISABLE KEYS */;
/*!40000 ALTER TABLE `Homework` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `InternetOpen`
--

DROP TABLE IF EXISTS `InternetOpen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `InternetOpen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `account` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `close_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `InternetOpen`
--

LOCK TABLES `InternetOpen` WRITE;
/*!40000 ALTER TABLE `InternetOpen` DISABLE KEYS */;
/*!40000 ALTER TABLE `InternetOpen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Mac`
--

DROP TABLE IF EXISTS `Mac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Mac` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `device` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mac` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `starting_from` date NOT NULL,
  `ending_to` date NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Mac`
--

LOCK TABLES `Mac` WRITE;
/*!40000 ALTER TABLE `Mac` DISABLE KEYS */;
INSERT INTO `Mac` VALUES (130,'Rabbit','Jack','iPad','01:02:03:04:05:06','2014-01-10','2014-02-10',NULL,'2014-09-30 18:10:24'),(131,'Long','Jack','iPad3','01:02:03:04:05:07','2014-01-10','2014-02-11',NULL,'2014-09-30 18:10:24'),(132,'GrayCat','Jack','iPad','01:02:03:04:05:08','2014-01-10','2015-02-10',NULL,'2014-09-30 18:10:24'),(133,'WhiteCow','Jack','iPad','01:02:03:04:05:0A','2013-01-10','2013-02-10',NULL,'2014-09-30 18:10:24');
/*!40000 ALTER TABLE `Mac` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Mikrotik`
--

DROP TABLE IF EXISTS `Mikrotik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Mikrotik` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apiUsername` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apiPassword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Mikrotik`
--

LOCK TABLES `Mikrotik` WRITE;
/*!40000 ALTER TABLE `Mikrotik` DISABLE KEYS */;
/*!40000 ALTER TABLE `Mikrotik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Provider`
--

DROP TABLE IF EXISTS `Provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Provider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jsonKey` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `autoCreateUsername` tinyint(1) DEFAULT NULL,
  `forceImportedPassword` tinyint(1) DEFAULT NULL,
  `tutorEmailInList` tinyint(1) DEFAULT NULL,
  `consegna` tinyint(1) DEFAULT NULL,
  `googleAppClientId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `googleAppClientSecret` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `internetTeachersControl` tinyint(1) DEFAULT NULL,
  `internetOpenAccessRange` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `googleAppEmailForError` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `googleAppDomain` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Provider`
--

LOCK TABLES `Provider` WRITE;
/*!40000 ALTER TABLE `Provider` DISABLE KEYS */;
INSERT INTO `Provider` VALUES (3,'Prima formazione','http://10.10.0.8/jsonIgrooveAstori','lkhjglkh78435vlsd',1,0,1,0,NULL,NULL,1,NULL,1,NULL,'2014-10-03 15:20:18',NULL,NULL);
/*!40000 ALTER TABLE `Provider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Student`
--

DROP TABLE IF EXISTS `Student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imported` tinyint(1) DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `distinguished_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `starting_password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2875 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Student`
--

LOCK TABLES `Student` WRITE;
/*!40000 ALTER TABLE `Student` DISABLE KEYS */;
/*!40000 ALTER TABLE `Student` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-10-03 15:20:54
