<?php

namespace Zen\IgrooveBundle\Tests\Manager;

use Psr\Log\NullLogger;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Zen\IgrooveBundle\Manager\GoogleApps;

class GoogleAppsTest extends KernelTestCase {

    protected static $testDomain = "domain.test";
    protected static $testGroupName = "Test Group";
    protected static $testGroupNameCleaned = "testgroup";
    protected static $testOuName = "Test Ou";
    protected static $testOuNameCleaned = "testou";

    public static function setUpBeforeClass() {
        self::bootKernel();
    }

    protected function getMockedGoogleApps($googleServiceMocked) {
        $googleApps = new GoogleApps(self::$testDomain, "", "", "/test/", "/tmp/", new NullLogger());
        $this->setVariablesOnClass($googleApps, ['service' => $googleServiceMocked]);
        return $googleApps;
    }

    protected function setVariablesOnClass($class, $variables=[]) {
        $classReflec = new \ReflectionClass($class);

        foreach ($variables as $k => $v) {
            $classProp = $classReflec->getProperty($k);
            $classProp->setAccessible(true);
            $classProp->setValue($class, $v);
        }

        return $class;
    }

    protected function getTestUser($number = 1) {
        $user = new \Google_Service_Directory_User();
        $nameInstance = new \Google_Service_Directory_UserName();
        $nameInstance->setGivenName("Test");
        $nameInstance->setFamilyName("User {$number}");
        $user->setName($nameInstance);
        $user->setHashFunction("MD5");
        $user->setPrimaryEmail("test{$number}@".self::$testDomain);
        $user->setPassword(hash("md5", "Az-12345"));
        $user->setChangePasswordAtNextLogin(false);
        return $user;
    }

    protected function getTestGroup() {
        $group = new \Google_Service_Directory_Group();
        $group->setName(self::$testGroupName);
        $group->setEmail(self::$testGroupNameCleaned."@".self::$testDomain);
        $group->setKind("Anyone");
        return $group;
    }

    public function testUserExists() {
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->users = $this->createPartialMock(\Google_Service_Directory_Users_Resource::class,['get']);
        $googleServiceMocked->users->expects($this->once())->method('get')->with("test@domain.test")->willReturn(new \Google_Service_Directory_User());

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $this->assertTrue($googleApps->userExists("test"));
    }

    public function testUserExistsFail() {
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->users = $this->createPartialMock(\Google_Service_Directory_Users_Resource::class,['get']);
        $googleServiceMocked->users->expects($this->once())->method('get')->with("test@domain.test")->willThrowException(new \Google_Service_Exception(''));

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $this->assertFalse($googleApps->userExists("test"));

        $this->expectException(\Exception::class);

        $googleApps->userExists("");
    }

    public function testGetUsersList() {
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->users = $this->createPartialMock(\Google_Service_Directory_Users_Resource::class,['listUsers']);

        $firstAnswer = new \Google_Service_Directory_Users();
        $firstAnswer->setNextPageToken("lkLKJl65kjfdkj2lkj4328lkj");
        $firstAnswer->setUsers([$this->getTestUser(1), $this->getTestUser(2)]);

        $secondAnswer = new \Google_Service_Directory_Users();
        $secondAnswer->setNextPageToken("");
        $secondAnswer->setUsers([$this->getTestUser(3)]);

        $googleServiceMocked->users->expects($this->atLeastOnce())->method('listUsers')
            ->withConsecutive([['maxResults' => 500, 'domain' => self::$testDomain, 'pageToken' => ""]],[['maxResults' => 500, 'domain' => self::$testDomain, 'pageToken' => "lkLKJl65kjfdkj2lkj4328lkj"]])
            ->willReturnOnConsecutiveCalls($firstAnswer, $secondAnswer);

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $users = $googleApps->getUsersList();

        $this->assertTrue(is_array($users));
        $this->assertArraySubset(["test1@".self::$testDomain => $this->getTestUser(1), "test3@".self::$testDomain => $this->getTestUser(3)], $users);
    }

    public function testGetUsersListFail() {
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->users = $this->createPartialMock(\Google_Service_Directory_Users_Resource::class,['listUsers']);
        $googleServiceMocked->users->expects($this->once())->method('listUsers')
            ->with(['maxResults' => 500, 'domain' => self::$testDomain, 'pageToken' => ""])
            ->willThrowException(new \Google_Service_Exception(''));
        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $users = $googleApps->getUsersList();

        $this->assertTrue(is_array($users));
        $this->assertEmpty($users);
    }

    public function testGetUsersListCached() {
        $testData = ["test"];
        $googleApps = $this->createPartialMock(GoogleApps::class, ["getUsersList"]);
        $googleApps->expects($this->exactly(2))->method("getUsersList")->willReturn($testData);

        $users = $googleApps->getUsersListCached();
        $this->assertEquals($testData, $users);

        $users = $googleApps->getUsersListCached();
        $this->assertEquals($testData, $users);

        $users = $googleApps->getUsersListCached(true);
        $this->assertEquals($testData, $users);
    }

    public function testEmailInDomain() {
        $googleApps = $this->getMockedGoogleApps(null);
        $this->assertTrue($googleApps->emailInDomain("test@".self::$testDomain));
        $this->assertFalse($googleApps->emailInDomain("test@fake.domain"));
    }


    public function testCreateUser() {
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->users = $this->createPartialMock(\Google_Service_Directory_Users_Resource::class,['insert', 'get']);
        $googleServiceMocked->users->expects($this->once())->method('insert')->with($this->getTestUser(1));
        $googleServiceMocked->users->expects($this->once())->method('get')->with("test1@domain.test")->willReturn(null);

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $googleApps->createUser("test1@".self::$testDomain, "Test", "User 1", "Az-12345");
    }

    public function testCreateUserFail() {
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->users = $this->createPartialMock(\Google_Service_Directory_Users_Resource::class,['insert', 'get']);
        $googleServiceMocked->users->expects($this->once())->method('insert')->with($this->getTestUser(1))->willThrowException(new \Google_Service_Exception(''));
        $googleServiceMocked->users->expects($this->once())->method('get')->with("test1@domain.test")->willReturn(null);

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $this->expectException(\Exception::class);

        $googleApps->createUser("test1@".self::$testDomain, "Test", "User 1", "Az-12345");
    }

    public function testCreateUserFailEmpty() {
        $googleApps = $this->getMockedGoogleApps(null);

        $this->expectException(\Exception::class);

        $googleApps->createUser("", "", "");
    }

    public function testRenameUser() {
        $testUser1 = $this->getTestUser(1);
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->users = $this->createPartialMock(\Google_Service_Directory_Users_Resource::class,['update', 'get']);
        $googleServiceMocked->users->expects($this->once())->method('update')->with("test1@".self::$testDomain, $testUser1);
        $googleServiceMocked->users->expects($this->once())->method('get')->with("test1@".self::$testDomain)->willReturn($testUser1);

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $googleApps->renameUser("test2@".self::$testDomain, "test1@".self::$testDomain, "Test", "User 2");
        $this->assertEquals("User 2", $testUser1->getName()->getFamilyName());
    }

    public function testRenameUserFail() {
        $testUser1 = $this->getTestUser(1);
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->users = $this->createPartialMock(\Google_Service_Directory_Users_Resource::class,['update', 'get']);
        $googleServiceMocked->users->expects($this->once())->method('update')->with("test1@".self::$testDomain, $testUser1)->willThrowException(new \Google_Service_Exception(''));
        $googleServiceMocked->users->expects($this->once())->method('get')->with("test1@".self::$testDomain)->willReturn($testUser1);

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $this->expectException(\Exception::class);

        $googleApps->renameUser("test2@".self::$testDomain, "test1@".self::$testDomain, "Test", "User 2");
    }

    public function testRenameUserFailEmpty() {
        $googleApps = $this->getMockedGoogleApps(null);

        $this->expectException(\Exception::class);

        $googleApps->renameUser("", "");
    }

    public function testUpdateUser() {
        $testUser1 = $this->getTestUser(1);
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->users = $this->createPartialMock(\Google_Service_Directory_Users_Resource::class,['update', 'get']);
        $googleServiceMocked->users->expects($this->once())->method('update')->with("test1@".self::$testDomain, $testUser1);
        $googleServiceMocked->users->expects($this->once())->method('get')->with("test1@".self::$testDomain)->willReturn($testUser1);

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $googleApps->updateUser("test1@".self::$testDomain, "Test", "User 2");
        $this->assertEquals("User 2", $testUser1->getName()->getFamilyName());
    }

    public function testUpdateUserFail() {
        $testUser1 = $this->getTestUser(1);
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->users = $this->createPartialMock(\Google_Service_Directory_Users_Resource::class,['update', 'get']);
        $googleServiceMocked->users->expects($this->once())->method('update')->with("test1@".self::$testDomain, $testUser1)->willThrowException(new \Google_Service_Exception(''));
        $googleServiceMocked->users->expects($this->once())->method('get')->with("test1@".self::$testDomain)->willReturn($testUser1);

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $this->expectException(\Exception::class);

        $googleApps->updateUser("test1@".self::$testDomain, "Test", "User 2");
    }

    public function testUpdateUserFailEmpty() {
        $googleApps = $this->getMockedGoogleApps(null);

        $this->expectException(\Exception::class);

        $googleApps->updateUser("");
    }

    public function testRemoveRemoveUser() {
        $testUser1 = $this->getTestUser(1);
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->users = $this->createPartialMock(\Google_Service_Directory_Users_Resource::class,['delete']);
        $googleServiceMocked->users->expects($this->once())->method('delete')->with("test1@".self::$testDomain);

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $googleApps->removeUser("test1@".self::$testDomain);
    }

    public function testRemoveUserFail() {
        $testUser1 = $this->getTestUser(1);
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->users = $this->createPartialMock(\Google_Service_Directory_Users_Resource::class,['delete']);
        $googleServiceMocked->users->expects($this->once())->method('delete')->with("test1@".self::$testDomain)->willThrowException(new \Google_Service_Exception(''));

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $this->expectException(\Exception::class);

        $googleApps->removeUser("test1@".self::$testDomain);
    }

    public function testRemoveUserFailEmpty() {
        $googleApps = $this->getMockedGoogleApps(null);

        $this->expectException(\Exception::class);

        $googleApps->removeUser("");
    }

    public function testResetUserPassword() {
        $testUser1 = $this->getTestUser(1);
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->users = $this->createPartialMock(\Google_Service_Directory_Users_Resource::class,['update', 'get']);
        $googleServiceMocked->users->expects($this->once())->method('update')->with("test1@".self::$testDomain, $testUser1);
        $googleServiceMocked->users->expects($this->once())->method('get')->with("test1@".self::$testDomain)->willReturn($testUser1);

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $googleApps->resetUserPassword("test1@".self::$testDomain, "Az-54321");
        $this->assertEquals(md5("Az-54321"), $testUser1->getPassword());
        $this->assertEquals("MD5", $testUser1->getHashFunction());
    }

    public function testResetUserPasswordFail() {
        $testUser1 = $this->getTestUser(1);
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->users = $this->createPartialMock(\Google_Service_Directory_Users_Resource::class,['update', 'get']);
        $googleServiceMocked->users->expects($this->once())->method('update')->with("test1@".self::$testDomain, $testUser1)->willThrowException(new \Google_Service_Exception(''));
        $googleServiceMocked->users->expects($this->once())->method('get')->with("test1@".self::$testDomain)->willReturn($testUser1);

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $this->expectException(\Exception::class);

        $googleApps->resetUserPassword("test1@".self::$testDomain, "Az-54321");
    }

    public function testResetUserPasswordFailEmpty() {
        $googleApps = $this->getMockedGoogleApps(null);

        $this->expectException(\Exception::class);

        $googleApps->updateUser("");
    }


    public function testGroupExists() {
        $googleApps = $this->createPartialMock(GoogleApps::class,['getGroupsListCached']);
        $googleApps->expects($this->atLeastOnce())->method("getGroupsListCached")->willReturn([self::$testGroupNameCleaned."@".self::$testDomain => ""]);

        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->groups = $this->createPartialMock(\Google_Service_Directory_Groups_Resource::class,['get']);
        $googleServiceMocked->groups->expects($this->once())->method('get')->with(self::$testGroupNameCleaned."@".self::$testDomain)->willReturn($this->getTestGroup());

        $this->setVariablesOnClass($googleApps, ['domain' => self::$testDomain, 'service' => $googleServiceMocked]);

        $this->assertTrue($googleApps->groupExists(self::$testGroupName));
        $this->assertTrue($googleApps->groupExists(self::$testGroupName, true));
        $this->assertFalse($googleApps->groupExists("Random Group", true));
    }

    public function testGetGroupsList() {
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->groups = $this->createPartialMock(\Google_Service_Directory_Groups_Resource::class,['listGroups']);

        $answer = new \Google_Service_Directory_Groups();
        $answer->setGroups([$this->getTestGroup()]);
        $googleServiceMocked->groups->expects($this->atLeastOnce())->method('listGroups')->with(['domain' => self::$testDomain])->willReturn($answer);

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $groups = $googleApps->getGroupsList();

        $this->assertTrue(is_array($groups));
        $this->assertArraySubset([self::$testGroupNameCleaned."@".self::$testDomain => $this->getTestGroup()], $groups);
    }

    public function testGetGroupsListFail() {
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->groups = $this->createPartialMock(\Google_Service_Directory_Groups_Resource::class,['listGroups']);
        $googleServiceMocked->groups->expects($this->atLeastOnce())->method('listGroups')->with(['domain' => self::$testDomain])->willThrowException(new \Google_Service_Exception(''));;

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $groups = $googleApps->getGroupsList();

        $this->assertTrue(is_array($groups));
        $this->assertEmpty($groups);
    }

    public function testGetGroupsListCached() {
        $testData = [self::$testGroupName];
        $googleApps = $this->createPartialMock(GoogleApps::class, ["getGroupsList"]);
        $googleApps->expects($this->exactly(2))->method("getGroupsList")->willReturn($testData);

        $groups = $googleApps->getGroupsListCached();
        $this->assertEquals($testData, $groups);

        $groups = $googleApps->getGroupsListCached();
        $this->assertEquals($testData, $groups);

        $groups = $googleApps->getGroupsListCached(true);
        $this->assertEquals($testData, $groups);
    }

    public function testCreateGroup() {
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->groups = $this->createPartialMock(\Google_Service_Directory_Groups_Resource::class,['insert', 'get']);
        $googleServiceMocked->groups->expects($this->once())->method('get')->with(self::$testGroupNameCleaned."@".self::$testDomain)->willReturn(null);
        $googleServiceMocked->groups->expects($this->once())->method('insert')->with($this->getTestGroup());

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $googleApps->createGroup(self::$testGroupName);
    }

    public function testCreateGroupFail() {
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->groups = $this->createPartialMock(\Google_Service_Directory_Groups_Resource::class,['insert', 'get']);
        $googleServiceMocked->groups->expects($this->once())->method('get')->with(self::$testGroupNameCleaned."@".self::$testDomain)->willReturn(null);
        $googleServiceMocked->groups->expects($this->once())->method('insert')->with($this->getTestGroup())->willThrowException(new \Google_Service_Exception(''));

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $this->expectException(\Exception::class);

        $googleApps->createGroup(self::$testGroupName);
    }

    public function testCreateGroupFailEmpty() {
        $googleApps = $this->getMockedGoogleApps(null);

        $this->expectException(\Exception::class);

        $googleApps->createGroup("");
    }

    public function testRenameGroup() {
        $testGroup = $this->getTestGroup();
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->groups = $this->createPartialMock(\Google_Service_Directory_Groups_Resource::class,['update', 'get']);
        $googleServiceMocked->groups->expects($this->once())->method('update')->with($testGroup->getEmail(), $testGroup);
        $googleServiceMocked->groups->expects($this->once())->method('get')->with($testGroup->getEmail())->willReturn($testGroup);

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $googleApps->renameGroup("Group 2", $testGroup->getName());
        $this->assertEquals("Group 2", $testGroup->getName());
        $this->assertEquals("group2@".self::$testDomain, $testGroup->getEmail());
    }

    public function testRenameGroupFail() {
        $testGroup = $this->getTestGroup();
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->groups = $this->createPartialMock(\Google_Service_Directory_Groups_Resource::class,['update', 'get']);
        $googleServiceMocked->groups->expects($this->once())->method('update')->with($testGroup->getEmail(), $testGroup)->willThrowException(new \Google_Service_Exception(''));
        $googleServiceMocked->groups->expects($this->once())->method('get')->with($testGroup->getEmail())->willReturn($testGroup);

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $this->expectException(\Exception::class);

        $googleApps->renameGroup("Group 2", $testGroup->getName());
        $this->assertEquals("Group 2", $testGroup->getName());
        $this->assertEquals("group2@".self::$testDomain, $testGroup->getEmail());
    }

    public function testRenameGroupFailEmpty() {
        $googleApps = $this->getMockedGoogleApps(null);

        $this->expectException(\Exception::class);

        $googleApps->renameGroup("", "");
    }


    public function testRemoveGroup() {
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->groups = $this->createPartialMock(\Google_Service_Directory_Groups_Resource::class,['delete']);
        $googleServiceMocked->groups->expects($this->once())->method('delete')->with(self::$testGroupNameCleaned."@".self::$testDomain);

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $googleApps->removeGroup(self::$testGroupName);
    }

    public function testRemoveGroupFail() {
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->groups = $this->createPartialMock(\Google_Service_Directory_Groups_Resource::class,['delete']);
        $googleServiceMocked->groups->expects($this->once())->method('delete')->with(self::$testGroupNameCleaned."@".self::$testDomain)->willThrowException(new \Google_Service_Exception(''));

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $this->expectException(\Exception::class);

        $googleApps->removeGroup(self::$testGroupName);
    }

    public function testRemoveGroupFailEmpty() {
        $googleApps = $this->getMockedGoogleApps(null);

        $this->expectException(\Exception::class);

        $googleApps->removeGroup("");
    }


    public function testUpdateGroupMembers() {
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->members = $this->createPartialMock(\Google_Service_Directory_Members_Resource::class,['listMembers', 'insert', 'delete']);

        $testUser1 = $this->getTestUser(1);
        $testUser2 = $this->getTestUser(2);
        $testUser3 = $this->getTestUser(3);

        $testMember1 = new \Google_Service_Directory_Member();
        $testMember1->setEmail($testUser1->getPrimaryEmail());
        $testMember2 = new \Google_Service_Directory_Member();
        $testMember2->setEmail($testUser2->getPrimaryEmail());

        $currentMembers = new \Google_Service_Directory_Members();
        $currentMembers->setMembers([$testMember1, $testMember2]);

        $googleServiceMocked->members->expects($this->once())->method('listMembers')
            ->with(self::$testGroupNameCleaned."@".self::$testDomain)
            ->willReturn($currentMembers);

        $toInsertMember = new \Google_Service_Directory_Member();
        $toInsertMember->setEmail($testUser3->getPrimaryEmail());
        $googleServiceMocked->members->expects($this->once())->method('insert')->with(self::$testGroupNameCleaned."@".self::$testDomain, $toInsertMember);
        $googleServiceMocked->members->expects($this->once())->method('delete')->with(self::$testGroupNameCleaned."@".self::$testDomain, $testUser2->getPrimaryEmail());

        $googleServiceMocked->groups = $this->createPartialMock(\Google_Service_Directory_Groups_Resource::class,['get']);
        $googleServiceMocked->groups->expects($this->once())->method('get')->with(self::$testGroupNameCleaned."@".self::$testDomain)->willReturn($this->getTestGroup());

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $googleApps->updateGroupMembers([$testUser1->getPrimaryEmail(), $testUser3->getPrimaryEmail()], self::$testGroupName);
    }

    public function testAddUserToGroup() {
        $testMember1 = new \Google_Service_Directory_Member();
        $testMember1->setEmail("test1@".self::$testDomain);

        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->members = $this->createPartialMock(\Google_Service_Directory_Members_Resource::class,['insert']);
        $googleServiceMocked->members->expects($this->once())->method('insert')->with(self::$testGroupNameCleaned."@".self::$testDomain, $testMember1);

        $googleServiceMocked->groups = $this->createPartialMock(\Google_Service_Directory_Groups_Resource::class,['get']);
        $googleServiceMocked->groups->expects($this->once())->method('get')->with(self::$testGroupNameCleaned."@".self::$testDomain)->willReturn(new \Google_Service_Directory_Group());

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $googleApps->addUserToGroup("test1", self::$testGroupName);
    }

    public function testAddUserToGroupFail() {
        $testMember1 = new \Google_Service_Directory_Member();
        $testMember1->setEmail("test1@".self::$testDomain);

        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->members = $this->createPartialMock(\Google_Service_Directory_Members_Resource::class,['insert']);
        $googleServiceMocked->members->expects($this->once())->method('insert')->with(self::$testGroupNameCleaned."@".self::$testDomain, $testMember1)->willThrowException(new \Google_Service_Exception(''));

        $googleServiceMocked->groups = $this->createPartialMock(\Google_Service_Directory_Groups_Resource::class,['get']);
        $googleServiceMocked->groups->expects($this->once())->method('get')->with(self::$testGroupNameCleaned."@".self::$testDomain)->willReturn(new \Google_Service_Directory_Group());

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $this->expectException(\Exception::class);

        $googleApps->addUserToGroup("test1", self::$testGroupName);
    }

    public function testAddUserToGroupFailEmpty() {
        $googleApps = $this->getMockedGoogleApps(null);

        $this->expectException(\Exception::class);

        $googleApps->addUserToGroup("", "");
    }

    public function testOuExists() {
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->orgunits = $this->createPartialMock(\Google_Service_Directory_OrgUnits_Resource::class,['get']);
        $googleServiceMocked->orgunits->expects($this->once())->method('get')
            ->with('my_customer', "test/".self::$testOuNameCleaned)
            ->willReturn(new \Google_Service_Directory_OrgUnit());

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $this->assertTrue($googleApps->ouExists(self::$testOuName));
    }

    public function testOuExistsFail() {
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->orgunits = $this->createPartialMock(\Google_Service_Directory_OrgUnits_Resource::class,['get']);
        $googleServiceMocked->orgunits->expects($this->once())->method('get')->with('my_customer', "test/".self::$testOuNameCleaned)->willThrowException(new \Google_Service_Exception(''));

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $this->assertFalse($googleApps->ouExists(self::$testOuName));
    }

    public function testGetOUsList() {
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->orgunits = $this->createPartialMock(\Google_Service_Directory_OrgUnits_Resource::class,['listOrgunits']);

        $testOu = new \Google_Service_Directory_OrgUnit();
        $testOu->setName(self::$testOuNameCleaned);

        $answer = new \Google_Service_Directory_OrgUnits();
        $answer->setOrganizationUnits([$testOu]);
        $googleServiceMocked->orgunits->expects($this->atLeastOnce())->method('listOrgunits')->with('my_customer')->willReturn($answer);

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $ous = $googleApps->getOUsList();

        $this->assertTrue(is_array($ous));
        $this->assertArraySubset([self::$testOuNameCleaned => $testOu], $ous);
    }

    public function testGetOUsListFail() {
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->orgunits = $this->createPartialMock(\Google_Service_Directory_OrgUnits_Resource::class,['listOrgunits']);
        $googleServiceMocked->orgunits->expects($this->atLeastOnce())->method('listOrgunits')->with('my_customer')->willThrowException(new \Google_Service_Exception(''));;

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $ous = $googleApps->getOUsList();

        $this->assertTrue(is_array($ous));
        $this->assertEmpty($ous);
    }

    public function testCreateOU() {
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->orgunits = $this->createPartialMock(\Google_Service_Directory_OrgUnits_Resource::class,['insert', 'get']);
        $googleServiceMocked->orgunits->expects($this->once())->method('get')->with('my_customer', "test/".self::$testOuNameCleaned)->willReturn(null);

        $testOu = new \Google_Service_Directory_OrgUnit();
        $testOu->setName(self::$testOuNameCleaned);
        $testOu->setParentOrgUnitPath("/test");

        $googleServiceMocked->orgunits->expects($this->once())->method('insert')->with('my_customer', $testOu);

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $googleApps->createOU(self::$testOuName);
    }

    public function testCreateOUFail() {
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->orgunits = $this->createPartialMock(\Google_Service_Directory_OrgUnits_Resource::class,['insert', 'get']);
        $googleServiceMocked->orgunits->expects($this->once())->method('get')->with('my_customer', "test/".self::$testOuNameCleaned)->willReturn(null);

        $testOu = new \Google_Service_Directory_OrgUnit();
        $testOu->setName(self::$testOuNameCleaned);
        $testOu->setParentOrgUnitPath("/test");

        $googleServiceMocked->orgunits->expects($this->once())->method('insert')->with('my_customer', $testOu)->willThrowException(new \Google_Service_Exception(''));

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $this->expectException(\Exception::class);

        $googleApps->createOU(self::$testOuName);
    }

    public function testCreateOUFailEmpty() {
        $googleApps = $this->getMockedGoogleApps(null);

        $this->expectException(\Exception::class);

        $googleApps->createOU("");
    }

    public function testUpdateOUMembers() {
        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);

        $googleServiceMocked->orgunits = $this->createPartialMock(\Google_Service_Directory_OrgUnits_Resource::class,['get']);
        $googleServiceMocked->orgunits->expects($this->once())->method('get')
            ->with('my_customer', "test/".self::$testOuNameCleaned)
            ->willReturn(new \Google_Service_Directory_OrgUnit());

        $googleServiceMocked->users = $this->createPartialMock(\Google_Service_Directory_Users_Resource::class,['listUsers', 'get', 'update']);

        $testUser1 = $this->getTestUser(1);
        $testUser2 = $this->getTestUser(2);
        $testUser3 = $this->getTestUser(3);

        $currentMembers = new \Google_Service_Directory_Users();
        $currentMembers->setUsers([$testUser1, $testUser2]);

        $googleServiceMocked->users->expects($this->at(0))
            ->method('listUsers')
            ->with(['domain' => self::$testDomain, 'query' => "orgUnitPath=/test/".self::$testOuNameCleaned.""])
            ->willReturn($currentMembers);

        $googleServiceMocked->users->expects($this->at(1))->method('get')->with($testUser3->getPrimaryEmail())->willReturn($testUser3);
        $googleServiceMocked->users->expects($this->at(2))->method('update')->with($testUser3->getPrimaryEmail(), $testUser3);
        $googleServiceMocked->users->expects($this->at(3))->method('update')->with($testUser2->getPrimaryEmail(), $testUser2);

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);
        $googleApps->updateOUMembers([$testUser1->getPrimaryEmail(), $testUser3->getPrimaryEmail()], self::$testOuNameCleaned);

        $this->assertEquals("/test/".self::$testOuNameCleaned, $testUser3->getOrgUnitPath());
        $this->assertEquals("/test", $testUser2->getOrgUnitPath());
    }

    public function testUpdateOUMembersFailEmpty() {
        $googleApps = $this->getMockedGoogleApps(null);

        $this->expectException(\Exception::class);

        $googleApps->updateOUMembers([], "");
    }

    public function testAddUserToOU() {
        $testUser = $this->getTestUser(1);

        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->users = $this->createPartialMock(\Google_Service_Directory_Users_Resource::class,['get', 'update']);
        $googleServiceMocked->users->expects($this->once())->method('get')->with($testUser->getPrimaryEmail())->willReturn($testUser);
        $googleServiceMocked->users->expects($this->once())->method('update')->with($testUser->getPrimaryEmail(), $testUser);

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $googleApps->addUserToOU("test1", self::$testGroupName);
    }

    public function testAddUserToOUFail() {
        $testUser = $this->getTestUser(1);

        $googleServiceMocked = $this->createMock(\Google_Service_Directory::class);
        $googleServiceMocked->users = $this->createPartialMock(\Google_Service_Directory_Users_Resource::class,['get', 'update']);
        $googleServiceMocked->users->expects($this->once())->method('get')->with($testUser->getPrimaryEmail())->willReturn($testUser);
        $googleServiceMocked->users->expects($this->once())->method('update')->with($testUser->getPrimaryEmail(), $testUser)->willThrowException(new \Google_Service_Exception(''));

        $googleApps = $this->getMockedGoogleApps($googleServiceMocked);

        $this->expectException(\Exception::class);

        $googleApps->addUserToOU("test1", self::$testGroupName);
    }

    public function testAddUserToOUFailEmpty() {
        $googleApps = $this->getMockedGoogleApps(null);

        $this->expectException(\Exception::class);

        $googleApps->addUserToOU("", "");
    }

}
