<?php

namespace Zen\IgrooveBundle\Tests\Manager;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Psr\Log\NullLogger;
use Zen\IgrooveBundle\Entity\InternetOpen;
use Zen\IgrooveBundle\Entity\LdapGroup;
use Zen\IgrooveBundle\Entity\LdapUser;
use Zen\IgrooveBundle\Manager\ConfigurationManager;
use Zen\IgrooveBundle\Manager\LdapProxy;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Zen\IgrooveBundle\Manager\MicrosoftLdapService;
use Zen\IgrooveBundle\Repository\InternetOpenRepository;
use Zen\IgrooveBundle\Repository\LdapGroupRepository;
use Zen\IgrooveBundle\Tests\TestUtilityTraits;

class LdapProxyTest extends KernelTestCase {

    use TestUtilityTraits;

    /**
     * @var EntityManager
     */
    protected static $em;

    public static function setUpBeforeClass() {
        self::bootKernel();
        self::$em = self::$kernel->getContainer()->get('doctrine')->getManager();
    }

    public function setUp() {
        self::prepareDB();
    }

    protected function getConfigurationManagerMock() {
        $fakeConfigurationManager = $this->createPartialMock(ConfigurationManager::class,['getActiveDirectoryConfiguration']);
        $fakeConfigurationManager->expects($this->atLeastOnce())
            ->method('getActiveDirectoryConfiguration')
            ->will($this->returnValue([]));
        return $fakeConfigurationManager;
    }

    public function testPurgeAndPopulateAll() {
        $nullLogger = new NullLogger();
        $fakeConfigurationManager = $this->getConfigurationManagerMock();
        $fakeMicrosoftLdapService = $this->createPartialMock(MicrosoftLdapService::class,
            ['setParameters',  'getConnectedServerHostname','getAllGroups', 'getAllUsers', 'getUserMembership', 'getGroupMembership']);

        $fakeMicrosoftLdapService->expects($this->once())->method('setParameters')->with([]);
        $fakeMicrosoftLdapService->expects($this->once())->method('getConnectedServerHostname')->willReturn("test");
        $fakeMicrosoftLdapService->expects($this->once())->method('getAllGroups')->willReturn(['Test Group', 'Test Group 2']);
        $fakeMicrosoftLdapService->expects($this->once())->method('getAllUsers')->willReturn([MicrosoftLdapServiceTest::$testUserData]);
        $fakeMicrosoftLdapService->expects($this->once())->method('getUserMembership')
            ->with($this->equalTo(MicrosoftLdapServiceTest::$testUserData['username']))
            ->willReturn(['Test Group']);
        $fakeMicrosoftLdapService->expects($this->atLeastOnce())->method('getGroupMembership')
            ->willReturnMap([['Test Group', ['Test Group 2']], ['Test Group 2', []]]);

        $startLdapGroup = new LdapGroup();
        $startLdapGroup->setName("Start Test Group");
        self::$em->persist($startLdapGroup);

        $startLdapUser = new LdapUser();
        $startLdapUser->setUsername("starttest.user");
        self::$em->persist($startLdapUser);

        self::$em->flush();
        unset($startLdapGroup, $startLdapUser);

        $ldapProxy = new LdapProxy(self::$em, $fakeConfigurationManager, $fakeMicrosoftLdapService, $nullLogger);
        $ldapProxy->purgeAndPopulateAll();

        $ldapStartTestGroup = self::$em->getRepository('ZenIgrooveBundle:LdapGroup')->findOneBy(['name' => "Start Test Group"]);
        $this->assertNull($ldapStartTestGroup);

        $ldapStartTestUser = self::$em->getRepository('ZenIgrooveBundle:LdapUser')->findOneBy(['username' => "starttest.user"]);
        $this->assertNull($ldapStartTestUser);

        $ldapTestGroup = self::$em->getRepository('ZenIgrooveBundle:LdapGroup')->findOneBy(['name' => "Test Group"]);
        $this->assertTrue($ldapTestGroup instanceof LdapGroup);
        $this->assertAttributeEquals("Test Group",'name',$ldapTestGroup);
        $this->assertAttributeEquals(null,'operation',$ldapTestGroup);
        $ldapTestGroupMembers = $ldapTestGroup->getMembersList();
        $this->assertTrue(is_array($ldapTestGroupMembers));
        $this->assertArraySubset(['user' => ['test2.user'], 'group' => ['Test Group 2']], $ldapTestGroupMembers);

        $ldapTestGroup2 = self::$em->getRepository('ZenIgrooveBundle:LdapGroup')->findOneBy(['name' => "Test Group 2"]);
        $this->assertTrue($ldapTestGroup2 instanceof LdapGroup);
        $this->assertAttributeEquals("Test Group 2",'name',$ldapTestGroup2);
        $this->assertAttributeEquals(null,'operation',$ldapTestGroup2);
        $ldapTestGroup2Members = $ldapTestGroup2->getMembersList();
        $this->assertTrue(is_array($ldapTestGroup2Members));
        $this->assertEmpty($ldapTestGroup2Members);

        $ldapTestUser = self::$em->getRepository('ZenIgrooveBundle:LdapUser')->findOneBy(['username' => MicrosoftLdapServiceTest::$testUserData['username']]);
        $this->assertTrue($ldapTestUser instanceof LdapUser);
        foreach (['distinguishedId', 'dn', 'lastname', 'firstname'] as $key) {
            $this->assertAttributeEquals(MicrosoftLdapServiceTest::$testUserData[$key], $key, $ldapTestUser);
        }
        $this->assertAttributeEquals(null,'operation',$ldapTestUser);
        $this->assertAttributeEquals(null,'starting_password',$ldapTestUser);
        $this->assertArraySubset(MicrosoftLdapServiceTest::$testUserData['parameters'], (array)json_decode($ldapTestUser->getAttributes()));
    }

    public function testSyncLDAPfromDB() {
        $nullLogger = new NullLogger();
        $fakeConfigurationManager = $this->getConfigurationManagerMock();
        $fakeMicrosoftLdapService = $this->createPartialMock(MicrosoftLdapService::class, ['setParameters',  'getConnectedServerHostname','createGroup', 'modifyUser', 'updateUsersIntoGroup']);
        $fakeMicrosoftLdapService->expects($this->atLeastOnce())->method('setParameters')->with([]);
        $fakeMicrosoftLdapService->expects($this->atLeastOnce())->method('getConnectedServerHostname')->willReturn("test");
        $fakeMicrosoftLdapService->expects($this->atLeastOnce())->method('createGroup')->willReturn(true);
        $fakeMicrosoftLdapService->expects($this->atLeastOnce())->method('modifyUser')->willReturn(true);
        $fakeMicrosoftLdapService->expects($this->atLeastOnce())->method('updateUsersIntoGroup')->willReturn(true);

        $startLdapGroup = new LdapGroup();
        $startLdapGroup->setName("Test Group");
        $startLdapGroup->setOperation('CREATE');
        self::$em->persist($startLdapGroup);

        $startLdapGroup2 = new LdapGroup();
        $startLdapGroup2->setName("Test Group 2");
        $startLdapGroup2->addMember("user", 'test.user');
        self::$em->persist($startLdapGroup2);

        $startLdapUser = new LdapUser();
        $startLdapUser->setUsername("test.user");
        $startLdapUser->setOperation('MODIFY');
        self::$em->persist($startLdapUser);

        $startLdapUser2 = new LdapUser();
        $startLdapUser2->setUsername("test2.user");
        self::$em->persist($startLdapUser2);

        self::$em->flush();

        $ldapProxy = $this->getMockBuilder(LdapProxy::class)
            ->setConstructorArgs([self::$em, $fakeConfigurationManager, $fakeMicrosoftLdapService, $nullLogger])
            ->enableProxyingToOriginalMethods()
            ->setMethods(['syncLdapGroupWithDBGroup', 'syncLdapUserWithDBUser', 'syncLdapGroupMembershipWithDB'])
            ->getMock();

        $ldapProxy->expects($this->atLeastOnce())->method('syncLdapGroupWithDBGroup')->withConsecutive($startLdapGroup, $startLdapGroup2);
        $ldapProxy->expects($this->atLeastOnce())->method('syncLdapUserWithDBUser')->with($startLdapUser);
        $ldapProxy->expects($this->atLeastOnce())->method('syncLdapGroupMembershipWithDB')->withConsecutive($startLdapGroup, $startLdapGroup2);

        $ldapProxy->syncLDAPfromDB();

        $ldapTestGroup = self::$em->getRepository('ZenIgrooveBundle:LdapGroup')->findOneBy(['name' => "Test Group"]);
        $this->assertTrue($ldapTestGroup instanceof LdapGroup);
        $this->assertAttributeEquals(null,'operation',$ldapTestGroup);

        $ldapTestGroup2 = self::$em->getRepository('ZenIgrooveBundle:LdapGroup')->findOneBy(['name' => "Test Group 2"]);
        $this->assertTrue($ldapTestGroup2 instanceof LdapGroup);
        $this->assertAttributeEquals(null,'operation',$ldapTestGroup2);

        $ldapTestUser = self::$em->getRepository('ZenIgrooveBundle:LdapUser')->findOneBy(['username' => 'test.user']);
        $this->assertTrue($ldapTestUser instanceof LdapUser);
        $this->assertAttributeEquals(null,'operation',$ldapTestUser);
    }

    public function testSyncLdapGroupWithDBGroupCreation() {
        $groupName = "Test Group";

        $nullLogger = new NullLogger();
        $fakeConfigurationManager = $this->getConfigurationManagerMock();
        $fakeMicrosoftLdapService = $this->createPartialMock(MicrosoftLdapService::class, ['setParameters',  'getConnectedServerHostname','createGroup']);
        $fakeMicrosoftLdapService->expects($this->once())->method('setParameters')->with([]);
        $fakeMicrosoftLdapService->expects($this->once())->method('getConnectedServerHostname')->willReturn("test");
        $fakeMicrosoftLdapService->expects($this->once())->method('createGroup')->with($groupName);

        $startLdapGroup = new LdapGroup();
        $startLdapGroup->setName($groupName);
        $startLdapGroup->setOperation('CREATE');

        $ldapProxy = new LdapProxy(self::$em, $fakeConfigurationManager, $fakeMicrosoftLdapService, $nullLogger);
        $ldapProxy->syncLdapGroupWithDBGroup($startLdapGroup);

        $this->assertAttributeEquals('MEMBERS CHANGED', 'operation', $startLdapGroup);
    }

    public function testSyncLdapGroupWithDBGroupCreationFail() {
        $groupName = "Test Group";

        $nullLogger = new NullLogger();
        $fakeConfigurationManager = $this->getConfigurationManagerMock();
        $fakeMicrosoftLdapService = $this->createPartialMock(MicrosoftLdapService::class, ['setParameters',  'getConnectedServerHostname','createGroup']);
        $fakeMicrosoftLdapService->expects($this->once())->method('setParameters')->with([]);
        $fakeMicrosoftLdapService->expects($this->once())->method('getConnectedServerHostname')->willReturn("test");
        $fakeMicrosoftLdapService->expects($this->once())->method('createGroup')->with($groupName)->willThrowException(new \Exception('Error creating group'));

        $startLdapGroup = new LdapGroup();
        $startLdapGroup->setName($groupName);
        $startLdapGroup->setOperation('CREATE');

        $this->expectException(\Exception::class);

        $ldapProxy = new LdapProxy(self::$em, $fakeConfigurationManager, $fakeMicrosoftLdapService, $nullLogger);
        $ldapProxy->syncLdapGroupWithDBGroup($startLdapGroup);

        $this->assertAttributeEquals('ERROR', 'operation', $startLdapGroup);
    }

    public function testSyncLdapGroupWithDBGroupRename() {
        $groupName = "Test Group";
        $newGroupName = "New Test Group";

        $nullLogger = new NullLogger();
        $fakeConfigurationManager = $this->getConfigurationManagerMock();
        $fakeMicrosoftLdapService = $this->createPartialMock(MicrosoftLdapService::class, ['setParameters',  'getConnectedServerHostname','renameGroup']);
        $fakeMicrosoftLdapService->expects($this->once())->method('setParameters')->with([]);
        $fakeMicrosoftLdapService->expects($this->once())->method('getConnectedServerHostname')->willReturn("test");
        $fakeMicrosoftLdapService->expects($this->once())->method('renameGroup')->with($groupName, $newGroupName);

        $startLdapGroup = new LdapGroup();
        $startLdapGroup->setName($groupName);
        $startLdapGroup->setOperation('RENAME:'.$newGroupName);

        $ldapProxy = new LdapProxy(self::$em, $fakeConfigurationManager, $fakeMicrosoftLdapService, $nullLogger);
        $ldapProxy->syncLdapGroupWithDBGroup($startLdapGroup);

        $this->assertAttributeEquals('', 'operation', $startLdapGroup);
    }

    public function testSyncLdapGroupWithDBGroupRenameFail() {
        $groupName = "Test Group";
        $newGroupName = "New Test Group";

        $nullLogger = new NullLogger();
        $fakeConfigurationManager = $this->getConfigurationManagerMock();
        $fakeMicrosoftLdapService = $this->createPartialMock(MicrosoftLdapService::class, ['setParameters',  'getConnectedServerHostname','renameGroup']);
        $fakeMicrosoftLdapService->expects($this->once())->method('setParameters')->with([]);
        $fakeMicrosoftLdapService->expects($this->once())->method('getConnectedServerHostname')->willReturn("test");
        $fakeMicrosoftLdapService->expects($this->once())->method('renameGroup')->with($groupName, $newGroupName)->willThrowException(new \Exception('Error renaming group'));

        $startLdapGroup = new LdapGroup();
        $startLdapGroup->setName($groupName);
        $startLdapGroup->setOperation('RENAME:'.$newGroupName);

        $this->expectException(\Exception::class);

        $ldapProxy = new LdapProxy(self::$em, $fakeConfigurationManager, $fakeMicrosoftLdapService, $nullLogger);
        $ldapProxy->syncLdapGroupWithDBGroup($startLdapGroup);

        $this->assertAttributeEquals('ERROR', 'operation', $startLdapGroup);
    }


    public function testSyncLdapGroupWithDBGroupRemove() {
        $groupName = "Test Group";

        $nullLogger = new NullLogger();
        $fakeConfigurationManager = $this->getConfigurationManagerMock();
        $fakeMicrosoftLdapService = $this->createPartialMock(MicrosoftLdapService::class, ['setParameters',  'getConnectedServerHostname','removeGroup']);
        $fakeMicrosoftLdapService->expects($this->once())->method('setParameters')->with([]);
        $fakeMicrosoftLdapService->expects($this->once())->method('getConnectedServerHostname')->willReturn("test");
        $fakeMicrosoftLdapService->expects($this->once())->method('removeGroup')->with($groupName);

        $startLdapGroup = new LdapGroup();
        $startLdapGroup->setName($groupName);
        $startLdapGroup->setOperation('REMOVE');
        self::$em->persist($startLdapGroup);

        $ldapProxy = new LdapProxy(self::$em, $fakeConfigurationManager, $fakeMicrosoftLdapService, $nullLogger);
        $ldapProxy->syncLdapGroupWithDBGroup($startLdapGroup);
    }

    public function testSyncLdapGroupWithDBGroupRemoveFail() {
        $groupName = "Test Group";

        $nullLogger = new NullLogger();
        $fakeConfigurationManager = $this->getConfigurationManagerMock();
        $fakeMicrosoftLdapService = $this->createPartialMock(MicrosoftLdapService::class, ['setParameters',  'getConnectedServerHostname','removeGroup']);
        $fakeMicrosoftLdapService->expects($this->once())->method('setParameters')->with([]);
        $fakeMicrosoftLdapService->expects($this->once())->method('getConnectedServerHostname')->willReturn("test");
        $fakeMicrosoftLdapService->expects($this->once())->method('removeGroup')->with($groupName)->willThrowException(new \Exception('Error removing group'));

        $startLdapGroup = new LdapGroup();
        $startLdapGroup->setName($groupName);
        $startLdapGroup->setOperation('REMOVE');

        $this->expectException(\Exception::class);

        $ldapProxy = new LdapProxy(self::$em, $fakeConfigurationManager, $fakeMicrosoftLdapService, $nullLogger);
        $ldapProxy->syncLdapGroupWithDBGroup($startLdapGroup);

        $this->assertAttributeEquals('ERROR', 'operation', $startLdapGroup);
    }

    public function testSyncLdapUserWithDBUserCreation() {
        $startLdapUser = new LdapUser();
        $startLdapUser->setUsername("test.user");
        $startLdapUser->setOperation('CREATE');

        $nullLogger = new NullLogger();
        $fakeConfigurationManager = $this->getConfigurationManagerMock();
        $fakeMicrosoftLdapService = $this->createPartialMock(MicrosoftLdapService::class, ['setParameters',  'getConnectedServerHostname','createUser', 'getObjectGUIDFromUsername']);
        $fakeMicrosoftLdapService->expects($this->once())->method('setParameters')->with([]);
        $fakeMicrosoftLdapService->expects($this->once())->method('getConnectedServerHostname')->willReturn("test");
        $fakeMicrosoftLdapService->expects($this->once())->method('createUser')->with($startLdapUser);
        $fakeMicrosoftLdapService->expects($this->once())->method('getObjectGUIDFromUsername')->with($startLdapUser->getUsername());

        $ldapProxy = new LdapProxy(self::$em, $fakeConfigurationManager, $fakeMicrosoftLdapService, $nullLogger);
        $ldapProxy->syncLdapUserWithDBUser($startLdapUser);

        $this->assertAttributeEquals('', 'operation', $startLdapUser);
    }

    public function testSyncLdapUserWithDBUserCreationFail() {
        $startLdapUser = new LdapUser();
        $startLdapUser->setUsername("test.user");
        $startLdapUser->setOperation('CREATE');

        $nullLogger = new NullLogger();
        $fakeConfigurationManager = $this->getConfigurationManagerMock();
        $fakeMicrosoftLdapService = $this->createPartialMock(MicrosoftLdapService::class, ['setParameters',  'getConnectedServerHostname','createUser']);
        $fakeMicrosoftLdapService->expects($this->once())->method('setParameters')->with([]);
        $fakeMicrosoftLdapService->expects($this->once())->method('getConnectedServerHostname')->willReturn("test");
        $fakeMicrosoftLdapService->expects($this->once())->method('createUser')->with($startLdapUser)->willThrowException(new \Exception('Error creating user'));

        $this->expectException(\Exception::class);

        $ldapProxy = new LdapProxy(self::$em, $fakeConfigurationManager, $fakeMicrosoftLdapService, $nullLogger);
        $ldapProxy->syncLdapUserWithDBUser($startLdapUser);

        $this->assertAttributeEquals('ERROR', 'operation', $startLdapUser);
    }

    public function testSyncLdapUserWithDBUserModify() {
        $startLdapUser = new LdapUser();
        $startLdapUser->setUsername("test.user");
        $startLdapUser->setOperation('MODIFY');

        $nullLogger = new NullLogger();
        $fakeConfigurationManager = $this->getConfigurationManagerMock();
        $fakeMicrosoftLdapService = $this->createPartialMock(MicrosoftLdapService::class, ['setParameters',  'getConnectedServerHostname','modifyUser']);
        $fakeMicrosoftLdapService->expects($this->once())->method('setParameters')->with([]);
        $fakeMicrosoftLdapService->expects($this->once())->method('getConnectedServerHostname')->willReturn("test");
        $fakeMicrosoftLdapService->expects($this->once())->method('modifyUser')->with($startLdapUser);

        $ldapProxy = new LdapProxy(self::$em, $fakeConfigurationManager, $fakeMicrosoftLdapService, $nullLogger);
        $ldapProxy->syncLdapUserWithDBUser($startLdapUser);

        $this->assertAttributeEquals('', 'operation', $startLdapUser);
    }

    public function testSyncLdapUserWithDBUserModifyFail() {
        $startLdapUser = new LdapUser();
        $startLdapUser->setUsername("test.user");
        $startLdapUser->setOperation('MODIFY');

        $nullLogger = new NullLogger();
        $fakeConfigurationManager = $this->getConfigurationManagerMock();
        $fakeMicrosoftLdapService = $this->createPartialMock(MicrosoftLdapService::class, ['setParameters',  'getConnectedServerHostname','modifyUser']);
        $fakeMicrosoftLdapService->expects($this->once())->method('setParameters')->with([]);
        $fakeMicrosoftLdapService->expects($this->once())->method('getConnectedServerHostname')->willReturn("test");
        $fakeMicrosoftLdapService->expects($this->once())->method('modifyUser')->with($startLdapUser)->willThrowException(new \Exception('Error modifying user'));;

        $this->expectException(\Exception::class);

        $ldapProxy = new LdapProxy(self::$em, $fakeConfigurationManager, $fakeMicrosoftLdapService, $nullLogger);
        $ldapProxy->syncLdapUserWithDBUser($startLdapUser);

        $this->assertAttributeEquals('ERROR', 'operation', $startLdapUser);
    }

    public function testSyncLdapUserWithDBUserRemove() {
        $startLdapUser = new LdapUser();
        $startLdapUser->setUsername("test.user");
        $startLdapUser->setOperation('REMOVE');
        self::$em->persist($startLdapUser);

        $nullLogger = new NullLogger();
        $fakeConfigurationManager = $this->getConfigurationManagerMock();
        $fakeMicrosoftLdapService = $this->createPartialMock(MicrosoftLdapService::class, ['setParameters',  'getConnectedServerHostname','removeUser']);
        $fakeMicrosoftLdapService->expects($this->once())->method('setParameters')->with([]);
        $fakeMicrosoftLdapService->expects($this->once())->method('getConnectedServerHostname')->willReturn("test");
        $fakeMicrosoftLdapService->expects($this->once())->method('removeUser')->with($startLdapUser->getUsername());

        $ldapProxy = new LdapProxy(self::$em, $fakeConfigurationManager, $fakeMicrosoftLdapService, $nullLogger);
        $ldapProxy->syncLdapUserWithDBUser($startLdapUser);
    }

    public function testSyncLdapUserWithDBUserRemoveFail() {
        $startLdapUser = new LdapUser();
        $startLdapUser->setUsername("test.user");
        $startLdapUser->setOperation('REMOVE');

        $nullLogger = new NullLogger();
        $fakeConfigurationManager = $this->getConfigurationManagerMock();
        $fakeMicrosoftLdapService = $this->createPartialMock(MicrosoftLdapService::class, ['setParameters',  'getConnectedServerHostname','removeUser']);
        $fakeMicrosoftLdapService->expects($this->once())->method('setParameters')->with([]);
        $fakeMicrosoftLdapService->expects($this->once())->method('getConnectedServerHostname')->willReturn("test");
        $fakeMicrosoftLdapService->expects($this->once())->method('removeUser')->with($startLdapUser->getUsername())->willThrowException(new \Exception('Error removing user'));;

        $this->expectException(\Exception::class);

        $ldapProxy = new LdapProxy(self::$em, $fakeConfigurationManager, $fakeMicrosoftLdapService, $nullLogger);
        $ldapProxy->syncLdapUserWithDBUser($startLdapUser);

        $this->assertAttributeEquals('ERROR', 'operation', $startLdapUser);
    }

    public function testSyncLdapGroupMembershipWithDB() {
        $groupName = "Test Group";
        $groupName2 = "Test Group 2";
        $username = "test.user";

        $startLdapGroup = new LdapGroup();
        $startLdapGroup->setName($groupName);
        $startLdapGroup->setOperation('MEMBERS CHANGED');
        $startLdapGroup->addMember('user', $username);
        $startLdapGroup->addMember('group', $groupName2);

        $nullLogger = new NullLogger();
        $fakeConfigurationManager = $this->getConfigurationManagerMock();
        $fakeMicrosoftLdapService = $this->createPartialMock(MicrosoftLdapService::class, ['setParameters',  'getConnectedServerHostname','updateUsersIntoGroup', 'updateGroupsIntoGroup']);
        $fakeMicrosoftLdapService->expects($this->once())->method('setParameters')->with([]);
        $fakeMicrosoftLdapService->expects($this->once())->method('getConnectedServerHostname')->willReturn("test");
        $fakeMicrosoftLdapService->expects($this->once())->method('updateUsersIntoGroup')->with($groupName, [$username]);
        $fakeMicrosoftLdapService->expects($this->once())->method('updateGroupsIntoGroup')->with($groupName, [$groupName2]);

        $ldapProxy = new LdapProxy(self::$em, $fakeConfigurationManager, $fakeMicrosoftLdapService, $nullLogger);
        $ldapProxy->syncLdapGroupMembershipWithDB($startLdapGroup);

        $this->assertAttributeEquals('', 'operation', $startLdapGroup);
    }

    public function testSyncLdapGroupMembershipWithDBFail() {
        $groupName = "Test Group";
        $groupName2 = "Test Group 2";
        $username = "test.user";

        $startLdapGroup = new LdapGroup();
        $startLdapGroup->setName($groupName);
        $startLdapGroup->setOperation('MEMBERS CHANGED');
        $startLdapGroup->addMember('user', $username);
        $startLdapGroup->addMember('group', $groupName2);

        $nullLogger = new NullLogger();
        $fakeConfigurationManager = $this->getConfigurationManagerMock();
        $fakeMicrosoftLdapService = $this->createPartialMock(MicrosoftLdapService::class, ['setParameters',  'getConnectedServerHostname','updateUsersIntoGroup', 'updateGroupsIntoGroup']);
        $fakeMicrosoftLdapService->expects($this->once())->method('setParameters')->with([]);
        $fakeMicrosoftLdapService->expects($this->once())->method('getConnectedServerHostname')->willReturn("test");
        $fakeMicrosoftLdapService->expects($this->once())->method('updateUsersIntoGroup')->with($groupName, [$username]);
        $fakeMicrosoftLdapService->expects($this->once())->method('updateGroupsIntoGroup')->with($groupName, [$groupName2])->willThrowException(new \Exception('Error updating users into group'));

        $this->expectException(\Exception::class);

        $ldapProxy = new LdapProxy(self::$em, $fakeConfigurationManager, $fakeMicrosoftLdapService, $nullLogger);
        $ldapProxy->syncLdapGroupMembershipWithDB($startLdapGroup);

        $this->assertAttributeEquals('ERROR', 'operation', $startLdapGroup);
    }

    public function testSyncInternetAccessLdapGroup() {
        $adPrefix = "test_";

        $fakeConfigurationManager = $this->createPartialMock(ConfigurationManager::class, ['getActiveDirectoryGeneratedGroupPrefix']);
        $fakeConfigurationManager->expects($this->atLeastOnce())->method('getActiveDirectoryGeneratedGroupPrefix')->willReturn($adPrefix);

        $ldapGroup = new LdapGroup();
        $ldapGroup2 = new LdapGroup();

        $fakeLdapGroupRepository = $this->createPartialMock(LdapGroupRepository::class,['find']);
        $fakeLdapGroupRepository->expects($this->at(0))->method('find')->with($adPrefix."InternetAccess")->willReturn($ldapGroup);
        $fakeLdapGroupRepository->expects($this->at(1))->method('find')->with($adPrefix."PersonalDeviceAccess")->willReturn($ldapGroup2);

        $internetOpen = new InternetOpen();
        $internetOpen->setActivationCompleated(FALSE);

        $fakeInternetOpenRepository = $this->createPartialMock(InternetOpenRepository::class,['findBy']);
        $fakeInternetOpenRepository->expects($this->once())->method('findBy')->with(['activationCompleated' => false, 'type' => ['user', 'group']])->willReturn([$internetOpen]);

        $fakeEm = $this->getMockedEM(['LdapGroup' => $fakeLdapGroupRepository, 'InternetOpen' => $fakeInternetOpenRepository]);
        $personsAndGroups = $this->createPartialMock(LdapProxy::class, ['syncLdapGroupMembershipWithDB']);
        $personsAndGroups->expects($this->at(0))->method('syncLdapGroupMembershipWithDB')->with($ldapGroup);
        $personsAndGroups->expects($this->at(1))->method('syncLdapGroupMembershipWithDB')->with($ldapGroup2);

        $this->setVariablesOnClass($personsAndGroups, ['em' => $fakeEm, 'configurationManager' => $fakeConfigurationManager]);

        $personsAndGroups->syncInternetAccessLdapGroup();

        $this->assertTrue($internetOpen->getActivationCompleated());
    }

    public function testSyncInternetAccessLdapGroupFail() {
        $adPrefix = "test_";

        $fakeConfigurationManager = $this->createPartialMock(ConfigurationManager::class, ['getActiveDirectoryGeneratedGroupPrefix']);
        $fakeConfigurationManager->expects($this->atLeastOnce())->method('getActiveDirectoryGeneratedGroupPrefix')->willReturn($adPrefix);

        $ldapGroup = new LdapGroup();

        $fakeLdapGroupRepository = $this->createPartialMock(LdapGroupRepository::class,['find']);
        $fakeLdapGroupRepository->expects($this->at(0))->method('find')->with($adPrefix."InternetAccess")->willReturn($ldapGroup);
        $fakeLdapGroupRepository->expects($this->at(1))->method('find')->with($adPrefix."PersonalDeviceAccess")->willReturn($ldapGroup);

        $internetOpen = new InternetOpen();
        $internetOpen->setActivationCompleated(FALSE);

        $fakeEm = $this->getMockedEM(['LdapGroup' => $fakeLdapGroupRepository]);
        $personsAndGroups = $this->createPartialMock(LdapProxy::class, ['syncLdapGroupMembershipWithDB']);
        $personsAndGroups->expects($this->once())->method('syncLdapGroupMembershipWithDB')->with($ldapGroup)->willThrowException(new \Exception());

        $this->setVariablesOnClass($personsAndGroups, ['em' => $fakeEm, 'configurationManager' => $fakeConfigurationManager]);

        $this->expectException(\Exception::class);

        $personsAndGroups->syncInternetAccessLdapGroup();
    }


    public function testCreateOuIfNotExists() {
        $ouName = "Test Ou";
        $path = "Ou Pre";

        $nullLogger = new NullLogger();
        $fakeConfigurationManager = $this->getConfigurationManagerMock();
        $fakeMicrosoftLdapService = $this->createPartialMock(MicrosoftLdapService::class, ['setParameters',  'getConnectedServerHostname','ouExists', 'createOu']);
        $fakeMicrosoftLdapService->expects($this->once())->method('setParameters')->with([]);
        $fakeMicrosoftLdapService->expects($this->once())->method('getConnectedServerHostname')->willReturn("test");
        $fakeMicrosoftLdapService->expects($this->once())->method('ouExists')->with($ouName, $path)->willReturn(false);
        $fakeMicrosoftLdapService->expects($this->once())->method('createOu')->with($ouName, $path);

        $ldapProxy = new LdapProxy(self::$em, $fakeConfigurationManager, $fakeMicrosoftLdapService, $nullLogger);

        $ldapProxy->createOuIfNotExists($ouName, $path);
    }

    public function testMoveUserIntoOu() {
        $username = "test.user";
        $ouName = "Test Ou";
        $path = "Ou Pre";

        $nullLogger = new NullLogger();
        $fakeConfigurationManager = $this->getConfigurationManagerMock();
        $fakeMicrosoftLdapService = $this->createPartialMock(MicrosoftLdapService::class, ['setParameters',  'getConnectedServerHostname','moveUserIntoOu']);
        $fakeMicrosoftLdapService->expects($this->once())->method('setParameters')->with([]);
        $fakeMicrosoftLdapService->expects($this->once())->method('getConnectedServerHostname')->willReturn("test");
        $fakeMicrosoftLdapService->expects($this->once())->method('moveUserIntoOu')->with($ouName, $username, $path);

        $ldapProxy = new LdapProxy(self::$em, $fakeConfigurationManager, $fakeMicrosoftLdapService, $nullLogger);
        $ldapProxy->moveUserIntoOu($ouName, $username, $path);
    }

    public function testMoveGroupIntoOu() {
        $groupName = "Test Group";
        $ouName = "Test Ou";
        $path = "Ou Pre";

        $nullLogger = new NullLogger();
        $fakeConfigurationManager = $this->getConfigurationManagerMock();
        $fakeMicrosoftLdapService = $this->createPartialMock(MicrosoftLdapService::class, ['setParameters',  'getConnectedServerHostname','moveGroupIntoOu']);
        $fakeMicrosoftLdapService->expects($this->once())->method('setParameters')->with([]);
        $fakeMicrosoftLdapService->expects($this->once())->method('getConnectedServerHostname')->willReturn("test");
        $fakeMicrosoftLdapService->expects($this->once())->method('moveGroupIntoOu')->with($ouName, $groupName, $path);

        $ldapProxy = new LdapProxy(self::$em, $fakeConfigurationManager, $fakeMicrosoftLdapService, $nullLogger);
        $ldapProxy->moveGroupIntoOu($ouName, $groupName, $path);
    }

    public function testUpdateUsersIntoOu() {
        $username = "test.user";
        $ouName = "Test Ou";
        $path = "Ou Pre";

        $nullLogger = new NullLogger();
        $fakeConfigurationManager = $this->getConfigurationManagerMock();
        $fakeMicrosoftLdapService = $this->createPartialMock(MicrosoftLdapService::class, ['setParameters',  'getConnectedServerHostname','updateUsersIntoOu']);
        $fakeMicrosoftLdapService->expects($this->once())->method('setParameters')->with([]);
        $fakeMicrosoftLdapService->expects($this->once())->method('getConnectedServerHostname')->willReturn("test");
        $fakeMicrosoftLdapService->expects($this->once())->method('updateUsersIntoOu')->with($ouName, [$username], $path);

        $ldapProxy = new LdapProxy(self::$em, $fakeConfigurationManager, $fakeMicrosoftLdapService, $nullLogger);
        $ldapProxy->updateUsersIntoOu($ouName, [$username], $path);
    }

    public function testUpdateGroupsIntoOu() {
        $groupName = "Test Group";
        $ouName = "Test Ou";
        $path = "Ou Pre";

        $nullLogger = new NullLogger();
        $fakeConfigurationManager = $this->getConfigurationManagerMock();
        $fakeMicrosoftLdapService = $this->createPartialMock(MicrosoftLdapService::class, ['setParameters',  'getConnectedServerHostname','updateGroupsIntoOu']);
        $fakeMicrosoftLdapService->expects($this->once())->method('setParameters')->with([]);
        $fakeMicrosoftLdapService->expects($this->once())->method('getConnectedServerHostname')->willReturn("test");
        $fakeMicrosoftLdapService->expects($this->once())->method('updateGroupsIntoOu')->with($ouName, [$groupName], $path);

        $ldapProxy = new LdapProxy(self::$em, $fakeConfigurationManager, $fakeMicrosoftLdapService, $nullLogger);
        $ldapProxy->updateGroupsIntoOu($ouName, [$groupName], $path);
    }


    public static function tearDownAfterClass() {
        self::$em->close();
        self::$em = null;
    }

    protected static function prepareDB() {
        $metadatas = self::$em->getMetadataFactory()->getAllMetadata();
        if (!empty($metadatas)) {
            $tool = new \Doctrine\ORM\Tools\SchemaTool(self::$em);
            $tool->dropSchema($metadatas);
            $tool->createSchema($metadatas);
        }
    }
}
