<?php namespace Zen\IgrooveBundle\Tests;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

trait TestUtilityTraits {

    protected $defaultRepositories = [];

    protected function getMockedEM($repositories = [], $externalMethods = []) {
        $methods = array_unique(array_merge(['getRepository', 'getClassMetadata', 'persist', 'flush'] , $externalMethods));
        $fakeEm = $this->getMockBuilder(EntityManager::class)
            ->setMethods($methods)
            ->disableOriginalConstructor()
            ->getMock();

        foreach ($this->defaultRepositories as $repositoryName) {
            if(!isset($repositories[$repositoryName])) {
                $repositories[$repositoryName] = $this->getMockBuilder(EntityRepository::class)
                    ->disableOriginalConstructor()->getMock();
            }
        }

        $fakeEm->expects($this->any())
            ->method('getRepository')
            ->with($this->anything())
            ->will($this->returnCallback(function ($entityName) use($repositories) {
                $entityName = str_replace("ZenIgrooveBundle:", "", $entityName);
                return isset($repositories[$entityName]) ? $repositories[$entityName] : "";
            }));

        if(!in_array('persist', $externalMethods)) {
            $fakeEm->expects($this->any())->method('persist')->will($this->returnValue(null));
        }

        if(!in_array('flush', $externalMethods)) {
            $fakeEm->expects($this->any())->method('flush')->will($this->returnValue(NULL));
        }

        if(!in_array('getClassMetadata', $externalMethods)) {
            $fakeEm->expects($this->any())->method('getClassMetadata')->will($this->returnValue((object)array('name' => 'aClass')));
        }

        return $fakeEm;
    }

    protected function setVariablesOnClass($class, $variables=[]) {
        $classReflec = new \ReflectionClass($class);

        foreach ($variables as $k => $v) {
            $classProp = $classReflec->getProperty($k);
            $classProp->setAccessible(true);
            $classProp->setValue($class, $v);
        }

        return $class;
    }

}