<?php

namespace Zen\IgrooveBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use \IMAG\LdapBundle\Event\LdapUserEvent;
use Zen\IgrooveBundle\Exception\NeedPasswordChangeAuthenticationException;

class LdapSecuritySubscriber implements EventSubscriberInterface {

    public static function getSubscribedEvents()
    {
        return array(
            \IMAG\LdapBundle\Event\LdapEvents::PRE_BIND => 'onPreBind',
        );
    }

    /**
     * Modifies the User before binding data from LDAP
     *
     * @param \IMAG\LdapBundle\Event\LdapUserEvent $event
     * @throws \Exception
     */
    public function onPreBind(LdapUserEvent $event)
    {
        $user = $event->getUser();

//        if((int)$user->getAttribute('useraccountcontrol') < 1 || (int)$user->getAttribute('useraccountcontrol') & 0x0002) {
//            throw new LockedException(sprintf('LDAP user %s is disabled', $user->getUsername()));
//        }

        if($user->getAttribute('pwdlastset') != "" && (int)$user->getAttribute('pwdlastset') < 1) {
            throw new NeedPasswordChangeAuthenticationException($user, "The password for the user {$user->getUsername()} is expired.");
        }
    }
}