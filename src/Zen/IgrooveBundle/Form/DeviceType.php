<?php

namespace Zen\IgrooveBundle\Form;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class DeviceType extends AbstractType
{
    private $entityManager;

    public function __construct(ObjectManager $entityManager) // Create the constructor if not exist and add the entity manager as first parameter (we will add it later)
    {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('device', null, array('label' => 'Dispositivo'))
            ->add('mac', null, array('label' => 'Indirizzo MAC'))
            ->add('mikrotikList', null, array(
                'label' => 'Lista IP su mikrotik (opzionale)',
                'required' => false,
                'query_builder' => function ($er) {
                    return $er->createQueryBuilder('p')->orderBy('p.nome', 'ASC');
            }))
            ->add('ips', 'collection', array(
                'label' => 'Indirizzi IP riservati (opzionale)',
                'required' => false,
                'by_reference' => FALSE,
                'allow_add'    => true,
                'allow_delete' => true,
                'type' => new DeviceIpType($this->entityManager)
            ))
            ->add('bypassHotspot', null, array('label' => 'Attiva bypass su hotspot'))
            ->add('active', null, array('label' => 'Attivo'));

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $data = $event->getData();
            if(isset($data['mac'])) {
                $data['mac'] = strtoupper(preg_replace("/^(?:([a-fA-F0-9]{2})[:|\-]?)(?:([a-fA-F0-9]{2})[:|\-]?)(?:([a-fA-F0-9]{2})[:|\-]?)(?:([a-fA-F0-9]{2})[:|\-]?)(?:([a-fA-F0-9]{2})[:|\-]?)(?:([a-fA-F0-9]{2})[:|\-]?)/","$1:$2:$3:$4:$5:$6",$data['mac']));
                $event->setData($data);
            }
        });
    }

    public function getName()
    {
        return 'zen_igroovebundle_mactype';
    }

}
