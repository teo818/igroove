<?php

namespace Zen\IgrooveBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MikrotikPoolType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nome')
            ->add('mikrotik')
            ->add('dhcpServerName', 'text', ['attr' => ['input_group' => ['button_append' => [
                'name' => 'searchDhcpServerNameBt',
                'type' => 'button',
                'options' => [
                    'label' => 'Cerca',//<span class="glyphicon glyphicon-search"></span>
//                    'attr' => ['data-toggle' => 'modal', 'data-target' => '#searchDhcpServerNameModal']
                ]
            ]]]])
            ->add('dottedIpStart')
            ->add('dottedIpEnd')
            ->add('creareSuMikrotik', null, array('label' => 'Creare automaticamente su mikrotik'));

    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Zen\IgrooveBundle\Entity\MikrotikPool'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'zen_igroovebundle_mikrotikpool';
    }
}
