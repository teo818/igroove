<?php

namespace Zen\IgrooveBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MikrotikType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ip')
            ->add('apiUsername')
            ->add('apiPassword','password', ['label' => 'Api Password (lasciare bianca per non modificare)', 'required' => false, 'always_empty' => false, 'attr' => ["autocomplete" => "off"]])
//            ->add('syncSuffix', 'text', ['label' => 'Stringa di sincronizzazione (aggiungere per utilizzare più iGroove sullo stesso mikrotik'])
            ->add('useBypass')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Zen\IgrooveBundle\Entity\Mikrotik'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'zen_igroovebundle_mikrotik';
    }
}
