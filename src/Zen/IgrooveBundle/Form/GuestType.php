<?php

namespace Zen\IgrooveBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class GuestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('lastname', null, array('label' => 'Cognome'))
                ->add('firstname', null, array('label' => 'Nome'))
                ->add('identified_by', null, array('label' => 'Identificato attraverso'))
                ->add('starting_from', null, array('widget' => 'single_text', 'label' => 'Accesso attivo dal'))
                ->add('ending_to', null, array('widget' => 'single_text', 'label' => 'Fino al'))
        ;
    }

    public function getName()
    {
        return 'zen_igroovebundle_guesttype';
    }

}
