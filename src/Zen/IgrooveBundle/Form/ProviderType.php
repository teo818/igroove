<?php

namespace Zen\IgrooveBundle\Form;

use Dompdf\Adapter\CPDF;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Zen\IgrooveBundle\Manager\ConfigurationManager;

class ProviderType extends AbstractType
{
    protected $filterProviderData;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array('label' => 'Nome del provider:', 'required' => false))
            ->add('filter', 'choice',
                array(
                    'choices' => $this->generateFilterSelect(),
                    'label' => 'Processare i dati secondo lo standard:',
                    'required' => false
                )
            )
            ->add('active', 'checkbox', array('label' => 'E\' attivo questo provider', 'required' => false));

        $this->generateFilterForm($builder);

        $builder
            ->add('studentAutoCreateUsername', 'checkbox',
                array(
                    'label' => 'Generare automaticamente gli username? (disabilitare in caso siano forniti dal provider).',
                    'required' => false
                )
            )
            ->add('studentForceImportedPassword', 'checkbox',
                array('label' => 'ReImpostare la password proveniente dal provider ad ogni importazione?', 'required' => false)
            )
            ->add('studentLdapCreateOU', 'checkbox',
                array('label' => 'Crea le unità organizzative', 'required' => false)
            )
            ->add('studentLdapOUPrefix', 'text',
                array('label' => 'Prefisso al nome delle unità organizzative', 'required' => false, 'trim' => false)
            )
            ->add('studentLdapOUPath', 'text',
                array('label' => 'Percorso delle unità organizzative', 'required' => false)
            )
            ->add('studentGoogleAppClientId', 'text', ['label' => "ClientId Google Apps (lasciare vuoto per disattivare)", 'required' => false])
            ->add('studentGoogleAppDomain', 'text', ['label' => "Dominio Google Apps", 'required' => false])
            ->add('studentGoogleAppClientSecret', 'text', ['label' => "Client Secret Google Apps", 'required' => false])
            ->add('studentGoogleAppAutoEmail', 'checkbox',
                array('label' => 'Aggiungi automaticamente l\'indirizzo email se manca', 'required' => false)
            )
            ->add('studentGoogleAppAutoEmailStyle', 'choice',
                array('label' => 'Sintassi dell\'indirizzo email: (se diversa dall\'username)', 'required' => false,
                      'choices' => ConfigurationManager::$usernameStyles
                ))
            ->add('studentGoogleAppCreateGroup', 'checkbox',
                array('label' => 'Crea i gruppi di distribuzione', 'required' => false)
            )
            ->add('studentGoogleAppCreateProviderGroup', 'checkbox',
                array('label' => 'Crea il gruppo del provider', 'required' => false)
            )
            ->add('studentGoogleAppUseUserInProviderGroup', 'checkbox',
                array('label' => 'Inserisci gli utenti al posto dei gruppi del gruppo provider', 'required' => false)
            )
            ->add('studentGoogleAppGroupPrefix', 'text',
                array('label' => 'Prefisso al nome dei gruppi di distribuzione:', 'required' => false, 'trim' => false)
            )
            ->add('studentGoogleAppGroupExtraEmail', 'textarea',
                array('label' => 'Lista di email da aggiungere in ogni gruppo di distribuzione: (separate da virgola)', 'required' => false)
            )
            ->add('studentGoogleAppProviderGroupExtraEmail', 'textarea',
                array('label' => 'Lista di email da aggiungere nel gruppo del provider: (separate da virgola)', 'required' => false)
            )
            ->add('studentGoogleAppCreateOU', 'checkbox',
                array('label' => 'Crea le unità organizzative', 'required' => false)
            )
            ->add('studentGoogleAppOUPrefix', 'text',
                array('label' => 'Prefisso al nome delle unità organizzative', 'required' => false)
            )
            ->add('studentGoogleAppOUPath', 'text',
                array('label' => 'Percorso delle unità organizzative', 'required' => true, 'empty_data' => "/")
            );

        $builder
            ->add('teacherAutoCreateUsername', 'checkbox',
                array(
                    'label' => 'Generare automaticamente gli username? (disabilitare in caso siano forniti dal provider).',
                    'required' => false
                )
            )
            ->add('teacherForceImportedPassword', 'checkbox',
                array('label' => 'ReImpostare la password proveniente dal provider ad ogni importazione?', 'required' => false)
            )
            ->add('teacherLdapCreateOU', 'checkbox',
                array('label' => 'Crea l\'unità organizzativa', 'required' => false)
            )
            ->add('teacherLdapOUPrefix', 'text',
                array('label' => 'Nome dell\'unità organizzativa', 'required' => false, 'trim' => false, 'empty_data' => "Teachers")
            )
            ->add('teacherLdapOUPath', 'text',
                array('label' => 'Percorso delle unità organizzativa', 'required' => false)
            )
            ->add('teacherGoogleAppClientId', 'text', ['label' => "ClientId Google Apps (lasciare vuoto per disattivare)", 'required' => false])
            ->add('teacherGoogleAppDomain', 'text', ['label' => "Dominio Google Apps", 'required' => false])
            ->add('teacherGoogleAppClientSecret', 'text', ['label' => "Client Secret Google Apps", 'required' => false])
            ->add('teacherGoogleAppAutoEmail', 'checkbox',
                array('label' => 'Aggiungi automaticamente l\'indirizzo email se manca', 'required' => false)
            )
            ->add('teacherGoogleAppAutoEmailStyle', 'choice',
                array('label' => 'Sintassi dell\'indirizzo email: (se diversa dall\'username)', 'required' => false,
                      'choices' => ConfigurationManager::$usernameStyles
            ))
            ->add('teacherGoogleAppCreateGroup', 'checkbox',
                array('label' => 'Crea i gruppi di distribuzione', 'required' => false)
            )
            ->add('teacherGoogleAppCreateProviderGroup', 'checkbox',
                array('label' => 'Crea il gruppo del provider', 'required' => false)
            )
            ->add('teacherGoogleAppUseUserInProviderGroup', 'checkbox',
                array('label' => 'Inserisci gli utenti al posto dei gruppi del gruppo provider', 'required' => false)
            )
            ->add('teacherGoogleAppGroupPrefix', 'text',
                array('label' => 'Prefisso al nome dei gruppi di distribuzione:', 'required' => false, 'trim' => false)
            )
            ->add('teacherGoogleAppGroupExtraEmail', 'textarea',
                array('label' => 'Lista di email da aggiungere in ogni gruppo di distribuzione: (separate da virgola)', 'required' => false)
            )
            ->add('teacherGoogleAppProviderGroupExtraEmail', 'textarea',
                array('label' => 'Lista di email da aggiungere nel gruppo del provider: (separate da virgola)', 'required' => false)
            )
            ->add('teacherGoogleAppCreateOU', 'checkbox',
                array('label' => 'Crea l\'unità organizzativa', 'required' => false)
            )
            ->add('teacherGoogleAppOUPrefix', 'text',
                array('label' => 'Nome dell\'unità organizzativa', 'required' => false, 'empty_data' => "Teachers")
            )
            ->add('teacherGoogleAppOUPath', 'text',
                array('label' => 'Percorso delle unità organizzativa', 'required' => true, 'empty_data' => "/")
            );

        $builder
            ->add('internetTeachersControl', 'checkbox',
                array('label' => 'Gli insegnanti possono controllare l\'accesso ad internet degli studenti?', 'required' => false)
            )
            ->add('internetOpenAccessRange', 'textarea',
                array('label' => 'Orari in cui internet è aperto per tutti gli studenti:', 'required' => false)
            );

        $pageSizes = [];
        foreach (CPDF::$PAPER_SIZES as $name => $size) {
            $pageSizes[$name] = $name;
        }

        $builder
            ->add('pdfHeader', 'ckeditor', ['label' => 'Testata nelle comunicazioni stampate', 'required' => false])
            ->add('pdfHeaderHeight', 'integer', ['label' => 'Altezza in px della testata', 'required' => FALSE])
            ->add('pdfFooter', 'ckeditor', ['label' => 'Fondo pagina nelle comunicazioni stampate', 'required' => false])
            ->add('pdfFooterHeight', 'integer', ['label' => 'Altezza in px del fondo pagina', 'required' => FALSE])
            ->add('pdfStudentBadge', 'ckeditor', ['label' => 'Testo badge studenti', 'required' => false])
            ->add('pdfStudentBadgePageSize', 'choice', ['label' => "Tipologia di carta per badge studenti", 'choices' => $pageSizes, 'preferred_choices' => ['a4']])
            ->add('pdfStudentBadgePageLandscape', 'checkbox', ['label' => "Orientamento Orizzontale della carta per badge studenti", 'required' => FALSE])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Zen\IgrooveBundle\Entity\Provider'
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'zen_igroovebundle_provider';
    }

    /**
     * @param mixed $filterProviderData
     */
    public function setFilterProviderData($filterProviderData) {
        $this->filterProviderData = $filterProviderData;
    }

    protected function generateFilterSelect() {
        $options = [];
        foreach($this->filterProviderData as $id => $data) {
            $options[$id] = $data['name'];
        }

        return $options;
    }

    protected function generateFilterForm(FormBuilderInterface $builder) {
        $container = $builder->create("filterDataCont", null, ['compound' => true, 'by_reference' => false, 'mapped' => false, 'label' => " "]);
        foreach($this->filterProviderData as $id => $data) {
            $form = $builder->create($id,null,['compound' => true, 'by_reference' => false, 'mapped' => false, 'label' => " "]);
            foreach($data['ui'] as $uiId => $uiData) {
                if($uiData['type'] == "password")
                    $form->add($uiId,$uiData['type'],['label' => $uiData['title'], 'always_empty' => false]);
                else
                    $form->add($uiId,$uiData['type'],['label' => $uiData['title']]);
            }
            $container->add($form);
        }
        $builder->add($container);
    }
}
