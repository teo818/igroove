<?php

namespace Zen\IgrooveBundle\Form;

use Sonata\AdminBundle\Form\Type\ModelHiddenType;
use Sonata\DoctrineORMAdminBundle\Model\ModelManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormTypeInterface;
use Zen\IgrooveBundle\Entity\Group;
use Zen\IgrooveBundle\Entity\Provider;
use Zen\IgrooveBundle\Entity\Student;

class StudentType extends AbstractType
{

    protected $modelManager;

    function __construct(ModelManager $modelManager)
    {
        $this->modelManager = $modelManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entity = $builder->getData();
        $newEntity = $imported = $autoUsername = $autoEmail = false;
        if($entity instanceof Student) {
            if($entity->getId() == null) {
                $newEntity = TRUE;
            }

            $imported = ($entity->getIdOnProvider() != "");

            $provider = $entity->getProvider();
            if($provider instanceof Provider) {
                $autoUsername = $provider->getStudentAutoCreateUsername();
                $autoEmail = $provider->getStudentGoogleAppAutoEmail();
            }
        }

        $builder
                ->add('fiscalCode', 'text', array('label' => 'Codice Fiscale', 'required' => true))
                ->add('lastname', null, array('label' => 'Cognome', 'required' => true, 'disabled' => $imported)) //disabilito se importati da provider (id_on_provider non nullo)
                ->add('firstname', null, array('label' => 'Nome', 'required' => true, 'disabled' => $imported)) //disabilito se importati da provider
                ->add('username', null, array('label' => 'Username'.($autoUsername?" (Lasciare vuoto per generare automaticamente)":"")))  // , 'disabled' => $autoUsername
                ->add('email', null, array('label' => 'E-Mail'.($autoEmail?" (Lasciare vuoto per generare automaticamente)":""))) // , 'disabled' => $autoEmail
                ->add('provider', 'entity', ['label' => "Provider", 'class' => "Zen\IgrooveBundle\Entity\Provider",
                                         'choice_label' => 'name', 'required' => true ]) //, 'disabled' => $imported
                ->add('additionalProviders', 'entity', [
                    'label' => "Provider Aggiuntivi",
                    'required' => false,
                    'multiple' => true,
                    'expanded' => true,
                    'class' => Provider::class
                ])
                ->add('memberOf', 'collection', array(
                    'label' => 'Gruppi/Classi',
                    'allow_add' => true,
                    'allow_delete' => true,
                    'required' => false,
//                    'by_reference' => FALSE,
                    'type' => "sonata_type_model_hidden",
                    'options' => ['model_manager' => $this->modelManager, 'class' => Group::class]
                    )
                )
        ;
    }

    public function getName()
    {
        return 'zen_igroovebundle_studenttype';
    }

}
