<?php

namespace Zen\IgrooveBundle\ImporterFilter;

use Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Group;
use Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Sector;
use Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Student;
use Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Subject;
use Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Teacher;

abstract class AbstractFilter {

    protected $uri;
    static $name;
    static $internalName;
    protected $parameters = [];
    static $parametersUi = [];

    protected $students = [];
    protected $groups = [];
    protected $teachers = [];
    protected $subjects = [];
    protected $sectors = [];
    protected $teacherSubjectGroupRelation = [];

    protected $isManualImport = false;

    abstract function parseRemoteData();

    public function setUri($uri)
    {
        $this->uri = $uri;
    }

    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * @return Student[]
     */
    public function getStudents() {
        return $this->students;
    }

    /**
     * @return Group[]
     */
    public function getGroups() {
        return $this->groups;
    }

    /**
     * @return Teacher[]
     */
    public function getTeachers() {
        return $this->teachers;
    }

    /**
     * @return Subject[]
     */
    public function getSubjects() {
        return $this->subjects;
    }

    /**
     * @return Sector[]
     */
    public function getSectors() {
        return $this->sectors;
    }

    /**
     * @return array
     */
    public function getTeacherSubjectGroupRelation() {
        return $this->teacherSubjectGroupRelation;
    }

    /**
     * @return bool
     */
    public function isManualImport() {
        return $this->isManualImport;
    }

    /**
     * @param bool $isManualImport
     */
    public function setIsManualImport($isManualImport) {
        $this->isManualImport = $isManualImport;
    }



}