<?php
namespace Zen\IgrooveBundle\ImporterFilter;


class DatabaseInterno extends AbstractFilter
{
    static $name = "Database Interno";
    static $internalName = "databaseinterno";

    public function setUri($uri) {}

    public function setSecretKey($key) {}

    public function parseRemoteData() {}
}