<?php

namespace Zen\IgrooveBundle\ImporterFilter;

use Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Teacher;
use Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Student;
use Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Group;

class Gsd extends AbstractFilter
{
    public static $name = 'SEED GSD';
    public static $internalName = 'gsd';
    public static $parametersUi = ['uri' => ['title' => 'URI della fonte dati', 'type' => 'text'], 'secretKey' => ['title' => 'Chiave Segreta', 'type' => 'text']];
    protected $dataUri;
    protected $secretKeyWithSalt;

    protected $guzzle;

    public function __construct($guzzle)
    {
        $this->guzzle = $guzzle;
    }

    public function setParameters($parameters)
    {
        parent::setParameters($parameters);
        $this->updateDataUri();
    }

    /*
     * Come concordato, per ottenere i dati occorre passare il parametro
     * k = md5((KEY + SYSDATE))
     * che funge da chiave di autenticazione.
     * Con
     * KEY     = chiave
     * SYSDATE = data attuale nel formato YYYYmmdd     (esempio per oggi 20141127)
     */

    private function updateSalt()
    {
        $this->secretKeyWithSalt = md5($this->parameters['secretKey'].date('Ymd'));
    }

    private function updateDataUri()
    {
        $this->updateSalt();
        $this->dataUri = $this->parameters['uri'].'?k='.$this->secretKeyWithSalt;
    }

    public function parseRemoteData()
    {
        $request = $this->guzzle->get($this->dataUri);
        $response = $request->send();
        $resultArray = $response->json();
        if ($resultArray['status'] != 'success') {
            return;
        }

        $listDn = array();

        foreach ($resultArray['payload'] as $row) {
            $prefisso = '';
            if ($row['tipo'] == 'docente') {
                $prefisso = 'DOC';
            }
            if ($row['tipo'] == 'studente') {
                $prefisso = 'STU';
            }
            if (strlen($row['username']) == 0) {
                continue;
            }
            if ((strtoupper(substr($row['username'], 0, 3)) == 'STU') or ((strtoupper(
                        substr($row['username'], 0, 3)
                    )) == 'DOC')
            ) {
                $username = $row['username'];
            } else {
                $username = $prefisso.$row['username'];
            }
            $i = 3;

            while (substr($username, $i, 1) == '0') {
                $username = substr($username, 0, 3).substr($username, $i + 1);
            }

            $password = trim(
                substr(
                    $this->getDecrypt(
                        $this->pkcs5_pad(base64_decode($row['password']), 16),
                        substr(sha1($this->parameters['secretKey'], true), 0, 16)
                    ),
                    0,
                    strlen(base64_decode($row['password']))
                )
            );
            $password = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $password);

            if (strlen(trim($password)) == 0) {
                continue;
            }

            $dn = trim(ucwords(strtolower($row['nome']))).' '.trim(ucwords(strtolower($row['cognome'])));

            if (in_array($dn, $listDn)) {
                $row['nome'] = trim(ucwords(strtolower($row['nome']))).' ('.strtoupper($username).')';
                $dn = trim(ucwords(strtolower($row['nome']))).' '.trim(ucwords(strtolower($row['cognome'])));
            }
            $listDn[] = $dn;
            $group = trim(ucwords(strtolower($row['tipo'])));

            if ($row['tipo'] == 'docente') {
                $this->teachers[strtolower($username)] = new Teacher(
                    strtolower($username),
                    strtolower($username),
                    trim(ucwords(strtolower($row['nome']))),
                    trim(ucwords(strtolower($row['cognome']))),
                    trim(strtolower($row['email'])),
                    strtoupper($username),
                    $password
                );
            }

            if ($row['tipo'] == 'studente') {
                $this->students[strtolower($username)] = new Student(
                    strtolower($username),
                    strtolower($username),
                    trim(ucwords(strtolower($row['nome']))),
                    trim(ucwords(strtolower($row['cognome']))),
                    $group,
                    trim(strtolower($row['email'])),
                    strtoupper($username),
                    $password
                );
                $this->groups[$group] = new Group(strtolower($group), $group, 0);
            }
        }
    }

    private function pkcs5_pad($text, $blocksize)
    {
        $pad = $blocksize - (strlen($text) % $blocksize);

        return $text.str_repeat(chr($pad), $pad);
    }

    private function getDecrypt($sStr, $sKey)
    {
        return mcrypt_decrypt(
            MCRYPT_RIJNDAEL_128,
            $sKey,
            ($sStr),
            MCRYPT_MODE_ECB
        );
    }
}
