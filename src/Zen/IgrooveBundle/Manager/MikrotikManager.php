<?php

namespace Zen\IgrooveBundle\Manager;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr\Join;
use PEAR2\Net\RouterOS\Client as Ros;
use PEAR2\Net\RouterOS\Request as RosRequest;
use PEAR2\Net\RouterOS\Query as RosQuery;

class MikrotikManager
{
    protected $mikrotiks;
    /**
     * @var Ros[]
     */
    protected $mikrotikClient = [];
    protected $userIntoMikrotik;
    protected $internetAccessUsers;
    /**
     * @var EntityManager
     */
    protected $em;

    static $bypassedIpListName = "Bypassed Ip";

    public function __construct($configurationManager, $em)
    {
        $this->em = $em;
        $this->configurationManager = $configurationManager;
        $this->loadClientsHandler();
    }

    /**
     * kick off the users not currently active or in active group.
     */
    public function KickOffUsers()
    {
        $this->internetAccessUsers = $groupMembersUsers = $this->em->getRepository('ZenIgrooveBundle:LdapGroup')->getAllChildrenRecursiveUsers($this->configurationManager->getActiveDirectoryGeneratedGroupPrefix().'InternetAccess');
        $userList = array();
        foreach ($groupMembersUsers as $user) {
            $userList[] = strtolower($user);
        }

        $igrooveUsers = $this->em->getRepository('ZenIgrooveBundle:Student')->getAllStudentsUsername();

        foreach ($this->mikrotikClient as $mikrotikIp => $mikrotikClient) {
            $userIntoMikrotik = $this->getUsersInHotspot($mikrotikIp);
            $usernameToRemove = array_diff(
                array_unique(array_keys($userIntoMikrotik)),
                array_unique($userList)
            );

            foreach ($usernameToRemove as $user) {
                if (strlen(trim($user)) == 0) {
                    continue;
                }
                if (!array_search($user, $igrooveUsers)) {
                    continue;
                }
                if (array_key_exists(strtolower($user), $userIntoMikrotik)) {
                    echo "\r\n --> Mikrotik kickoff ".$user;
                    $delRequest = new RosRequest('/ip/hotspot/active/remove');
                    $delRequest->setArgument('numbers', $userIntoMikrotik[strtolower($user)]);
                    $mikrotikClient->sendSync($delRequest);
                }
            }
        }
    }

    /**
     * add or remove the mac address bypassed in the hotspot of all the mikrotiks.
     *
     * @return array action executed
     */
    public function manageMacToHotspot()
    {
        $criteria = array('active' => true, 'bypassHotspot' => true);
        $entities = $this->em->getRepository('ZenIgrooveBundle:Device')->findBy($criteria);
        $macInIgroove = array();
        foreach ($entities as $entity) {
            $macInIgroove[strtoupper($entity->getMac())] = 'igroove - '.$entity->getDevice();
        }
        $result = array();
        foreach ($this->mikrotikClient as $mikrotikIp => $mikrotikClient) {
            if (array_key_exists($mikrotikIp, $this->mikrotiks) == false) {
                continue;
            }
            if ($this->mikrotiks[$mikrotikIp]->getUseBypass() == false) {
                $result[$mikrotikIp]['ROUTER'] = 'skip this router bypass';
                continue;
            }

            $macIntoMikrotik = $this->getMacsIntoHotspot($mikrotikIp);
            $macToAdd = array_diff_key($macInIgroove, $macIntoMikrotik);
            $macToRemove = array_diff_key($macIntoMikrotik, $macInIgroove);

            foreach ($macToAdd as $mac => $comment) {
                $result[$mikrotikIp][$mac] = 'Add '.$comment;

                $addRequest = new RosRequest('/ip/hotspot/ip-binding/add');
                $addRequest->setArgument('mac-address', $mac);
                $addRequest->setArgument('type', 'bypassed');
                $addRequest->setArgument('comment', $comment);
                $mikrotikClient->sendSync($addRequest);
            }
            foreach ($macToRemove as $mac => $element) {
                $result[$mikrotikIp][$mac] = 'Remove '.$element['comment'];

                $delRequest = new RosRequest('/ip/hotspot/ip-binding/remove');
                $delRequest->setArgument('numbers', $element['id']);
                $mikrotikClient->sendSync($delRequest);
            }
        }

        return $result;
    }

    /**
     * add or remove the ip addresses in the firewall addresslist.
     *
     * @return array action executed
     */
    public function addIpToIpList()
    {
        $ipsInIgroove = $this->getIgrooveIpInAddressList();

        $result = array();
        foreach ($this->mikrotikClient as $mikrotikIp => $mikrotikClient) {
            if (array_key_exists($mikrotikIp, $ipsInIgroove) == false) {
                continue;
            }

            $ipsIntoMikrotik = $this->getMikrotikIpAddresslists($mikrotikIp);
            $entriesToAdd = array_diff_key($ipsInIgroove[$mikrotikIp], $ipsIntoMikrotik);
            $entriesToRemove = array_diff_key($ipsIntoMikrotik, $ipsInIgroove[$mikrotikIp]);

            foreach ($entriesToAdd as $k => $entry) {
                $result[$mikrotikIp][$k] = 'Add '.$entry['device'].' ('.$entry['ip'].') to list '.$entry['list'];

                $comment = 'igroove - '.$entry['device'];
                $addRequest = new RosRequest('/ip/firewall/address-list/add');
                $addRequest->setArgument('address', $entry['ip']);
                $addRequest->setArgument('list', $entry['list']);
                $addRequest->setArgument('comment', $comment);
                $mikrotikClient->sendSync($addRequest);
            }

            foreach ($entriesToRemove as $k => $element) {
                $comment = $element['comment'];
                $result[$mikrotikIp][$k] = 'Remove '.$comment;

                $delRequest = new RosRequest('/ip/firewall/address-list/remove');
                $delRequest->setArgument('numbers', $element['id']);
                $mikrotikClient->sendSync($delRequest);
            }
        }

        return $result;
    }

    public function addIpToBypassedList()
    {
        $ipsInIgroove = $this->getBypassedIgrooveIp();

        $result = [];
        foreach ($this->mikrotikClient as $mikrotikIp => $mikrotikClient) {
            if(!isset($ipsInIgroove[$mikrotikIp])) {
                continue;
            }

            $ipInMikrotik = $this->getMikrotikIpInAddressList($mikrotikIp, self::$bypassedIpListName);
            $entriesToAdd = array_diff_key($ipsInIgroove[$mikrotikIp], $ipInMikrotik);
            $entriesToRemove = array_diff_key($ipInMikrotik, $ipsInIgroove[$mikrotikIp]);

            foreach ($entriesToAdd as $k => $entry) {
                $result[$mikrotikIp][$k] = 'Add '.$entry['device'].' ('.$entry['ip'].') to list '.self::$bypassedIpListName;

                $comment = 'igroove - '.$entry['device'];
                $addRequest = new RosRequest('/ip/firewall/address-list/add');
                $addRequest->setArgument('address', $entry['ip']);
                $addRequest->setArgument('list', self::$bypassedIpListName);
                $addRequest->setArgument('comment', $comment);
                $mikrotikClient->sendSync($addRequest);
            }

            foreach ($entriesToRemove as $k => $element) {
                $comment = $element['comment'];
                $result[$mikrotikIp][$k] = 'Remove '.$comment;

                $delRequest = new RosRequest('/ip/firewall/address-list/remove');
                $delRequest->setArgument('numbers', $element['id']);
                $mikrotikClient->sendSync($delRequest);
            }
        }

        $internetOpens = $this->em->getRepository('ZenIgrooveBundle:InternetOpen')->findBy(['type' => 'ipList', 'activationCompleated' => false]);
        foreach ($internetOpens as $internetOpen) {
            $internetOpen->setActivationCompleated(true);
        }
        $this->em->flush();

        return $result;
    }

    /**
     * add or remove pools with creareSuMikrotik option enabled on the mikrotiks.
     *
     * @return array
     */
    public function addPools()
    {
        $responses = array();
        foreach ($this->mikrotiks as $mikrotik) {
            try {
                $mikrotikClient = $this->getMikrotikClient($mikrotik->getIp());
            } catch (\ErrorException $e) {
                continue;
            }

            $mikrotikPools = $this->getMikrotikPools($mikrotik->getIp());
            $criteria = array('mikrotik' => $mikrotik, 'creareSuMikrotik' => true);
            $poolsInIgroove = $this->em->getRepository('ZenIgrooveBundle:MikrotikPool')->findBy($criteria);

            foreach ($poolsInIgroove as $poolInIgroove) {
                $rangesInIgroove = $poolInIgroove->getDottedIpStart().'-'.$poolInIgroove->getDottedIpEnd();

                if (array_key_exists($poolInIgroove->getNome(), $mikrotikPools)) {
                    $rangesInMikrotik = $mikrotikPools[$poolInIgroove->getNome()]['ranges'];
                    if ($rangesInIgroove == $rangesInMikrotik) {
                        continue;
                    }
                    $delRequest = new RosRequest('/ip/pool/remove');
                    $delRequest->setArgument('numbers', $mikrotikPools[$poolInIgroove->getNome()]['id']);
                    $mikrotikClient->sendSync($delRequest);
                    $responses[$mikrotik->getIp()][] = 'DEL '.$poolInIgroove->getNome().': '.$rangesInMikrotik;
                }

                $addRequest = new RosRequest('/ip/pool/add');
                $addRequest->setArgument('name', $poolInIgroove->getNome());
                $addRequest->setArgument('ranges', $rangesInIgroove);
                $mikrotikClient->sendSync($addRequest);
                $responses[$mikrotik->getIp()][] = 'ADD '.$poolInIgroove->getNome().': '.$rangesInIgroove;
            }
        }

        return $responses;
    }

    /**
     * Add ip reservation in dhcp server lease.
     */
    public function addIpReservation()
    {
        $ipsInIgroove = $this->getIgrooveDeviceIps();

        $result = array();
        foreach ($this->mikrotikClient as $mikrotikIp => $mikrotikClient) {
            $ipsInMikrotik = $ipsToRemoveFromMikrotik = $this->getMikrotikIpLeasesInDhcpServer($mikrotikIp);
            $ipsToAddToMikrotik = [];

            foreach ($ipsInIgroove[$mikrotikIp] as $deviceIpInIgroove => $ipInIgroove) {
                if (array_key_exists($deviceIpInIgroove, $ipsInMikrotik) == false) {
                    if (sizeof($ipInIgroove['ip']) > 0) {
                        $ipsToAddToMikrotik[] = $ipInIgroove;
                    }
                    continue;
                }

                if ($ipsInMikrotik[$deviceIpInIgroove]['check_string'] != $ipInIgroove['check_string']) {
                    $updRequest = new RosRequest('/ip/dhcp-server/lease/set');
                    $updRequest->setArgument('numbers', $ipsInMikrotik[$deviceIpInIgroove]['id']);
                    $updRequest->setArgument('comment', 'igroove - '.$ipInIgroove['name']);
                    $updRequest->setArgument('mac-address', $ipInIgroove['mac']);
                    $updRequest->setArgument('address', $ipInIgroove['ip']);
                    $updRequest->setArgument('server', $ipInIgroove['server']);
                    $mikrotikClient->sendSync($updRequest);

                    $result[$mikrotikIp][$deviceIpInIgroove] = 'Update '.$ipInIgroove['name'].' ('.$ipInIgroove['ip'].') in dhcp server '.$ipInIgroove['server']." | {$ipsInMikrotik[$deviceIpInIgroove]['check_string']} {$ipInIgroove['check_string']}";
                }

                unset($ipsToRemoveFromMikrotik[$deviceIpInIgroove]);
            }

            foreach ($ipsToRemoveFromMikrotik as $ipToRemove => $ipToRemoveFromMikrotik) {
                $delRequest = new RosRequest('/ip/dhcp-server/lease/remove');
                $delRequest->setArgument('numbers', $ipToRemoveFromMikrotik['id']);
                $mikrotikClient->sendSync($delRequest);

                $result[$mikrotikIp][$ipToRemove] = 'Remove '.$ipToRemoveFromMikrotik['name'].' ('.$ipToRemoveFromMikrotik['ip'].' - '.$ipToRemoveFromMikrotik['mac'].') to dhcp server '.$ipToRemoveFromMikrotik['server'];
            }

            foreach ($ipsToAddToMikrotik as $ipToAddToMikrotik) {
                $result[$mikrotikIp][$ipToAddToMikrotik['ip']] = 'Add '.$ipToAddToMikrotik['name'].' ('.$ipToAddToMikrotik['ip'].') to dhcp server '.$ipToAddToMikrotik['server'];

                $addRequest = new RosRequest('/ip/dhcp-server/lease/add');
                $addRequest->setArgument('comment', 'igroove - '.$ipToAddToMikrotik['name']);
                $addRequest->setArgument('mac-address', $ipToAddToMikrotik['mac']);
                $addRequest->setArgument('address', $ipToAddToMikrotik['ip']);
                $addRequest->setArgument('server', $ipToAddToMikrotik['server']);
                $mikrotikClient->sendSync($addRequest);
            }
        }

        return $result;
    }

    /**
     * Get Current active users in hotspot of mikrotik with specified ip.
     *
     * @param $mikrotikIp
     *
     * @return array
     */
    private function getUsersInHotspot($mikrotikIp)
    {
        $userIntoMikrotik = array();

        try {
            $mikrotikClient = $this->getMikrotikClient($mikrotikIp);
        } catch (\ErrorException $e) {
            return $userIntoMikrotik;
        }

        $responses = $mikrotikClient->sendSync(new RosRequest('/ip/hotspot/active/print'));
        $userIntoMikrotik = array();

        foreach ($responses as $element) {
            $username = explode('@', $element->getProperty('user'));
            if (empty($username) || $username[0] == '') {
                continue;
            }

            if (isset($username[1]) && strtolower($this->configurationManager->getFreeradiusRealm()) != strtolower($username[1])) {
                continue;
            } elseif (!isset($username[1]) && $this->configurationManager->getFreeradiusRealm() != '') {
                continue;
            }

            $userIntoMikrotik[$username[0]] = $element->getProperty('.id');
        }

        return $userIntoMikrotik;
    }

    /**
     * Get Current active users in all the mikrotiks hotspot.
     *
     * @return array
     */
    public function getAllUsersInHotspots()
    {
        $userIntoMikrotik = array();
        foreach ($this->mikrotikClient as $mikrotikClient) {
            $responses = $mikrotikClient->sendSync(
                new RosRequest('/ip/hotspot/active/print')
            );

            foreach ($responses as $element) {
                $userIntoMikrotik[strtolower($element->getProperty('user'))] = strtolower($element->getProperty('server'));
            }
        }

        return $userIntoMikrotik;
    }

    /**
     * @param $mikrotikIp
     *
     * @return array
     */
    public function getDhcpServersName($mikrotikIp)
    {
        try {
            $mikrotikClient = $this->getMikrotikClient($mikrotikIp);
        } catch (\ErrorException $e) {
            return [];
        }

        $responses = $mikrotikClient->sendSync(new RosRequest('/ip/dhcp-server/print'));
        $dhcpServersName = array();
        foreach ($responses as $element) {
            if ($element->getProperty('name') == '') {
                continue;
            }

            $dhcpServersName[] = $element->getProperty('name');
        }

        return $dhcpServersName;
    }

    /**
     * get the mac addresses of the devices bypassed in the hotspot of the mikrotik with ip specified.
     *
     * @param $mikrotikIp
     *
     * @return array
     */
    private function getMacsIntoHotspot($mikrotikIp)
    {
        try {
            $mikrotikClient = $this->getMikrotikClient($mikrotikIp);
        } catch (\ErrorException $e) {
            return [];
        }

        $responses = $mikrotikClient->sendSync(new RosRequest('/ip/hotspot/ip-binding/print'));
        $macIntoMikrotik = array();
        foreach ($responses as $element) {
            if (substr($element->getProperty('comment'), 0, 7) == 'igroove') {
                $macIntoMikrotik[strtoupper($element->getProperty('mac-address'))] = array(
                    'id' => $element->getProperty('.id'),
                    'comment' => $element->getProperty('comment'),
                );
            }
        }

        return $macIntoMikrotik;
    }

    /**
     * get the list of ips in all the addresslist of the mikrotik with specified ip.
     *
     * @param $mikrotikIp
     *
     * @return array
     */
    private function getMikrotikIpAddresslists($mikrotikIp)
    {
        try {
            $mikrotikClient = $this->getMikrotikClient($mikrotikIp);
        } catch (\ErrorException $e) {
            return [];
        }

        $responses = $mikrotikClient->sendSync(new RosRequest('/ip/firewall/address-list/print'));
        $ipAddresslistIntoMikrotik = array();
        foreach ($responses as $element) {
            if (substr($element->getProperty('comment'), 0, 7) == 'igroove' && $element->getProperty('list') != self::$bypassedIpListName) {
                $ipAddresslistIntoMikrotik[md5(strtolower($element->getProperty('address').$element->getProperty('list')))] = array(
                    'id' => $element->getProperty('.id'),
                    'address' => $element->getProperty('address'),
                    'list' => $element->getProperty('list'),
                    'comment' => $element->getProperty('comment'),
                );
            }
        }

        return $ipAddresslistIntoMikrotik;
    }

    private function getMikrotikIpInAddressList($mikrotikIp, $listName)
    {
        try {
            $mikrotikClient = $this->getMikrotikClient($mikrotikIp);
        } catch (\ErrorException $e) {
            return [];
        }

        $request = new RosRequest('/ip/firewall/address-list/print');
        $request->setQuery(RosQuery::where('list', $listName));
        $responses = $mikrotikClient->sendSync($request);
        $ipAddresslistIntoMikrotik = array();
        foreach ($responses as $element) {
            if (substr($element->getProperty('comment'), 0, 7) == 'igroove') {
                $ipAddresslistIntoMikrotik[md5($element->getProperty('address'))] = array(
                    'id' => $element->getProperty('.id'),
                    'address' => $element->getProperty('address'),
                    'comment' => $element->getProperty('comment'),
                );
            }
        }

        return $ipAddresslistIntoMikrotik;
    }

    /**
     * Get all ips assigned to an address list in igroove.
     *
     * @return array
     */
    private function getIgrooveIpInAddressList()
    {
        $query = $this->em->getRepository('ZenIgrooveBundle:DeviceIp')
            ->createQueryBuilder('i')
            ->select('i.ip,d.device,l.nome,mk.ip as mip')
            ->join('i.mac', 'd')
            ->join('d.mikrotikList', 'l')
            ->join('Zen\IgrooveBundle\Entity\Mikrotik', 'mk', Join::WITH, 'mk.id = l.mikrotik')
            ->where('d.active = :true')
            ->setParameter('true', true)
            ->getQuery();
        $results = $query->getResult();

        $ipInAddresslist = [];
        $mts = $this->em->getRepository('ZenIgrooveBundle:Mikrotik')->findAll();
        foreach ($mts as $m) {
            $ipInAddresslist[$m->getIp()] = [];
        }
        foreach ($results as $result) {
            if ($result['ip'] > 0) {
                $deviceIp = long2ip($result['ip']);
                $ipInAddresslist[$result['mip']][md5($deviceIp.strtolower($result['nome']))] = array(
                    'ip' => $deviceIp,
                    'list' => $result['nome'],
                    'device' => $result['device'],
                );
            }
        }

        return $ipInAddresslist;
    }

    private function getBypassedIgrooveIp() {
        $query = $this->em->getRepository('ZenIgrooveBundle:DeviceIp')
            ->createQueryBuilder('i')
            ->select('i.ip,d.device,mk.ip as mip')
            ->join('i.mac', 'd')
            ->join('d.mikrotikList', 'l')
            ->join('Zen\IgrooveBundle\Entity\Mikrotik', 'mk', Join::WITH, 'mk.id = l.mikrotik')
            ->join('Zen\IgrooveBundle\Entity\InternetOpen', 'io', Join::WITH, 'l.id = io.account')
            ->where("d.active = :true AND io.type = 'iplist' AND io.close_at > :now")
            ->setParameter('true', true)
            ->setParameter('now', new \DateTime('now'))
            ->getQuery();
        $results = $query->getResult();

        $ipInAddresslist = [];
        $mts = $this->em->getRepository('ZenIgrooveBundle:Mikrotik')->findAll();
        foreach ($mts as $m) {
            $ipInAddresslist[$m->getIp()] = [];
        }
        foreach ($results as $result) {
            if ($result['ip'] > 0) {
                $deviceIp = long2ip($result['ip']);
                $ipInAddresslist[$result['mip']][md5($deviceIp)] = array(
                    'ip' => $deviceIp,
                    'device' => $result['device'],
                );
            }
        }

        return $ipInAddresslist;
    }

    /**
     * Get all ip currently set from igroove in dhcp server as lease.
     *
     * @param $mikrotikIp
     *
     * @return array
     */
    private function getMikrotikIpLeasesInDhcpServer($mikrotikIp)
    {
        try {
            $mikrotikClient = $this->getMikrotikClient($mikrotikIp);
        } catch (\ErrorException $e) {
            return [];
        }

        $responses = $mikrotikClient->sendSync(new RosRequest('/ip/dhcp-server/lease/print'));
        $ipIntoMikrotik = array();
        foreach ($responses as $element) {
            if (substr($element->getProperty('comment'), 0, 7) == 'igroove') {
                $name = substr($element->getProperty('comment'), 10);
                $mac = strtoupper($element->getProperty('mac-address'));
                $ipIntoMikrotik[$element->getProperty('address')] = array(
                    'id' => $element->getProperty('.id'),
                    'ip' => $element->getProperty('address'),
                    'name' => $name,
                    'mac' => $mac,
                    'server' => $element->getProperty('server'),
                    'check_string' => md5(strtolower($element->getProperty('address').$name.$element->getProperty('mac-address').$element->getProperty('server'))),
                );
            }
        }

        return $ipIntoMikrotik;
    }

    /**
     * get the list of device ips currently in igroove.
     *
     * @return array
     */
    private function getIgrooveDeviceIps()
    {
        $query = $this->em->getRepository('ZenIgrooveBundle:DeviceIp')
            ->createQueryBuilder('i')
            ->select('i.ip,d.device,d.mac,p.dhcpServerName,mk.ip as mip')
            ->join('i.mac', 'd')
            ->join('i.mikrotikPool', 'p')
            ->join('Zen\IgrooveBundle\Entity\Mikrotik', 'mk', Join::WITH, 'mk.id = p.mikrotik')
            ->where('d.active = :true')
            ->setParameter('true', true)
            ->getQuery();
        $results = $query->getResult();
        $mts = $this->em->getRepository('ZenIgrooveBundle:Mikrotik')->findAll();
        foreach ($mts as $m) {
            $ipInIgroove[$m->getIp()] = [];
        }
        foreach ($results as $result) {
            if ($result['ip'] > 0) {
                $deviceIp = long2ip($result['ip']);
                $mac = strtoupper($result['mac']);
                $ipInIgroove[$result['mip']][$deviceIp] = array(
                    'ip' => $deviceIp,
                    'name' => $result['device'],
                    'mac' => $mac,
                    'server' => $result['dhcpServerName'],
                    'check_string' => md5(strtolower($deviceIp.$result['device'].$result['mac'].$result['dhcpServerName'])),
                );
            }
        }

        return $ipInIgroove;
    }

    /**
     * get the dhcp pools in the mikrotik with specified ip.
     *
     * @param $mikrotikIp
     *
     * @return array
     */
    private function getMikrotikPools($mikrotikIp)
    {
        $poolsIntoMikrotik = array();

        try {
            $mikrotikClient = $this->getMikrotikClient($mikrotikIp);
        } catch (\ErrorException $e) {
            return $poolsIntoMikrotik;
        }

        $responses = $mikrotikClient->sendSync(new RosRequest('/ip/pool/print'));
        foreach ($responses as $element) {
            if ($element->getProperty('name')) {
                $poolsIntoMikrotik[($element->getProperty('name'))] = array(
                    'id' => $element->getProperty('.id'),
                    'name' => $element->getProperty('name'),
                    'ranges' => $element->getProperty('ranges'),
                    'comment' => $element->getProperty('comment'),
                );
            }
        }

        return $poolsIntoMikrotik;
    }

    /**
     * reboot all the mikrotiks.
     *
     * @return array
     */
    public function rebootMikrotiks()
    {
        $messages = array();
        foreach ($this->mikrotikClient as $ip => $mikrotikClient) {
            $rebootRequest = new RosRequest('/system/reboot');
            $rebootRequest->setTag('rebootFromIgroove');
            $mikrotikClient->sendAsync($rebootRequest);
            $messages[] = 'Reboot '.$ip;
        }

        return $messages;
    }

    /**
     * load the mikrotiks client.
     */
    protected function loadClientsHandler()
    {
        $mts = $this->em->getRepository('ZenIgrooveBundle:Mikrotik')->findAll();
        foreach ($mts as $m) {
            $this->mikrotiks[$m->getIp()] = $m;
        }
        $this->mikrotikClient = array();
        if ($this->mikrotiks) {
            foreach ($this->mikrotiks as $mikrotik) {
                try {
                    $this->mikrotikClient[$mikrotik->getIp()] = new Ros(
                        $mikrotik->getIp(),
                        $mikrotik->getApiUsername(),
                        $mikrotik->getApiPassword()
                    );
                    $mikrotik->setStatus(true);
                } catch (\Exception $e) {
                    unset($this->mikrotikClient[$mikrotik->getIp()]);
                    $mikrotik->setStatus(false);
                    continue;
                }
            }
            $this->em->flush();
        }
    }

    /**
     * @param $mikrotikIp
     *
     * @return Ros
     *
     * @throws \ErrorException
     */
    protected function getMikrotikClient($mikrotikIp)
    {
        if ((array_key_exists($mikrotikIp, $this->mikrotikClient) == false) || !$this->mikrotikClient[$mikrotikIp] instanceof Ros) {
            throw new \ErrorException("Mikrotik with ip {$mikrotikIp} not accessible");
        }

        return $this->mikrotikClient[$mikrotikIp];
    }

    /**
     * @return Ros[]
     */
    protected function getMikrotikClients()
    {
        return $this->mikrotikClient;
    }

    protected function checkSyncKey($stringToCheck) {

    }
}
