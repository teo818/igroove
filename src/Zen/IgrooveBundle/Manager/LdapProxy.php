<?php

namespace Zen\IgrooveBundle\Manager;

use adldap\adLDAPException;
use Zen\IgrooveBundle\Entity\LdapGroup;
use Zen\IgrooveBundle\Entity\LdapUser;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Zen\IgrooveBundle\Exception\LoggedException;

class LdapProxy
{
    private $ldapProxy;
    private $logger;
    protected $em;
    protected $configurationManager;
    protected $groupRepository;
    protected $studentRepository;
    protected $ldapGroupRepository;
    protected $ldapUserRepository;

    /**
     * LdapProxy constructor.
     *
     * @param EntityManager $em
     * @param ConfigurationManager $configurationManager
     * @param MicrosoftLdapService $MicrosoftLdapService
     * @param LoggerInterface $logger
     */
    public function __construct(EntityManager $em, ConfigurationManager $configurationManager, MicrosoftLdapService $MicrosoftLdapService, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->configurationManager = $configurationManager;
        $this->ldapProxy = $MicrosoftLdapService;
        $this->ldapProxy->setParameters($configurationManager->getActiveDirectoryConfiguration());
        $this->groupRepository = $this->em->getRepository('ZenIgrooveBundle:Group');
        $this->studentRepository = $this->em->getRepository('ZenIgrooveBundle:Student');
        $this->ldapGroupRepository = $this->em->getRepository('ZenIgrooveBundle:LdapGroup');
        $this->ldapUserRepository = $this->em->getRepository('ZenIgrooveBundle:LdapUser');

        $this->logger->debug("LDAP: Connected to ".$this->ldapProxy->getConnectedServerHostname());
    }

    /**
     * Clear all the LdapUser and LdapGroup from database,
     * and regenerate it from AD
     */
    public function purgeAndPopulateAll()
    {
        $this->logger->info("LDAPtoDB: started");
        $this->refreshConnection();
        $ldapGroups = $this->ldapGroupRepository->findAll();
        foreach ($ldapGroups as $ldapGroup) {
            $this->em->remove($ldapGroup);
        }
        $ldapUsers = $this->ldapUserRepository->findAll();
        foreach ($ldapUsers as $ldapUser) {
            $this->em->remove($ldapUser);
        }

        $this->logger->info("LDAPtoDB: database flushed");

        $this->em->flush();

        $this->populateGroups();
        $this->populateUsers();
        $this->populateMembership();
        $this->logger->info("LDAPtoDB: ended");
    }


    /**
     * Recreate all the LdapGroup reading the current groups in AD
     */
    protected function populateGroups()
    {
        $groups = $this->ldapProxy->getAllGroups();
        if ($groups) {
            foreach ($groups as $group) {
                $ldapGroup = new LdapGroup();
                $ldapGroup->setName($group);
                $this->em->persist($ldapGroup);
            }

            try {
                $this->em->flush();
            } catch (\Throwable $e) {
                $this->logger->error("LDAPtoDB-populateGroups: Error during import of users: ".$e->getMessage());
            }
        }

        $this->logger->info("LDAPtoDB: group database populated with ".count($groups)." entry");
    }


    /**
     * Recreate all the LdapUser reading the current users created by iGroove in AD
     */
    protected function populateUsers()
    {
        $users = $this->ldapProxy->getAllUsers();
        if ($users) {
            foreach ($users as $user) {
                $ldapUser = new LdapUser();
                $ldapUser->setDn($user['dn']);
                $ldapUser->setUsername($user['username']);
                $ldapUser->setFirstname($user['firstname']);
                $ldapUser->setLastname($user['lastname']);
                $ldapUser->setDistinguishedId($user['distinguishedId']);
                $ldapUser->setAttributes(json_encode($user['parameters']));
                $this->em->persist($ldapUser);
            }

            try {
                $this->em->flush();
            } catch (\Throwable $e) {
                $this->logger->error("LDAPtoDB-populateUsers: Error during import of users: ".$e->getMessage());
            }
        }

        $this->logger->info("LDAPtoDB: user database populated with ".count($users)." entry");
    }

    /**
     * Reinsert all the member/group of a LdapGroup present in AD
     */
    protected function populateMembership()
    {
        $ldapUsers = $this->ldapUserRepository->findAll();
        foreach ($ldapUsers as $ldapUser) {
            $groups = $this->ldapProxy->getUserMembership($ldapUser->getUsername());
            if (is_array($groups)) {
                foreach ($groups as $group) {
                    $ldapGroup = $this->ldapGroupRepository->find($group);
                    if ($ldapGroup) {
                        $ldapGroup->addMember('user', $ldapUser->getUsername());
                        $ldapGroup->setOperation(null);
                    }
                }
            }
        }
        $this->logger->info("LDAPtoDB: user membership updated");

        $ldapGroups = $this->ldapGroupRepository->findAll();
        foreach ($ldapGroups as $ldapGroup) {
            $groups = $this->ldapProxy->getGroupMembership($ldapGroup->getName());
            foreach ($groups as $groupName) {
                $ldapGroup->addMember('group', $groupName);
                $ldapGroup->setOperation(null);
            }
        }

        try {
            $this->em->flush();
        } catch (\Throwable $e) {
            $this->logger->error("LDAPtoDB-populateMembership: Error during import of users: ".$e->getMessage());
        }

        $this->logger->info("LDAPtoDB: group membership updated");
    }

    /**
     * Execute all the operation of the LdapUsers an LdapGroups to AD
     */
    public function syncLDAPfromDB()
    {
        $this->logger->info("DBtoLDAP: started");
        $this->em->clear();
        $this->syncLDAPfromDB_groups();
        $this->syncLDAPfromDB_users();
        $this->syncLDAPfromDB_groups_membership();
        $this->logger->info("DBtoLDAP: ended");
    }

    /**
     * Execute all the operation of the LdapGroups to AD
     */
    protected function syncLDAPfromDB_groups()
    {
        $ldapGroups = $this->ldapGroupRepository->getAllWithOperation();
        foreach ($ldapGroups as $ldapGroup) {
            try {
                $this->syncLdapGroupWithDBGroup($ldapGroup);
            } catch (\Exception $e) {}
        }
        $this->em->flush();
    }

    /**
     * Execute the operation on an LdapGroup to AD
     *
     * @param LdapGroup $ldapGroup
     * @throws \Exception
     */
    public function syncLdapGroupWithDBGroup(LdapGroup $ldapGroup) {
        if ($ldapGroup->getOperation() == 'CREATE') {
            $this->logger->info("DBtoLDAP: creating group ".$ldapGroup->getName());

            try {
                $this->ldapProxy->createGroup($ldapGroup->getName());
                $ldapGroup->setOperation('MEMBERS CHANGED');
            } catch (\Throwable $e) {
                $ldapGroup->setOperation('ERROR');
                $this->logger->error("[DBtoLDAP-group-create] ".$e->getMessage());
                throw new LoggedException($e->getMessage(), $e->getCode(), $e);
            } catch (\Exception $e) {
                $ldapGroup->setOperation('ERROR');
                $this->logger->error("[DBtoLDAP-group-create] ".$e->getMessage());
                throw $e;
            }
        } elseif(substr($ldapGroup->getOperation(), 0, 7) == "RENAME:" && strlen($ldapGroup->getOperation()) > 8) {
            $newName = substr($ldapGroup->getOperation(), 7);
            $this->logger->info("DBtoLDAP: renaming group ".$ldapGroup->getName()." to {$newName}");

            try {
                $this->ldapProxy->renameGroup($ldapGroup->getName(), $newName);
                $ldapGroup->setName($newName);
                $ldapGroup->setOperation('');
            } catch (\Throwable $e) {
                $ldapGroup->setOperation('ERROR');
                $this->logger->error("[DBtoLDAP-group-rename] ".$e->getMessage());
                throw new LoggedException($e->getMessage(), $e->getCode(), $e);
            } catch (\Exception $e) {
                $ldapGroup->setOperation('ERROR');
                $this->logger->error("[DBtoLDAP-group-rename] ".$e->getMessage());
                throw $e;
            }
        } elseif($ldapGroup->getOperation() == "REMOVE") {
            $this->logger->info("DBtoLDAP: removing group ".$ldapGroup->getName());

            try {
                $this->ldapProxy->removeGroup($ldapGroup->getName());
                $this->em->remove($ldapGroup);
            } catch (\Throwable $e) {
                $ldapGroup->setOperation('ERROR');
                $this->logger->error("[DBtoLDAP-group-remove] ".$e->getMessage());
                throw new LoggedException($e->getMessage(), $e->getCode(), $e);
            } catch (\Exception $e) {
                $ldapGroup->setOperation('ERROR');
                $this->logger->error("[DBtoLDAP-group-remove] ".$e->getMessage());
                throw $e;
            }
        }
    }

    /**
     * Execute all the operation of the LdapUsers to AD
     */
    protected function syncLDAPfromDB_users()
    {
        $ldapUsers = $this->ldapUserRepository->getAllWithOperation();
        foreach ($ldapUsers as $ldapUser) {
            try {
                $this->syncLdapUserWithDBUser($ldapUser);
            } catch (\Exception $e) {}
        }
        $this->em->flush();
    }

    /**
     * Execute the operation on an LdapUser to AD
     *
     * @param LdapUser $ldapUser
     * @throws \Exception
     */
    public function syncLdapUserWithDBUser(LdapUser $ldapUser) {
        if (($ldapUser->getOperation() == '') OR ($ldapUser->getOperation() == 'ERROR')) {
            return;
        }
        if ($ldapUser->getOperation() == 'CREATE') {
            $this->logger->info("DBtoLDAP: creating user ".$ldapUser->getUsername());

            try {
                $this->ldapProxy->createUser($ldapUser);
                $ldapUser->setOperation("");
                $objectGUID = $this->ldapProxy->getObjectGUIDFromUsername($ldapUser->getUsername());
                if($objectGUID != "") {
                    $ldapUser->setAttribute('objectGUID', $objectGUID);
                }
            } catch (\Throwable $e) {
                $ldapUser->setOperation("ERROR");
                $this->logger->error("[DBtoLDAP-user-create] ".$e->getMessage());
                throw new LoggedException($e->getMessage(), $e->getCode(), $e);
            } catch (\Exception $e) {
                $ldapUser->setOperation("ERROR");
                $this->logger->error("[DBtoLDAP-user-create] ".$e->getMessage());
                throw $e;
            }
        } else if ($ldapUser->getOperation() == 'MODIFY') {
            $this->logger->info("DBtoLDAP: modifying user ".$ldapUser->getUsername());

            try {
                $this->ldapProxy->modifyUser($ldapUser);
                $ldapUser->setOperation("");
            } catch (\Throwable $e) {
                $ldapUser->setOperation("ERROR");
                $this->logger->error("[DBtoLDAP-user-modify] ".$e->getMessage());
                throw new LoggedException($e->getMessage(), $e->getCode(), $e);
            } catch (\Exception $e) {
                $ldapUser->setOperation("ERROR");
                $this->logger->error("[DBtoLDAP-user-modify] ".$e->getMessage());
                throw $e;
            }
        } else if ($ldapUser->getOperation() == 'REMOVE') {
            $this->logger->info("DBtoLDAP: removing user ".$ldapUser->getUsername());

            try {
                $this->ldapProxy->removeUser($ldapUser->getUsername());
                $this->em->remove($ldapUser);
            } catch (\Throwable $e) {
                $ldapUser->setOperation("ERROR");
                $this->logger->error("[DBtoLDAP-user-remove] ".$e->getMessage());
                throw new LoggedException($e->getMessage(), $e->getCode(), $e);
            } catch (\Exception $e) {
                $ldapUser->setOperation("ERROR");
                $this->logger->error("[DBtoLDAP-user-remove] ".$e->getMessage());
                throw $e;
            }
        }
    }


    /**
     * Syncronize all the membership of the LdapGroups to Ad
     */
    protected function syncLDAPfromDB_groups_membership()
    {
        $ldapGroups = $this->ldapGroupRepository->getAllWithOperation();
        foreach ($ldapGroups as $ldapGroup) {
            try {
                $this->syncLdapGroupMembershipWithDB($ldapGroup);
            } catch (\Exception $e) {}
        }
        $this->em->flush();
    }

    /**
     * Syncronize the membership of an LdapGroup to AD
     *
     * @param LdapGroup $ldapGroup
     * @throws \Exception
     */
    public function syncLdapGroupMembershipWithDB(LdapGroup $ldapGroup) {
        if ($ldapGroup->getOperation() != 'MEMBERS CHANGED') {
            return;
        }

        $name = $ldapGroup->getName();
        $memberList = $ldapGroup->getMembersList();
        $this->logger->info("DBtoLDAP: updating member of group ".$name);

        $errors = [];
        if (isset($memberList['user'])) {
            try {
                $this->ldapProxy->updateUsersIntoGroup($name, $memberList['user']);

            } catch (\Throwable $e) {
                $this->logger->error("[DBtoLDAP-group-users] ".$e->getMessage());
                $errors[] = $e->getMessage();
            } catch (\Exception $e) {
                $this->logger->error("[DBtoLDAP-group-users] ".$e->getMessage());
                $errors[] = $e->getMessage();
            }
        }

        if (isset($memberList['group'])) {
            try {
                $this->ldapProxy->updateGroupsIntoGroup($name, $memberList['group']);
            } catch (\Throwable $e) {
                $this->logger->error("[DBtoLDAP-group-groups] ".$e->getMessage());
                $errors[] = $e->getMessage();
            } catch (\Exception $e) {
                $this->logger->error("[DBtoLDAP-group-groups] ".$e->getMessage());
                $errors[] = $e->getMessage();
            }
        }

        if (!empty($errors)) {
            $ldapGroup->setOperation('ERROR');
            $this->logger->error("[DBtoLDAP-group-groups] ".implode(" - ", $errors));
            throw new LoggedException(implode(" \n", $errors));
        } else {
            $ldapGroup->setOperation('');
        }
    }

    /**
     * Synchronize the member of the InternetAccess group with the Ldap and set as activated the members
     *
     * @throws \Exception
     */
    public function syncInternetAccessLdapGroup() {
        $internetAccessGroupName = $this->configurationManager->getActiveDirectoryGeneratedGroupPrefix() . "InternetAccess";
        $internetLdapGroup = $this->em->getRepository('ZenIgrooveBundle:LdapGroup')->find($internetAccessGroupName);

        $personalDeviceAccessGroupName = $this->configurationManager->getActiveDirectoryGeneratedGroupPrefix() . "PersonalDeviceAccess";
        $personalDeviceLdapGroup = $this->em->getRepository('ZenIgrooveBundle:LdapGroup')->find($personalDeviceAccessGroupName);

        if(!$internetLdapGroup instanceof LdapGroup) {
            $this->logger->error("[DBtoLDAP-syncInternetAccess] Invalid group {$internetAccessGroupName}");
            throw new LoggedException("Invalid group {$internetAccessGroupName}");
        }

        if(!$personalDeviceLdapGroup instanceof LdapGroup) {
            $this->logger->error("[DBtoLDAP-syncInternetAccess] Invalid group {$personalDeviceAccessGroupName}");
            throw new LoggedException("Invalid group {$personalDeviceAccessGroupName}");
        }

        $this->syncLdapGroupMembershipWithDB($internetLdapGroup);
        $this->syncLdapGroupMembershipWithDB($personalDeviceLdapGroup);

        $internetOpensToClose = $this->em->getRepository('ZenIgrooveBundle:InternetOpen')->findBy(['activationCompleated' => false, 'type' => ['user', 'group']]);
        foreach ($internetOpensToClose as $internetOpen) {
            $internetOpen->setActivationCompleated(true);
        }
        $this->em->flush();
    }

    /**
     * Create the specified OU, if not exists
     *
     * @param string $ouName
     * @param string $path
     */
    public function createOuIfNotExists($ouName, $path="") {
        if($this->ldapProxy->ouExists($ouName, $path)) {
            return;
        }

        $this->ldapProxy->createOu($ouName, $path);
    }

    /**
     * Move a username into an OU
     *
     * @param string $ouName
     * @param string $username
     * @param string $path
     */
    public function moveUserIntoOu($ouName, $username, $path="") {
        $this->ldapProxy->moveUserIntoOu($ouName, $username, $path);
    }

    /**
     * Move a group into an OU
     *
     * @param string $ouName
     * @param string $groupName
     * @param string $path
     */
    public function moveGroupIntoOu($ouName, $groupName, $path="") {
        $this->ldapProxy->moveGroupIntoOu($ouName, $groupName, $path);
    }

    /**
     * Update the OU position of the specified users
     *
     * @param string $ouName
     * @param array $users
     * @param string $path
     */
    public function updateUsersIntoOu($ouName, $users, $path="") {
        $this->ldapProxy->updateUsersIntoOu($ouName, $users, $path);
    }

    /**
     * Update the OU position of the specified groups
     *
     * @param string $ouName
     * @param array $groups
     * @param string $path
     */
    public function updateGroupsIntoOu($ouName, $groups, $path="") {
        $this->ldapProxy->updateGroupsIntoOu($ouName, $groups, $path);
    }

    /**
     * Refresh an opened connection
     */
    public function refreshConnection() {
        $this->ldapProxy->refreshConnection();
    }
}
