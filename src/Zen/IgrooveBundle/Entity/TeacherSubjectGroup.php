<?php

namespace Zen\IgrooveBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * TeacherSubjectGroup
 *
 * @ORM\Table(uniqueConstraints={@UniqueConstraint(name="unique", columns={"teacher_id", "subject_id", "group_id"})})
 * @ORM\Entity
 */
class TeacherSubjectGroup
{
    /**
     * @var string
     *
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var Teacher
     *
     * @ORM\ManyToOne(targetEntity="Teacher", inversedBy="teacherSubjectGroups")
     **/
    private $teacher;

    /**
     * @var Subject
     *
     * @ORM\ManyToOne(targetEntity="Subject", inversedBy="teacherSubjectGroups")
     **/
    private $subject;

    /**
     * @var Group
     *
     * @ORM\ManyToOne(targetEntity="Group", inversedBy="teacherSubjectGroups")
     **/
    private $group;

    /**
     * @var Provider
     *
     * @ORM\ManyToOne(targetEntity="Provider", inversedBy="teacherSubjectGroups")
     **/
    private $provider;


    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Teacher
     */
    public function getTeacher() {
        return $this->teacher;
    }

    /**
     * @param Teacher $teacher
     */
    public function setTeacher($teacher) {
        $this->teacher = $teacher;
    }

    /**
     * @return Subject
     */
    public function getSubject() {
        return $this->subject;
    }

    /**
     * @param Subject $subject
     */
    public function setSubject($subject) {
        $this->subject = $subject;
    }

    /**
     * @return Group
     */
    public function getGroup() {
        return $this->group;
    }

    /**
     * @param Group $group
     */
    public function setGroup($group) {
        $this->group = $group;
    }

    /**
     * @return Provider
     */
    public function getProvider() {
        return $this->provider;
    }

    /**
     * @param Provider $provider
     */
    public function setProvider($provider) {
        $this->provider = $provider;
    }
}

