<?php

namespace Zen\IgrooveBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Sector
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Zen\IgrooveBundle\Repository\SectorRepository")
 */
class Sector
{
    /**
     * @var string
     *
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var Group[]
     *
     * @ORM\OneToMany(targetEntity="Group", mappedBy="sector")
     * @Serializer\Groups({"sector_groups"})
     * @Serializer\MaxDepth(3)
     *
     **/
    private $groups;

    /**
     * @var Provider
     *
     * @ORM\ManyToOne(targetEntity="Provider", inversedBy="sectors")
     * @Serializer\Groups({"sector_provider"})
     * @Serializer\MaxDepth(2)
     **/
    private $provider;

    /**
     * @var string
     * @ORM\Column(name="id_on_provider", type="integer", nullable=true)
     */
    private $idOnProvider;


    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Sector
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getGroups() {
        return $this->groups;
    }

    /**
     * @param mixed $groups
     */
    public function setGroups($groups) {
        $this->groups = $groups;
    }

    /**
     * @return Provider
     */
    public function getProvider() {
        return $this->provider;
    }

    /**
     * @param Provider $provider
     */
    public function setProvider($provider) {
        $this->provider = $provider;
    }

    /**
     * @return string
     */
    public function getIdOnProvider() {
        return $this->idOnProvider;
    }

    /**
     * @param string $idOnProvider
     */
    public function setIdOnProvider($idOnProvider) {
        $this->idOnProvider = $idOnProvider;
    }

    public function isSame(\Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Sector $importedSector) {
        if($importedSector->getName() != $this->getName()) {
            return false;
        }

        return TRUE;
    }


    public function __clone() {
        $this->groups = clone $this->groups;
    }

}

