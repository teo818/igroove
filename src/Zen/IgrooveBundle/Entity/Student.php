<?php

namespace Zen\IgrooveBundle\Entity;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Zen\IgrooveBundle\Repository\StudentRepository")
 * @UniqueEntity("fiscalCode")
 * @UniqueEntity("username")
 * @UniqueEntity("email")
 */
class Student extends PersonAbstract
{
    /**
     * @ORM\ManyToMany(targetEntity="Group", inversedBy="students")
     * @ORM\JoinTable(name="students_groups",
     *  joinColumns={@ORM\JoinColumn(name="student_id", referencedColumnName="id")},
     *  inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     *      )
     * @Serializer\MaxDepth(3)
     * @Serializer\Groups({"student_groups"})
     * @Serializer\SerializedName("groups")
     **/
    protected $memberOf;

    /**
     * Student constructor.
     *
     * @param $id
     */
    public function __construct($fiscalCode)
    {
        $this->memberOf = new \Doctrine\Common\Collections\ArrayCollection();
        parent::__construct($fiscalCode);
    }

    public function __clone() {
        parent::__clone();
        $this->memberOf = clone $this->memberOf;
    }

    /**
     * Add memberOf.
     *
     * @param \Zen\IgrooveBundle\Entity\Group $memberOf
     *
     * @return PersonAbstract
     */
    public function addMemberOf(\Zen\IgrooveBundle\Entity\Group $memberOf)
    {
        if (!$this->isMemberOf($memberOf)) {
            $this->memberOf->add($memberOf);
        }

        return $this;
    }

    /**
     * Remove memberOf.
     *
     * @param \Zen\IgrooveBundle\Entity\Group $memberOf
     */
    public function removeMemberOf(\Zen\IgrooveBundle\Entity\Group $memberOf)
    {
        $this->memberOf->removeElement($memberOf);
    }

    public function removeAllMemberOf()
    {
        foreach ($this->memberOf as $group) {
            $group->removeStudent($this);
        }
        $this->memberOf = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get memberOf.
     *
     * @return Group[]|\Doctrine\Common\Collections\Collection
     */
    public function getMemberOf()
    {
        return $this->memberOf;
    }

    /**
     * @return Group[]|\Doctrine\Common\Collections\Collection
     */
    public function getGroups()
    {
        return $this->memberOf;
    }

    /**
     * @return Group[]|\Doctrine\Common\Collections\Collection
     */
    public function getNotManuallyManagedGroups()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('manageManually', false));

        return $this->memberOf->matching($criteria);
    }

    /**
     * @param Provider $provider
     * @return Group[]|\Doctrine\Common\Collections\Collection|null
     */
    public function getNotManuallyManagedGroupOfProvider(Provider $provider)
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->andX(
            Criteria::expr()->eq('provider_id', $provider->getId()),
            Criteria::expr()->eq('manageManually', false)
        ));

        $groups = $this->memberOf->matching($criteria);

        return isset($groups[0]) ? $groups[0] : null;
    }

    /**
     * @return Group[]|\Doctrine\Common\Collections\Collection
     */
    public function getManuallyManagedGroups()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('manageManually', true));

        return $this->memberOf->matching($criteria);
    }

    /**
     * check membership.
     *
     * @return bool
     */
    public function isMemberOf($groupToTest)
    {
        foreach ($this->memberOf as $group) {
            if ($group == $groupToTest) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param \Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Student $importedStudent
     * @param Provider|NULL $provider
     * @return bool
     */
    public function isSame(\Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Student $importedStudent, Provider $provider = null)
    {
        if (strtolower($this->fiscalCode) != strtolower($importedStudent->getFiscalCode())) {
            return false;
        }

        if ($this->firstname != $importedStudent->getFirstName()) {
            return false;
        }

        if ($this->lastname != $importedStudent->getLastName()) {
            return false;
        }

        if ($importedStudent->getEmail() !== null && strtolower($this->email) != strtolower($importedStudent->getEmail())) {
            return false;
        }

        if ($importedStudent->getPassword() !== null && $this->starting_password != $importedStudent->getPassword()) {
            return false;
        }
        if ($importedStudent->getUsername() !== null && strtolower($this->username) != strtolower($importedStudent->getUsername())) {
            return false;
        }

        if($provider === null) {
            $provider = $this->getProvider();
        }

        $group = $this->getNotManuallyManagedGroupOfProvider($provider);
        if ($importedStudent->getGroup() !== null) {
            if ($group === null || $group->getId() != $importedStudent->getGroup()->getId()) {
                return false;
            }
        } elseif($group !== null) {
            return false;
        }

        return true;
    }

    /**
     * @param \Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Student $importedStudent
     * @param Provider|NULL $provider
     * @return bool
     */
    public function haveSameGroup(\Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Student $importedStudent, Provider $provider = null)
    {
        $group = $this->getNotManuallyManagedGroupOfProvider($provider);
        if ($importedStudent->getGroup() !== null) {
            if ($group === null || $group->getId() != $importedStudent->getGroup()->getId()) {
                return false;
            }
        } elseif($group !== null) {
            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public function getProviderSettings()
    {
        if (!$this->provider instanceof Provider) {
            return Provider::$settingsFieldNulled;
        }

        return $this->provider->getStudentSettings();
    }
}
