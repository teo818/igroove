<?php

namespace Zen\IgrooveBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class VersionCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('version')
            ->setDescription('Check versions (your and avaibility');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;

        $versionManager = $this->getContainer()->get('zen.igroove.version');
        $versions = $versionManager->checkNewVersion();
        $versions = $versionManager->getVersions();
        var_dump($versions);
    }
}
