<?php
namespace Zen\IgrooveBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zen\IgrooveBundle\Entity\Provider;
use Zen\IgrooveBundle\LdapTool;

class CronCommand extends ContainerAwareCommand
{
	/**
     * @var \Zen\IgrooveBundle\Manager\PersonsAndGroups
     */
    private $personsAndGroups;
	/**
     * @var \Zen\IgrooveBundle\Manager\LdapProxy
     */
    private $ldapProxy;
    private $configurationManager;
    private $em;
    protected $logger;
    protected $output;

    protected function configure()
    {
        $this->setName('cron')
            ->setDescription('Importing and parsing');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->logger = $this->getContainer()->get('logger');
        $this->em = $this->getContainer()->get('doctrine')->getManager();
        $this->configurationManager = $this->getContainer()->get('zen.igroove.configuration');
        $this->personsAndGroups = $this->getContainer()->get('personsAndGroups');
        $this->ldapProxy = $this->getContainer()->get('ldap.proxy');
        $this->output = $output;

        $providers = $this->em->getRepository('ZenIgrooveBundle:Provider')->findBy(array('active' => true));
        foreach ($providers as $provider) {
            if (!$provider instanceof Provider || !$provider->getFilter()) {
                continue;
            }

            $output->writeln("<info>Start Importing data from {$provider->getName()}</info>");
            $filter = clone $this->getContainer()->get($provider->getFilter());
            $filter->setParameters($provider->getFilterData());
            try {
                $filter->parseRemoteData();
            } catch(\ErrorException $e) {
                $output->writeln("Errore nel parsing del provider {$provider->getName()}: ".$e->getMessage());
                $this->logger->error("Errore nel parsing del provider {$provider->getName()}: ".$e->getMessage());
                continue;
            }
            $this->personsAndGroups->importElements($filter, $provider);
            $output->writeln("<info>Ended Importing data from {$provider->getName()}</info>");
        }
        $this->printAndLogInfo('purgeAndPopulateAll');
        $this->ldapProxy->purgeAndPopulateAll();
        $this->printAndLogInfo('checkLdapGroups');
        $this->personsAndGroups->checkLdapGroups();
        $this->printAndLogInfo('checkLdapOUs');
        $this->personsAndGroups->checkLdapOUs();
        $this->printAndLogInfo('checkLdapUsers');
        $this->personsAndGroups->checkLdapUsers();
        $this->printAndLogInfo('checkLdapUsersMembership');
        $this->personsAndGroups->checkLdapUsersMembership();
        $this->printAndLogInfo('checkLdapUsersOUMembership');
        $this->personsAndGroups->checkLdapUsersOUMembership();

        $this->printAndLogInfo('syncLDAPfromDB</info> via RabbitMQ');
        $msg = array('command' => 'syncLDAPfromDB', 'parameters' => array());
        $client = $this->getContainer()->get('old_sound_rabbit_mq.ldap_service_producer');
        $client->publish(serialize($msg));

        $this->printAndLogInfo('checkGoogleAppsGroups via RabbitMQ');
        $this->personsAndGroups->checkGoogleAppsGroups();
        $this->printAndLogInfo('checkGoogleAppsOUs via RabbitMQ');
        $this->personsAndGroups->checkGoogleAppsOUs();
        $this->printAndLogInfo('checkGoogleAppsUsers via RabbitMQ');
        $this->personsAndGroups->checkGoogleAppsUsers();
        $this->printAndLogInfo('checkGoogleAppsUsersMembershipToGroup via RabbitMQ');
        $this->personsAndGroups->checkGoogleAppsUsersMembershipToGroup();
        $this->printAndLogInfo('checkGoogleAppsUsersMembershipToOU via RabbitMQ');
        $this->personsAndGroups->checkGoogleAppsUsersMembershipToOU();



//        $this->printAndLogInfo('syncLDAPfromDB');
//        $this->ldapProxy->syncLDAPfromDB();

        $this->printAndLogInfo("All done!");
    }
    
    protected function printAndLogInfo($message) {
        $this->output->writeln('<info>'.$message.'</info>');
        $this->logger->info("CRON:".$message);
    }
}