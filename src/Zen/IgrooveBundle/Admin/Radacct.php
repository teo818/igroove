<?php

namespace Zen\IgrooveBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class Radacct extends Admin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('radacctid')
            ->add('acctsessionid')
            ->add('acctuniqueid')
            ->add('username')
            ->add('groupname')
            ->add('realm')
            ->add('nasipaddress')
            ->add('nasportid')
            ->add('nasporttype')
            ->add('acctstarttime')
            ->add('acctstoptime')
            ->add('acctsessiontime')
            ->add('acctauthentic')
            ->add('connectinfoStart')
            ->add('connectinfoStop')
            ->add('acctinputoctets')
            ->add('acctoutputoctets')
            ->add('calledstationid')
            ->add('callingstationid')
            ->add('acctterminatecause')
            ->add('servicetype')
            ->add('framedprotocol')
            ->add('framedipaddress')
            ->add('acctstartdelay')
            ->add('acctstopdelay');


    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper   ->add('radacctid')
            ->add('acctsessionid')
            ->add('acctuniqueid')
            ->add('username')
            ->add('groupname')
            ->add('realm')
            ->add('nasipaddress')
            ->add('nasportid')
            ->add('nasporttype')
            ->add('acctstarttime')
            ->add('acctstoptime')
            ->add('acctsessiontime')
            ->add('acctauthentic')
            ->add('connectinfoStart')
            ->add('connectinfoStop')
            ->add('acctinputoctets')
            ->add('acctoutputoctets')
            ->add('calledstationid')
            ->add('callingstationid')
            ->add('acctterminatecause')
            ->add('servicetype')
            ->add('framedprotocol')
            ->add('framedipaddress')
            ->add('acctstartdelay')
            ->add('acctstopdelay');

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('radacctid')
            ->add('acctsessionid')
            ->add('acctuniqueid')
            ->add('username')
            ->add('groupname')
            ->add('realm')
            ->add('nasipaddress')
            ->add('nasportid')
            ->add('nasporttype')
            ->add('acctstarttime')
            ->add('acctstoptime')
            ->add('acctsessiontime')
            ->add('acctauthentic')
            ->add('connectinfoStart')
            ->add('connectinfoStop')
            ->add('acctinputoctets')
            ->add('acctoutputoctets')
            ->add('calledstationid')
            ->add('callingstationid')
            ->add('acctterminatecause')
            ->add('servicetype')
            ->add('framedprotocol')
            ->add('framedipaddress')
            ->add('acctstartdelay')
            ->add('acctstopdelay');
    }

}


