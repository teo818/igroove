<?php

namespace Zen\IgrooveBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class Device extends Admin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id')
            ->add('device')
            ->add('mac')
            ->add('ips')
            ->add('bypassHotspot')
            ->add('active')
            ;


    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id')
            ->add('device')
            ->add('mac')
            ->add('ips')
            ->add('bypassHotspot')
            ->add('active')
            ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('device')
            ->add('mac')
            ->add('ips')
            ->add('bypassHotspot')
            ->add('active')
;
    }

}


