<?php

namespace Zen\IgrooveBundle\Controller;

use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Zen\IgrooveBundle\Entity\MikrotikPool;
use Zen\IgrooveBundle\Form\MikrotikPoolType;

/**
 * MikrotikPool controller.
 *
 * @Route("/mikrotik/pool")
 */
class MikrotikPoolController extends Controller
{

    /**
     * Lists all MikrotikPool entities.
     *
     * @Route("/", name="mikrotik_pool")
     * @Method("GET")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function indexAction()
    {
        $client = $this->container->get('old_sound_rabbit_mq.mikrotik_service_producer');
        $msg = array('command' => 'addPools', 'parameters' => array());
        $client->publish(serialize($msg));

        $request = $this->get('Request');
        $queryString = $request->get('queryString', false);
        $q = '%' . $queryString . '%';
        $em = $this->getDoctrine()->getManager();
        if ($queryString) {
            $query = $em->createQuery(
                "SELECT l FROM ZenIgrooveBundle:MikrotikPool l WHERE l.nome  LIKE :q ORDER BY l.nome"
            )
                ->setParameter('q', $q);
        } else {
            $query = $em->createQuery("SELECT l FROM ZenIgrooveBundle:MikrotikPool l  ORDER BY l.nome");
        }
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1),25
        );


        return array(
            'pagination' => $pagination
        );
    }

    /**
     * Creates a new MikrotikPool entity.
     *
     * @Route("/", name="mikrotik_pool_create")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("POST")
     * @Template("ZenIgrooveBundle:MikrotikPool:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new MikrotikPool();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);

            $notAssociatedDeviceIps = $em->getRepository('ZenIgrooveBundle:DeviceIp')->getAllNotAssociatedIpsInRange($entity->getIpStart(), $entity->getIpEnd())->toArray();
            $outOfRangeDeviceIps = $em->getRepository('ZenIgrooveBundle:DeviceIp')->getAllAssociatedIpsOutOfRange($entity, $entity->getIpStart(), $entity->getIpEnd())->toArray();

            foreach ($notAssociatedDeviceIps as $notAssociatedDeviceIp) {
                $notAssociatedDeviceIp->setMikrotikPool($entity);
                $em->persist($notAssociatedDeviceIp);
            }

            foreach ($outOfRangeDeviceIps as $outOfRangeDeviceIp) {
                $outOfRangeDeviceIp->setMikrotikPool(null);
                $em->persist($outOfRangeDeviceIp);
            }

            $em->flush();

            return $this->redirect($this->generateUrl('mikrotik_pool'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a MikrotikPool entity.
     *
     * @param MikrotikPool $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(MikrotikPool $entity)
    {
        $form = $this->createForm(new MikrotikPoolType(), $entity, array(
            'action' => $this->generateUrl('mikrotik_pool_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Salva Modifiche'));


        return $form;
    }

    /**
     * Displays a form to create a new MikrotikPool entity.
     *
     * @Route("/new", name="mikrotik_pool_new")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new MikrotikPool();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing MikrotikPool entity.
     *
     * @Route("/{id}/edit", name="mikrotik_pool_edit")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ZenIgrooveBundle:MikrotikPool')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MikrotikPool entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a MikrotikPool entity.
    *
    * @param MikrotikPool $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(MikrotikPool $entity)
    {
        $form = $this->createForm(new MikrotikPoolType(), $entity, array(
            'action' => $this->generateUrl('mikrotik_pool_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Salva Modifiche'));

        return $form;
    }
    /**
     * Edits an existing MikrotikPool entity.
     *
     * @Route("/{id}", name="mikrotik_pool_update")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("PUT")
     * @Template("ZenIgrooveBundle:MikrotikPool:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ZenIgrooveBundle:MikrotikPool')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MikrotikPool entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $notAssociatedDeviceIps = $em->getRepository('ZenIgrooveBundle:DeviceIp')->getAllNotAssociatedIpsInRange($entity->getIpStart(), $entity->getIpEnd())->toArray();
            $outOfRangeDeviceIps = $em->getRepository('ZenIgrooveBundle:DeviceIp')->getAllAssociatedIpsOutOfRange($entity, $entity->getIpStart(), $entity->getIpEnd())->toArray();

            foreach ($notAssociatedDeviceIps as $notAssociatedDeviceIp) {
                $notAssociatedDeviceIp->setMikrotikPool($entity);
                $em->persist($notAssociatedDeviceIp);
            }

            foreach ($outOfRangeDeviceIps as $outOfRangeDeviceIp) {
                $outOfRangeDeviceIp->setMikrotikPool(null);
                $em->persist($outOfRangeDeviceIp);
            }

            $em->flush();

            return $this->redirect($this->generateUrl('mikrotik_pool'));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a MikrotikPool entity.
     *
     * @Route("/{id}", name="mikrotik_pool_delete")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ZenIgrooveBundle:MikrotikPool')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find MikrotikPool entity.');
            }

            foreach ($entity->getDeviceIps() as $deviceIp) {
                $deviceIp->setMikrotikPool(null);
                $em->persist($deviceIp);
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('mikrotik_pool'));
    }

    /**
     * Creates a form to delete a MikrotikPool entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('mikrotik_pool_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Rimuovi questo pool', 'attr' => ['class' => 'btn btn-danger']))
            ->getForm()
        ;
    }

    /**
     * Check first free ip for in a pool
     *
     * @Route("/{id}/checkFreeIp", name="mikrotik_pool_check_free_ip")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function checkFreeIpAction($id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ZenIgrooveBundle:MikrotikPool')->find($id);

        if (!$entity) {
            return new Response(json_encode(['error' => 'Unable to find the pool you specified']), 404);
        }

        $selectedIp = null;
        $lastIp = $entity->getIpStart();
        foreach ($entity->getDeviceIps() as $ip) {
            if($lastIp < $ip->getRawIp()) {
                $selectedIp = $lastIp;
                break;
            }

            $lastIp = $ip->getRawIp()+1;
        }

        if($selectedIp === null && $lastIp <= $entity->getIpEnd()) {
            $selectedIp = $lastIp;
        }

        if($selectedIp === null) {
            return new Response(json_encode(['error' => "No more ip in the range {$entity->getDottedIpStart()} - {$entity->getDottedIpEnd()} of the pool {$entity->getNome()}"]));
        }

        return new Response(json_encode(['ip' => long2ip($selectedIp)]));
    }
    
}
