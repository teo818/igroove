<?php

namespace Zen\IgrooveBundle\Controller;

use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Zen\IgrooveBundle\Entity\Group;
use Zen\IgrooveBundle\Entity\Provider;
use Zen\IgrooveBundle\Entity\Sector;
use Zen\IgrooveBundle\Entity\Student;
use Zen\IgrooveBundle\Entity\Teacher;
use Zen\IgrooveBundle\Form\ProviderType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Zen\IgrooveBundle\ImporterFilter\AbstractFilter;
use Zen\IgrooveBundle\ImporterFilter\ImportedEntity\AbstractEntity;
use Zen\IgrooveBundle\Manager\ConfigurationManager;
use Symfony\Component\Finder\Finder;

/**
 * Provider controller.
 *
 * @Route("/config/provider")
 */
class ProviderController extends Controller
{
    protected $filterProviderData;
    protected $importedEntities;

    /**
     * Creates a new Provider entity.
     *
     * @Route("/", name="config_provider_create")
     * @Method("POST")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("ZenIgrooveBundle:Provider:edit.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Provider();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $this->prepareFilterProviderData($entity,$form);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('config_edit').'#provider');
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Provider entity.
     *
     * @param Provider $entity The entity
     * @Secure(roles="ROLE_ADMIN")
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Provider $entity)
    {
        $formType = new ProviderType();
        $formType->setFilterProviderData($this->getFilterProviderData());

        $form = $this->createForm($formType, $entity, array(
            'action' => $this->generateUrl('config_provider_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crea'));

        return $form;
    }

    /**
     * Displays a form to create a new Provider entity.
     *
     * @Route("/new", name="config_provider_new")
     * @Method("GET")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("ZenIgrooveBundle:Provider:edit.html.twig")
     */
    public function newAction()
    {
        $entity = new Provider();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'main_form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Provider entity.
     *
     * @Route("/{id}/edit", name="config_provider_edit")
     * @Method("GET")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ZenIgrooveBundle:Provider')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Provider entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'main_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Provider entity.
    *
    * @param Provider $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Provider $entity)
    {
        $formType = new ProviderType();
        $formType->setFilterProviderData($this->getFilterProviderData());
        $form = $this->createForm($formType, $entity, array(
            'action' => $this->generateUrl('config_provider_update', array('id' => $entity->getId())),
            'method' => 'PUT'
        ));

        if(!empty($entity->getFilterData()) && $entity->getFilter() != "" && isset($this->filterProviderData[$entity->getFilter()])) {
            $filterData = $entity->getFilterData();
            foreach($form->get("filterDataCont")->get($entity->getFilter())->all() as $dataField) {
                if(isset($filterData[$dataField->getName()])) {
                    $dataField->setData($filterData[$dataField->getName()]);
                }
            }
        }

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Provider entity.
     *
     * @Route("/{id}", name="config_provider_update")
     * @Method("PUT")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("ZenIgrooveBundle:Provider:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ZenIgrooveBundle:Provider')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Provider entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $this->prepareFilterProviderData($entity,$editForm);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('config_edit').'#provider');
        }

        return array(
            'entity'      => $entity,
            'main_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Provider entity.
     *
     * @Route("/{id}", name="config_provider_delete")
     * @Method("DELETE")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ZenIgrooveBundle:Provider')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Provider entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('config_edit').'#provider');
    }

    /**
     * Creates a form to delete a Provider entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('config_provider_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }

    protected function getFilterProviderData() {
        if(empty($this->filterProviderData)) {
            $finder = new Finder();
            $finder->files()->in(__DIR__."/../ImporterFilter");
            foreach($finder as $file) {
                if($file->getRelativePathname() == "AbstractFilter.php" || $file->getRelativePathname() == "")
                    continue;

                $class = "Zen\\IgrooveBundle\\ImporterFilter\\".substr($file->getRelativePathname(),0,-4);
                if(!class_exists($class))
                    continue;

                $this->filterProviderData[$class::$internalName] = ['name' => $class::$name, 'ui' => $class::$parametersUi];
            }
        }

        return $this->filterProviderData;
    }

    protected function prepareFilterProviderData(Provider $provider, Form $form) {
        $filter = $provider->getFilter();

        if(!$form->get("filterDataCont")->has($filter) || !is_array($form->get("filterDataCont")->get($filter)->getData()))
            return;

        $currentFilterData = $provider->getFilterData();
        foreach($form->get("filterDataCont")->get($filter)->getData() as $fieldName => $fieldValue) {
            if(!isset($this->filterProviderData[$filter]['ui'][$fieldName]))
                continue;

            if($this->filterProviderData[$filter]['ui'][$fieldName]['type'] == "password" && $fieldValue == "")
                continue;

            $currentFilterData[$fieldName] = $fieldValue;
        }

        $provider->setFilterData($currentFilterData);
    }

    /**
     *
     * @Route("/{id}/sync")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("ZenIgrooveBundle:Provider:showManualSync.html.twig")
     */
    public function showManualSyncAction($id) {
        $provider = $this->getDoctrine()->getManager()->getRepository('ZenIgrooveBundle:Provider')->find($id);

        if (!$provider) {
            throw $this->createNotFoundException('Unable to find Provider entity.');
        }

        return ['provider' => $provider];
    }

    /**
     * @Route("/{id}/load-data", name="config_provider_load_data")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("POST")
     */
    public function loadDataToSyncAction($id) {
        $provider = $this->getDoctrine()->getManager()->getRepository('ZenIgrooveBundle:Provider')->find($id);

        if (!$provider) {
            return new JsonResponse(['error' => "Provider non trovato"]);
        }

        $filter = clone $this->get($provider->getFilter());
        if(!$filter instanceof AbstractFilter) {
            return new JsonResponse(['error' => "Filtro provider non valido"]);
        }
        $filter->setParameters($provider->getFilterData());
        $filter->setIsManualImport(true);
        try {
            $filter->parseRemoteData();
        } catch(\ErrorException $e) {
            return new JsonResponse(['error' => "Errore nel parsing del provider {$provider->getName()}: ".$e->getMessage()]);
        }

        $data = [];
        $data['sectors'] = $this->compareProviderData($filter->getSectors(),$provider,"Sector","name");

        $newGroups = $filter->getGroups();
        foreach ($newGroups as $newGroup) {
            if ($newGroup->getSectorId() !== null && isset($this->importedEntities['Sector'][$newGroup->getSectorId()])) {
                $newGroup->setSector($this->importedEntities['Sector'][$newGroup->getSectorId()]);
            }
        }

        $data['groups'] = $this->compareProviderData($newGroups,$provider,"Group","name");
        $data['subjects'] = $this->compareProviderData($filter->getSubjects(),$provider,"Subject","name");
        $data['teachers'] = $this->compareProviderData($filter->getTeachers(),$provider,"Teacher","fiscalCode");

        $newStudents = $filter->getStudents();
        foreach ($newStudents as $k => $newStudent) {
            if ($newStudent->getGroupId() !== null && isset($this->importedEntities['Group'][$newStudent->getGroupId()])) {
                $newStudent->setGroup($this->importedEntities['Group'][$newStudent->getGroupId()]);
            }
        }
        $data['students'] = $this->compareProviderData($newStudents, $provider,"Student","fiscalCode");

        return new JsonResponse(['data' => $data]);

    }

    /**
     * @Route("/{providerId}/import-data", name="config_provider_import_data")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("POST")
     */
    public function importDataAction(Request $request, $providerId) {
        $entityData = $request->get('entityData');
        $entityType = $request->get('entityType');
        $entityId = $request->get('entityId');

        if($entityData === null || $entityId === null|| $entityType === null) {
            return new JsonResponse(['success' => false, 'error' => "Invalid data"]);
        }

        $provider = $this->getDoctrine()->getManager()->getRepository('ZenIgrooveBundle:Provider')->find($providerId);

        if (!$provider) {
            return new JsonResponse(['success' => false, 'error' => "Provider non trovato"]);
        }

        $entityData['id'] = $entityId;
        $entityType = ucfirst(substr($entityType, 0, -1));
        $entityTypeFull = '\\Zen\\IgrooveBundle\\Entity\\'.$entityType;

        if(!class_exists($entityTypeFull)) {
            return new JsonResponse(['success' => false, 'error' => "Invalid entity type"]);
        }

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ZenIgrooveBundle:'.$entityType);

        $importedEntity = $this->getImportedEntityFromData($entityData, $entityType);
        if(!$importedEntity instanceof AbstractEntity || !$importedEntity->isValid()) {
            return $importedEntity instanceof JsonResponse ? $importedEntity : new JsonResponse(['success' => false, 'error' => "Invalid entity type or data"]);
        }

        if(($entityType === "Student" || $entityType === "Teacher")) {
            $entity = $repository->findOneBy(['fiscalCode' => $importedEntity->getFiscalCode()]);
            if($entity instanceof $entityTypeFull) {
                if($entity->getProvider()->getId() != $providerId) {
                    $entity->addAdditionalProvider($provider);
                    $em->flush();

                    if($entity instanceof Student && !$entity->haveSameGroup($importedEntity, $provider)) {
                        $repository->updateMembershipWithImportData($entity, $importedEntity, $provider);
                    }
                }

                return new JsonResponse(['success' => true, 'error' => null]);
            }
        }

        try {
            $entity = $repository->createWithImportData($importedEntity, $provider, $entityData['id']);
        } catch (\Exception $e) {
            return new JsonResponse(['success' => false, 'error' => "Error during entity creation"]);
        }

        if($entityType === "Student") {
            $repositoryAction = $this->get('zen.igroove.repositoryAction.student');
            try {
                $repositoryAction->executeAfterCreate($entity);
            } catch (\Exception $e) {
                return new JsonResponse(['success' => false, 'error' => "Error during post operation: ".$e->getMessage()]);
            }
        } elseif($entityType === "Teacher") {
            $repositoryAction = $this->get('zen.igroove.repositoryAction.teacher');
            try {
                $repositoryAction->executeAfterCreate($entity);
            } catch (\Exception $e) {
                return new JsonResponse(['success' => false, 'error' => "Error during post operation: ".$e->getMessage()]);
            }
        } elseif($entityType === "Group") {
            $repositoryAction = $this->get('zen.igroove.repositoryAction.group');
            try {
                $repositoryAction->executeAfterCreate($entity);
            } catch (\Exception $e) {
                return new JsonResponse(['success' => false, 'error' => "Error during post operation: ".$e->getMessage()]);
            }
        }

        return new JsonResponse(['success' => true, 'error' => null, 'newEntityId' => $entity->getId()]);
    }

    /**
     * @Route("/{providerId}/update-data", name="config_provider_update_data")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("POST")
     */
    public function updateDataAction(Request $request, $providerId) {
        $entityData = $request->get('entityData');
        $entityId = $request->get('entityId');
        $entityType = $request->get('entityType');

        if($entityData === null || $entityId === null || $entityType === null || !isset($entityData['oldId'])) {
            return new JsonResponse(['success' => false, 'error' => "Invalid data"]);
        }

        $provider = $this->getDoctrine()->getManager()->getRepository('ZenIgrooveBundle:Provider')->find($providerId);

        if (!$provider) {
            return new JsonResponse(['success' => false, 'error' => "Provider non trovato"]);
        }

        $entityData['id'] = $entityId;
        $entityType = ucfirst(substr($entityType, 0, -1));
        $entityTypeFull = '\\Zen\\IgrooveBundle\\Entity\\'.$entityType;

        if(!class_exists($entityTypeFull)) {
            return new JsonResponse(['success' => false, 'error' => "Invalid entity type"]);
        }

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ZenIgrooveBundle:'.$entityType);

        $entity = $repository->find($entityData['oldId']);
        if(!$entity instanceof $entityTypeFull) {
            return new JsonResponse(['success' => false, 'error' => "Invalid entity"]);
        }

        $importedEntity = $this->getImportedEntityFromData($entityData, $entityType);
        if(!$importedEntity instanceof AbstractEntity || !$importedEntity->isValid()) {
            return $importedEntity instanceof JsonResponse ? $importedEntity : new JsonResponse(['success' => false, 'error' => "Invalid entity type or data"]);
        }

        if(($entityType === "Student" || $entityType === "Teacher") && $entity->getProvider()->getId() != $providerId) {
            $entity->addAdditionalProvider($provider);
            $em->flush();

            if($entity instanceof Student && !$entity->haveSameGroup($importedEntity, $provider)) {
                $repository->updateMembershipWithImportData($entity, $importedEntity, $provider);
            }

            return new JsonResponse(['success' => true, 'error' => null]);
        }

        $previousEntity = clone $entity;

        try {
            $repository->updateWithImportData($entity, $importedEntity, $provider);
        } catch (\Exception $e) {
            return new JsonResponse(['success' => false, 'error' => "Error during entity update"]);
        }

        if($entityType === "Student") {
            $repositoryAction = $this->get('zen.igroove.repositoryAction.student');
            try {
                $repositoryAction->executeAfterUpdate($entity, $previousEntity);
            } catch (\Exception $e) {
                return new JsonResponse(['success' => false, 'error' => "Error during post operation: ".$e->getMessage()]);
            }
        } elseif($entityType === "Teacher") {
            $repositoryAction = $this->get('zen.igroove.repositoryAction.teacher');
            try {
                $repositoryAction->executeAfterUpdate($entity, $previousEntity);
            } catch (\Exception $e) {
                return new JsonResponse(['success' => false, 'error' => "Error during post operation: ".$e->getMessage()]);
            }
        } elseif($entityType === "Group") {
            $repositoryAction = $this->get('zen.igroove.repositoryAction.group');
            try {
                $repositoryAction->executeAfterUpdate($entity, $previousEntity->getName());
            } catch (\Exception $e) {
                return new JsonResponse(['success' => false, 'error' => "Error during post operation: ".$e->getMessage()]);
            }
        }

        return new JsonResponse(['success' => true, 'error' => null]);
    }

    /**
     * @Route("/{providerId}/remove-data", name="config_provider_remove_data")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("POST")
     */
    public function removeDataAction(Request $request, $providerId) {
        $entityData = $request->get('entityData');
        $entityId = $request->get('entityId');
        $entityType = $request->get('entityType');
        $options = $request->get('options');

        if($entityData === null || $entityId === null || $entityType === null) {
            return new JsonResponse(['success' => false, 'error' => "Invalid data"]);
        }

        $entityType = ucfirst(substr($entityType, 0, -1));
        $entityTypeFull = '\\Zen\\IgrooveBundle\\Entity\\'.$entityType;

        if(!class_exists($entityTypeFull)) {
            return new JsonResponse(['success' => false, 'error' => "Invalid entity type"]);
        }

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('ZenIgrooveBundle:'.$entityType);

        $entity = $repository->find($entityId);
        if(!$entity instanceof $entityTypeFull) {
            return new JsonResponse(['success' => false, 'error' => "Invalid entity"]);
        }

        $currentProvider = $em->getRepository("ZenIgrooveBundle:Provider")->find($providerId);
        if(!$currentProvider instanceof Provider) {
            return new JsonResponse(['success' => false, 'error' => "Invalid current provider"]);
        }

        $errors = [];
        $personsAndGroups = $this->get('personsAndGroups');
        if($entity instanceof Student) {
            $repositoryAction = $this->get('zen.igroove.repositoryAction.student');

            if($entity->getProvider()->getId() != $providerId) {
                try {
                    $repositoryAction->executeBeforeRemoveFromAdditionalProvider($entity, $currentProvider);
                    $entity->removeAdditionalProvider($currentProvider);
                    $em->flush();
                } catch (\Exception $e) {
                    $errors[] = $e->getMessage();
                }

                return new JsonResponse(['success' => true, 'error' => implode("\n", $errors)]);
            }

            try {
                $repositoryAction->executeBeforeRemove($entity,
                    isset($options['deleteOnLdap']) && ($options['deleteOnLdap'] == "true"),
                    isset($options['deleteOnGApps']) ? (int)$options['deleteOnGApps'] : 0,
                    isset($options['gAppsOu']) ? $options['gAppsOu'] : ""
                );
            } catch (\Exception $e) {
                $errors[] = $e->getMessage();
            }
        } elseif($entity instanceof Teacher) {
            $repositoryAction = $this->get('zen.igroove.repositoryAction.teacher');

            if($entity->getProvider()->getId() != $providerId) {
                try {
                    $repositoryAction->executeBeforeRemoveFromAdditionalProvider($entity, $currentProvider);
                    $entity->removeAdditionalProvider($currentProvider);
                    $em->flush();
                } catch (\Exception $e) {
                    $errors[] = $e->getMessage();
                }

                return new JsonResponse(['success' => true, 'error' => implode("\n", $errors)]);
            }

            try {
                $repositoryAction->executeBeforeRemove($entity,
                    isset($options['deleteOnLdap']) && ($options['deleteOnLdap'] == "true"),
                    isset($options['deleteOnGApps']) ? (int)$options['deleteOnGApps'] : 0,
                    isset($options['gAppsOu']) ? $options['gAppsOu'] : ""
                );
            } catch (\Exception $e) {
                $errors[] = $e->getMessage();
            }
        } elseif ($entity instanceof Group) {
            if(count($entity->getStudents()) > 0) {
                return new JsonResponse(['success' => false, 'error' => "Impossibile eliminare un gruppo non vuoto"]);
            }

            $repositoryAction = $this->get('zen.igroove.repositoryAction.group');

            try {
                $repositoryAction->executeBeforeRemove($entity,
                    isset($options['deleteOnLdap']) && ($options['deleteOnLdap'] == "true"),
                    isset($options['deleteOnGApps']) && ($options['deleteOnGApps'] == "true")
                );
            } catch (\Exception $e) {
                $errors[] = $e->getMessage();
            }
        }

        try {
            $em->remove($entity);
            $em->flush();
        } catch (\Exception $e) {
            $errors[] = $personsAndGroups->prepareFormErrorMessage("Errore durante l'eliminazione dell'entità", $e);
        }

        return new JsonResponse(['success' => true, 'error' => implode("\n", $errors)]);
    }

    protected function getImportedEntityFromData($entityData, $entityType) {
        $em = $this->getDoctrine()->getManager();
        $importedEntity = null;

        if($entityType === "Student") {
            $importedEntity = new \Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Student(
                $entityData['id'], $entityData['fiscalCode'], $entityData['firstName'],
                $entityData['lastName'], $entityData['groupId'] ?: null, $entityData['email'] ?: null,
                $entityData['username'] ?: null, $entityData['password'] ?: null);

            if (!isset($entityData['internalGroupId']) || $entityData['internalGroupId'] == "") {
                return new JsonResponse(['success' => FALSE, 'error' => "Invalid group id for student"]);
            }

            $group = $em->getRepository("ZenIgrooveBundle:Group")->find($entityData['internalGroupId']);
            if (!$group instanceof Group) {
                return new JsonResponse(['success' => FALSE, 'error' => "Invalid group for student"]);
            }

            $importedEntity->setGroup($group);

        } elseif($entityType === "Group") {
            $importedEntity = new \Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Group($entityData['id'], $entityData['name'], $entityData['sectorId']);

            if (!isset($entityData['internalSectorId']) || $entityData['internalSectorId'] == "") {
                return new JsonResponse(['success' => FALSE, 'error' => "Invalid group id for student"]);
            }

            $sector = $em->getRepository("ZenIgrooveBundle:Sector")->find($entityData['internalSectorId']);
            if (!$sector instanceof Sector) {
                return new JsonResponse(['success' => FALSE, 'error' => "Invalid sector for group"]);
            }

            $importedEntity->setSector($sector);
        } elseif($entityType === "Teacher") {
            $importedEntity = new \Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Teacher(
                $entityData['id'], $entityData['fiscalCode'], $entityData['firstName'],
                $entityData['lastName'], $entityData['email'] ?: null, $entityData['password'] ?: null
            );
        } elseif($entityType === "Subject") {
            $importedEntity = new \Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Subject($entityData['id'], $entityData['name']);
        } elseif($entityType === "Sector") {
            $importedEntity = new \Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Sector($entityData['id'], $entityData['name']);
        }

        return $importedEntity;
    }

    /**
     * @param AbstractEntity[] $newEntities
     * @param Provider $provider
     * @param $entityType
     * @param string $keyToCheck
     * @return array
     */
    protected function compareProviderData($newEntities, Provider $provider, $entityType, $keyToCheck = 'id') {
        $em = $this->getDoctrine()->getManager();
        $toImport = $toUpdate = $residualCurrentEntities = $checkKey = [];
        $this->importedEntities[$entityType] = [];

        $keyToCheckGet = 'get'.str_replace('_', '', ucwords($keyToCheck, '_'));
        $entityTypeFull = '\\Zen\\IgrooveBundle\\Entity\\'.$entityType;

        $currentEntities = $em->getRepository('ZenIgrooveBundle:'.$entityType)->findBy(['provider' => $provider->getId()]);
        foreach ($currentEntities as $currentEntity) {
            //aggiungo a entità residue, se non ha un idOnProvider o se non esiste l'entità con quell'id fra le nuove
            if ($currentEntity->getIdOnProvider() == '' || !isset($newEntities[$currentEntity->getIdOnProvider()])) {
                $residualCurrentEntities[$currentEntity->getId()] = $currentEntity;
                if ($keyToCheck != '') {
                    $checkKey[strtolower($currentEntity->$keyToCheckGet())] = $currentEntity->getId();
                }

                continue;
            }

            $newEntity = $newEntities[$currentEntity->getIdOnProvider()];
            if (!$newEntity->isValid()) {
//                $this->logger->error("* Dati dell'entità dal provider {$currentEntity->getIdOnProvider()} da aggiornare su id locale {$currentEntity->getId()}, non validi");
                continue;
            }
            unset($newEntities[$currentEntity->getIdOnProvider()]);
            $this->importedEntities[$entityType][$currentEntity->getIdOnProvider()] = $currentEntity;
            if (!$currentEntity->isSame($newEntity)) {
                $toUpdate[$currentEntity->getIdOnProvider()] = $newEntity->toArray();
                $toUpdate[$currentEntity->getIdOnProvider()]['oldId'] = $currentEntity->getId();
                if($entityType == "Sector" || $entityType == "Subject" || $entityType == "Group") {
                    $toUpdate[$currentEntity->getIdOnProvider()]['_name'] =  $currentEntity->getName();
                } elseif ($entityType == "Student" || $entityType == "Teacher") {
                    $toUpdate[$currentEntity->getIdOnProvider()]['_name'] = $currentEntity->getLastname()." ".$currentEntity->getFirstname()." (".$currentEntity->getFiscalCode().")";
                }
            }
        }

        if($entityType == "Student" || $entityType == "Teacher") {
            $additionalPersons = $entityType == "Teacher" ? $provider->getAdditionalTeachers() : $provider->getAdditionalStudents();
            $residualCurrentAdditionalEntities = [];
            if ($keyToCheck != '') {
                foreach ($additionalPersons as $k => $additionalPerson) {
                    $residualCurrentAdditionalEntities[strtolower($additionalPerson->$keyToCheckGet())] = $additionalPerson;
                }
            }
        }

        foreach ($newEntities as $idOnProvider => $newEntity) {
            //cerco se esiste un entità con la chiave da cercare (nome o id) combaciante, e nel caso la aggiorno, altrimenti inserico
            if ($keyToCheck != '' && isset($checkKey[strtolower($newEntity->$keyToCheckGet())]) &&
                isset($residualCurrentEntities[$checkKey[strtolower($newEntity->$keyToCheckGet())]])
            ) {
                $currentEntity = $residualCurrentEntities[$checkKey[strtolower($newEntity->$keyToCheckGet())]];
                unset($residualCurrentEntities[$checkKey[strtolower($newEntity->$keyToCheckGet())]]);

                if (!$currentEntity instanceof $entityTypeFull) {
                    continue;
                }

                if (!$newEntity->isValid()) {
//                    $this->logger->error("* Dati dell'entità dal provider {$keyToCheck} = {$newEntity->$keyToCheckGet()} da aggiornare su id locale {$entity->getId()}, non validi");
                    continue;
                }

                $currentEntity->setIdOnProvider($idOnProvider);
                $this->importedEntities[$entityType][$idOnProvider] = $currentEntity;
                if (!$currentEntity->isSame($newEntity)) {
                    $toUpdate[$currentEntity->getIdOnProvider()] = $newEntity->toArray();
                    $toUpdate[$currentEntity->getIdOnProvider()]['oldId'] = $currentEntity->getId();
                    if($entityType == "Sector" || $entityType == "Subject" || $entityType == "Group") {
                        $toUpdate[$currentEntity->getIdOnProvider()]['_name'] =  $currentEntity->getName();
                    } elseif ($entityType == "Student" || $entityType == "Teacher") {
                        $toUpdate[$currentEntity->getIdOnProvider()]['_name'] = $currentEntity->getLastname()." ".$currentEntity->getFirstname()." (".$currentEntity->getFiscalCode().")";
                    }
                } else {
                    $em->flush();
                }
            } elseif(($entityType == "Student" || $entityType == "Teacher") && $keyToCheck != '' &&
                isset($residualCurrentAdditionalEntities[strtolower($newEntity->$keyToCheckGet())])) { //se è una persona, cerco se esiste fra le persone non del provider, ma associate

                $entity = $residualCurrentAdditionalEntities[strtolower($newEntity->$keyToCheckGet())];
                unset($residualCurrentAdditionalEntities[strtolower($newEntity->$keyToCheckGet())]);

                if (!$entity instanceof $entityTypeFull) {
                    continue;
                }

                if (!$newEntity->isValid()) {
                    continue;
                }

                if($entityType == "Student" && !$entity->haveSameGroup($newEntity, $provider)) {
                    $toUpdate[$currentEntity->getIdOnProvider()] = $newEntity->toArray();
                    $toUpdate[$currentEntity->getIdOnProvider()]['oldId'] = $currentEntity->getId();
                    $toUpdate[$currentEntity->getIdOnProvider()]['_name'] = $currentEntity->getLastname()." ".$currentEntity->getFirstname()." (".$currentEntity->getFiscalCode().")";
                }
            } elseif($keyToCheck != '' && ($entityType == "Student" || $entityType == "Teacher") && $newEntity->$keyToCheckGet() != "" &&
                ($entity = $em->getRepository('ZenIgrooveBundle:'.$entityType)->findOneBy([$keyToCheck => strtolower($newEntity->$keyToCheckGet())])) instanceof $entityTypeFull
            ) {

                $toImport[$idOnProvider] = $newEntity->toArray();
                $toImport[$idOnProvider]['_name'] = $newEntity->getLastName()." ".$newEntity->getFirstName()." (".$newEntity->getFiscalCode().")";
            } else {
                if (!$newEntity->isValid()) {
//                    $this->logger->error("* Dati dell'entità dal provider {$newEntity->$keyToCheckGet()} da inserire, non validi");
                    continue;
                }

                if($em->getRepository('ZenIgrooveBundle:'.$entityType)->findOneBy([$keyToCheck => strtolower($newEntity->$keyToCheckGet())]) instanceof $entityTypeFull) {
                    continue;
                }

                $toImport[$idOnProvider] = $newEntity->toArray();
                if($entityType == "Sector" || $entityType == "Subject" || $entityType == "Group") {
                    $toImport[$idOnProvider]['_name'] =  $newEntity->getName();
                } elseif ($entityType == "Student" || $entityType == "Teacher") {
                    $toImport[$idOnProvider]['_name'] = $newEntity->getLastName()." ".$newEntity->getFirstName()." (".$newEntity->getFiscalCode().")";
                }
            }

            unset($newEntities[$idOnProvider]);
        }

        $toDelete = [];
        foreach ($residualCurrentEntities as $id => $residualCurrentEntity) {
            if($entityType == "Sector" || $entityType == "Subject" || $entityType == "Group") {
                $toDelete[$id] = ['_name' => $residualCurrentEntity->getName()];
            } elseif ($entityType == "Student" || $entityType == "Teacher") {
                $toDelete[$id] = ['_name' => $residualCurrentEntity->getLastname()." ".$residualCurrentEntity->getFirstname()." (".$residualCurrentEntity->getFiscalCode().")"];
            }
        }

        return ['toImport' => $toImport, 'toUpdate' => $toUpdate, 'toDelete' => $toDelete];
    }
}
