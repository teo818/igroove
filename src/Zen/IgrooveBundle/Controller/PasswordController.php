<?php

namespace Zen\IgrooveBundle\Controller;

use IMAG\LdapBundle\User\LdapUser;
use IMAG\LdapBundle\User\LdapUserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Zen\IgrooveBundle\Entity\Student;
use Zen\IgrooveBundle\Entity\Teacher;


class PasswordController extends Controller
{
    /**
     * @Route("/passwordChange/forceChangeMyPassword", name="forceChangeMyPassword")
     * @Template("@ZenIgroove/Password/changeMyPassword.html.twig")
     */
    public function forceChangeMyPasswordAction()
    {
        try {
            return $this->changeMyPasswordAction();
        } catch (AccessDeniedException $e) {
            throw new AccessDeniedHttpException($e->getMessage(), $e);
        }
    }

    /**
     * @Route("/passwordChange/checkPassword", name="forceCheckPassword")
     */
    public function forceCheckPasswordAction()
    {
        try {
            return $this->checkPasswordAction();
        } catch (AccessDeniedException $e) {
            throw new AccessDeniedHttpException($e->getMessage(), $e);
        }
    }

    /**
     * @Route("/passwordChange/setNewPassword", name="forceSetNewPassword")
     */
    public function forceSetNewPasswordAction()
    {
        try {
            return $this->setNewPasswordAction();
        } catch (AccessDeniedException $e) {
            throw new AccessDeniedHttpException($e->getMessage(), $e);
        }
    }

    /**
     * @Route("/changeMyPassword", name="changeMyPassword")
     * @Template()
     */
    public function changeMyPasswordAction()
    {
        $configurationManager = $this->get('zen.igroove.configuration');
        $configurationAD = $configurationManager->getActiveDirectoryConfiguration();
        $em = $this->getDoctrine()->getManager();

        if($this->get('session')->get('forcePasswordChangeUser', null) instanceof LdapUser) {
            $username = $this->get('session')->get('forcePasswordChangeUser')->getUsername();
            $force = true;
        } elseif($this->get('security.token_storage')->getToken() instanceof TokenInterface && $this->get('security.token_storage')->getToken()->getUsername() !== "") {
            $username = $this->get('security.token_storage')->getToken()->getUsername();
            $force = false;
        } else {
            throw $this->createAccessDeniedException();
        }

        $person = null;

        if(($student = $em->getRepository('ZenIgrooveBundle:Student')->findOneBy(['username' => $username])) instanceof Student) {
            $person = $student;
        } elseif (($teacher = $em->getRepository('ZenIgrooveBundle:Teacher')->findOneBy(['username' => $username])) instanceof Teacher) {
            $person = $teacher;
        }

        $google = false;
        if($person !== null && $person->getEmail() != "" && $person->getProviderSettings()['googleAppClientId'] !== "") {
            $google = true;
        }

        return array(
            'password' => $configurationAD['password'],
            'google' => $google,
            'force' => $force
        );
    }

    /**
     * @Route("/checkPassword", name="checkPassword")
     */
    public function checkPasswordAction()
    {
        $request = $this->get('Request');
        $password = $request->get('password', false);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        if($this->get('session')->get('forcePasswordChangeUser', null) instanceof LdapUser) {
            $username = $this->get('session')->get('forcePasswordChangeUser')->getUsername();
        } elseif($this->get('security.token_storage')->getToken() instanceof TokenInterface && $this->get('security.token_storage')->getToken()->getUsername() !== "") {
            $username = $this->get('security.token_storage')->getToken()->getUsername();
        } else {
            $response->setContent(json_encode(["success" => FALSE, 'error' => 'Access Denied']));
            return $response;
        }

        $ldap = $this->get('zen.igroove.microsoftLdap');
        $ldap->setParameters($this->get('zen.igroove.configuration')->getActiveDirectoryConfiguration());

        $result = $ldap->checkUserPassword($username, $password, true);
        $response->setContent(json_encode(['success' => $result]));

        return $response;
    }

    /**
     * @Route("/setNewPassword", name="setNewPassword")
     */
    public function setNewPasswordAction()
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');

        if($this->get('session')->get('forcePasswordChangeUser', null) instanceof LdapUser) {
            $username = $this->get('session')->get('forcePasswordChangeUser')->getUsername();
        } elseif($this->get('security.token_storage')->getToken() instanceof TokenInterface && $this->get('security.token_storage')->getToken()->getUsername() !== "") {
            $username = $this->get('security.token_storage')->getToken()->getUsername();
        } else {
            $response->setContent(json_encode(["success" => FALSE, 'error' => 'Access Denied']));
            return $response;
        }

        $request = $this->get('Request');
        $password = $request->get('password', false);
        $em = $this->getDoctrine()->getManager();
        $person = null;

        if(($student = $em->getRepository('ZenIgrooveBundle:Student')->findOneBy(['username' => $username])) instanceof Student) {
            $person = $student;
        } elseif (($teacher = $em->getRepository('ZenIgrooveBundle:Teacher')->findOneBy(['username' => $username])) instanceof Teacher) {
            $person = $teacher;
        }

        if($person === null) {
            $response->setContent(json_encode(["success" => FALSE, 'error' => 'Invalid user']));
            return $response;
        }

        $newPerson = clone $person;
        $newPerson->setStartingPassword($password);
        $personsAndGroups = $this->get('personsAndGroups');

        $targetService = $request->get('targetService', 'All');

        if($targetService == "Google" || $targetService == "All") {
            try {
                $personsAndGroups->resetPersonGoogleAppsUserPassword($newPerson);
            } catch (\Exception $e) {
                $response->setContent(json_encode(["success" => FALSE, 'error' => $e->getMessage()]));
                return $response;
            }
        }

        if($targetService == "Ad" || $targetService == "All") {
            try {
                $personsAndGroups->resetPersonLdapUserPassword($newPerson);
                $this->get('session')->set('forcePasswordChangeUser', null);
                $this->get('security.token_storage')->setToken(null);
                $this->get('request')->getSession()->invalidate();
            } catch (\Exception $e) {
                $response->setContent(json_encode(["success" => FALSE, 'error' => $e->getMessage()]));
                return $response;
            }
        }

        $response->setContent(json_encode(["success" => true, 'error' => '']));
        return $response;


//        $em = $this->getDoctrine()->getManager();
//
//        /** @var $adldapManager  */
//        $adldapManager = $this->get('zen.igroove.adldap');
//        $adldap = $adldapManager->getAdLdap();
//
//        $user = $adldap->user()->infoCollection($username, array('*'));
//        $setpassword = $adldap->user()->password($user->samaccountname, $password);
//
////        $student = $em->getRepository('ZenIgrooveBundle:Student')->findOneByUsername($username);
////        if ($student) {
////            $student->setStartingPassword('Reimpostata dallo studente in ' . $password);
////            $em->flush();
////        }
//
//        return array('error' => !$setpassword);
    }

    /**
     * @Route("/reset-student-password", name="resetStudentPassword")
     * @Secure(roles="ROLE_TEACHER")
     * @Template()
     */
    public function resetStudentPasswordAction()
    {
        return array();
    }

    /**
     * @Route("/check-student-exists", name="checkStudentExists")
     * @Secure(roles="ROLE_TEACHER")
     */
    public function checkStudentExistsAction()
    {
        $request = $this->get('Request');
        $studentUsername = $request->get('student', false);
        $hasGoogleApp = false;

        $em = $this->getDoctrine()->getManager();
        $ldapUser = $em->getRepository('ZenIgrooveBundle:LdapUser')->findOneByUsername($studentUsername);
        $student = $em->getRepository('ZenIgrooveBundle:Student')->findOneByUsername($studentUsername);

        $found = ($student AND $ldapUser);


        if ($student) {
            $hasGoogleApp = (strlen($student->getProvider()->getGoogleAppDomain() > 0)) ? true : false;
        }
        $response = new Response(json_encode(array('stato' => $found, 'hasGoogleApp' => $hasGoogleApp)));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }


    /**
     * @Route("/do-reset-student-password", name="doResetStudentPassword")
     * @Template()
     * @Secure(roles="ROLE_TEACHER")
     */
    public function doResetStudentPasswordAction()
    {
        $configurationManager = $this->container->get('zen.igroove.configuration');
        $request = $this->get('Request');
        $emailReset = $request->get('emailReset', false);
        $request = $this->get('Request');
        $studentUsername = $request->get('studentUsername', false);
        $hasGoogleApp = $request->get('hasGoogleApp', false);
        $em = $this->getDoctrine()->getManager();
        $ldapUser = $em->getRepository('ZenIgrooveBundle:LdapUser')->findOneByUsername($studentUsername);
        $student = $em->getRepository('ZenIgrooveBundle:Student')->findOneByUsername($studentUsername);
        $found = ($student AND $ldapUser);
        if ($found) {
            $newPassword = $ldapUser->setRandomPassword($configurationManager);
            $student->setStartingPassword($newPassword);
            $em->flush();
            $msg = array('command' => 'syncLDAPfromDB', 'parameters' => array());
            $client = $this->container->get('old_sound_rabbit_mq.ldap_service_producer');
            $client->publish(serialize($msg));
            if (($hasGoogleApp) AND ($emailReset)) {
                $msg = array(
                    'command' => 'resetPassword',
                    'parameters' => array('username' => $ldapUser->getEmail(), 'password' => $newPassword)
                );
                $client = $this->container->get('old_sound_rabbit_mq.google_app_service_producer');
                $client->publish(serialize($msg));
            }
            return array(
                'studentUsername' => $studentUsername,
                'password' => $newPassword,
                'emailReset' => $emailReset,
                'email' => $ldapUser->getEmail()
            );
        }
        return $this->redirect(
            $this->generateUrl(
                'resetStudentPassword'
            )
        );

    }

}

