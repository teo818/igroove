<?php

namespace Zen\IgrooveBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

abstract class AbstractController extends Controller {

    protected function getCurrentUserUsername() {
        static $currentUserName;

        if(!isset($currentUserName)) {
            $currentUserName = "";
            if($this->get('security.token_storage')->getToken() instanceof TokenInterface) {
                $currentUserName = $this->get('security.token_storage')->getToken()->getUsername();
            }
        }

        return $currentUserName;
    }

    protected function logAction($action, $info) {
        $ip = $_SERVER['REMOTE_ADDR'];
        $this->get('monolog.logger.actions')->notice("[{$action}] ".$this->getCurrentUserUsername()."({$ip}) {$info}", [
            'action' => $action,
            'username' => $this->getCurrentUserUsername(),
            'ip' => $ip
        ]);
    }
}