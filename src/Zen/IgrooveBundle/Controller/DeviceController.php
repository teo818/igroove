<?php

namespace Zen\IgrooveBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Zen\IgrooveBundle\Entity\Device;
use Zen\IgrooveBundle\Entity\MikrotikList;
use Zen\IgrooveBundle\Form\DeviceType;
use Zen\IgrooveBundle\Manager\ConfigurationManager;

/**
 * Device controller.
 *
 */
class DeviceController extends Controller
{
    /**
     * Lists all Device entities.
     *
     * @Route("/device", name="device")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function indexAction()
    {
        $request = $this->get('Request');
        $filter=array();
        $queryString = $request->get('queryString', false);
        if ($queryString) {
            $filter['queryString']=$queryString;
        }
        $onlyActive = $request->get('onlyActive', 'off') == 'on' ? true : false;
        $filter['onlyActive']=$onlyActive;
        $mikrotikListId = $request->get('mikrotikListId', false);
        if (($mikrotikListId!==false) and ($mikrotikListId!=-1)){
            $filter['mikrotikListId']=$mikrotikListId;
        }

        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('ZenIgrooveBundle:Device')->getQueryFromFilter($filter);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1),25
        );

        $mikrotikLists = $em->getRepository('ZenIgrooveBundle:MikrotikList')->findBy([], ['nome' => 'ASC']);

        $request->getSession()->set("macControllerLastQuery",$request->getQueryString());

        return array(
            'pagination' => $pagination,
            'filter' => $filter,
            'mikrotikLists'=>$mikrotikLists
        );
    }

    /**
     * Displays a form to create a new Device entity.
     *
     * @Route("/device/new", name="device_new")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new Device();
        $entity->setActive(true);

        $qs = $this->getLastQueryForRedirect();

        if(!empty($qs) && isset($qs['mikrotikListId']) && $qs['mikrotikListId'] != "") {
            $mikrotikList = $em->getRepository('ZenIgrooveBundle:MikrotikList')->find($qs['mikrotikListId']);
            if($mikrotikList instanceof MikrotikList){
                $entity->setMikrotikList($mikrotikList);
            }
        }

        $form = $this->createForm(new DeviceType($em), $entity);

        $mikrotikPoolList = $em->getRepository('ZenIgrooveBundle:MikrotikPool')->findBy([],['nome' => 'ASC']);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'mikrotikPoolList' => $mikrotikPoolList,
            'redirect_qs' => $qs
        );
    }

    /**
     * Creates a new Device entity.
     *
     * @Route("/device/create", name="device_create")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("post")
     * @Template("ZenIgrooveBundle:Device:new.html.twig")
     */
    public function createAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new Device();
        $request = $this->get('Request');
        $form = $this->createForm(new DeviceType($em), $entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $client = $this->container->get('old_sound_rabbit_mq.mikrotik_service_producer');
            $msg = array('command' => 'addMacToHospot', 'parameters' => array());
            $client->publish(serialize($msg));
            $msg = array('command' => 'addIpReservation', 'parameters' => array());
            $client->publish(serialize($msg));
            $msg = array('command' => 'addIpList', 'parameters' => array());
            $client->publish(serialize($msg));

            return $this->redirect($this->generateUrl('device', $this->getLastQueryForRedirect()));
        }

        $mikrotikPoolList = $em->getRepository('ZenIgrooveBundle:MikrotikPool')->findBy([],['nome' => 'ASC']);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'mikrotikPoolList' => $mikrotikPoolList,
            'redirect_qs' => $this->getLastQueryForRedirect()
        );
    }

    /**
     * Displays a form to edit an existing Device entity.
     *
     *
     * @Route("/device/{id}/edit", name="device_edit")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ZenIgrooveBundle:Device')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Device entity.');
        }

        $mikrotikPoolList = $em->getRepository('ZenIgrooveBundle:MikrotikPool')->findBy([],['nome' => 'ASC']);

        $editForm = $this->createForm(new DeviceType($em), $entity);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'mikrotikPoolList' => $mikrotikPoolList,
            'redirect_qs' => $this->getLastQueryForRedirect()
        );
    }

    /**
     * Edits an existing Device entity.
     *
     * @Route("/device/{id}/update", name="device_update")
     * @Method("post")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("ZenIgrooveBundle:Device:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ZenIgrooveBundle:Device')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Device entity.');
        }

        $originalIps = clone $entity->getIps();
        $editForm = $this->createForm(new DeviceType($em), $entity);

        /**
         * @var Request
         */
        $request = $this->get('Request');
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            foreach ($originalIps as $deviceIp) {
                if (false === $entity->getIps()->contains($deviceIp)) {
                    $em->remove($deviceIp);
                }
            }

            $em->persist($entity);
            $em->flush();

            $client = $this->container->get('old_sound_rabbit_mq.mikrotik_service_producer');
            $msg = array('command' => 'addMacToHospot', 'parameters' => array());
            $client->publish(serialize($msg));
            $msg = array('command' => 'addIpReservation', 'parameters' => array());
            $client->publish(serialize($msg));
            $msg = array('command' => 'addIpList', 'parameters' => array());
            $client->publish(serialize($msg));

            return $this->redirect($this->generateUrl('device', $this->getLastQueryForRedirect()));
        }

        $mikrotikPoolList = $em->getRepository('ZenIgrooveBundle:MikrotikPool')->findBy([],['nome' => 'ASC']);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'mikrotikPoolList' => $mikrotikPoolList,
            'redirect_qs' => $this->getLastQueryForRedirect()
        );
    }

    /**
     * Deletes a Device entity.
     *
     * @Route("/device/{id}/delete", name="device_delete")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ZenIgrooveBundle:Device')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Guest entity.');
        }

        foreach ($entity->getIps() as $deviceIp) {
            $em->remove($deviceIp);
        }

        $em->remove($entity);
        $em->flush();

        $client = $this->container->get('old_sound_rabbit_mq.mikrotik_service_producer');
        $msg = array('command' => 'addMacToHospot', 'parameters' => array());
        $client->publish(serialize($msg));
        $msg = array('command' => 'addIpReservation', 'parameters' => array());
        $client->publish(serialize($msg));
        $msg = array('command' => 'addIpList', 'parameters' => array());
        $client->publish(serialize($msg));

        return $this->redirect($this->generateUrl('device', $this->getLastQueryForRedirect()));
    }

    /**
     * Last query searched in list (to go back to the last search)
     *
     * @return array
     */
    protected function getLastQueryForRedirect() {
        $qs = [];
        if($this->get('Request')->getSession()->get("macControllerLastQuery","") != "") {
            parse_str($this->get('Request')->getSession()->get("macControllerLastQuery"), $qs);
        }

        return $qs;
    }
}
