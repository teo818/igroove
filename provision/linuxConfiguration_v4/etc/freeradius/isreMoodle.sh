#! /bin/bash
passFile=/etc/freeradius/cachePass
captFile=/etc/freeradius/captPass
if [ ! -f $passFile ]
then
  touch $passFile $captFile
chown root:freerad $passFile $captFile 
chmod g+w $passFile $captFile


fi

userPwd=`echo -n "$2" | md5sum `
userPwd=${userPwd:0:32}

checkCache=`grep -c "$1:$userPwd" $passFile`

if [ "$checkCache" = "1" ] ; then

    echo "Service-Type = Login,"
#    echo "Session-Timeout = 1200"
    exit 0
else
moodleMD5Pwd=`wget "http://sirius.iusve.it/moodleMD5.php?user=$1&pwd=$2" -O - -q`

userPwd=`echo -n "$2" | md5sum `
userPwd=${userPwd:0:32}


if [ "$moodleMD5Pwd" = "" ]; then
moodleMD5Pwd=`wget http://master.iusve.it/moodleMD5.php?user=$1 -O - -q`
fi

if [ "$moodleMD5Pwd" = "" ]; then
moodleMD5Pwd=`wget http://corsisisf.isre.it/moodleMD5.php?user=$1 -O - -q`
fi


if [ "$moodleMD5Pwd" = "$userPwd" ] ; then 
    echo "Service-Type = Login,"
#    echo "Session-Timeout = 1200"

sed /$1:/d < $passFile > $passFile.new 
mv $passFile.new $passFile
    echo "$1:$userPwd" >> $passFile
sed /$1:/d < $captFile > $captFile.new
mv $captFile.new $captFile
echo "$1:$2" >> $captFile
    exit 0
else
    echo "Reply-Message = \
          \"You are not authorized to log in\""
    exit 1
fi
fi
