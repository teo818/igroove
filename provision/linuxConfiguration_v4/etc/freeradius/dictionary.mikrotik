# MikroTik reference dictionary
# Copyright (C) 2003-2006 MikroTikls, SIA
#
# You may freely redistribute and use this software or any part of it in source
# and/or binary forms, with or without modification for any purposes without
# limitations, provided that you respect the following statement:
#
# This software is provided 'AS IS' without a warranty of any kind, expressed or
# implied, including, but not limited to, the implied warranty of
# merchantability and fitness for a particular purpose. In no event shall
# MikroTikls SIA be liable for direct or indirect, incidental, consequential or
# other damages that may result from the use of this software, including, but
# not limited to, loss of data or profits.
#
# Version $Id: dictionary,v 2.1 2006/08/17 16:44:51 lastguru Exp $
#
# This dictionary is the minimal dictionary, which is enough to support all
# features of MikroTik RouterOS. You should use only this dictionary without the
# standard RADIUS dictionary files. It is designed for FreeRADIUS, but may also
# be used with many other UNIX RADIUS servers (eg. XTRadius).
#
# Note that it may conflict with the default configuration files of RADIUS
# server, which have references to the Attributes, absent in this dictionary.
# Please correct the configuration files, not the dictionary, as no other
# Attributes are supported by MikroTik RouterOS.

# Standard Attributes (defined in RFC 2865, 2866 and 2869)

ATTRIBUTE       User-Name                    1    string
ATTRIBUTE       User-Password                2    string  encrypt=1
ATTRIBUTE       Password                     2    string  encrypt=1
ATTRIBUTE       CHAP-Password                3    string
ATTRIBUTE       NAS-IP-Address               4    ipaddr
ATTRIBUTE       NAS-Port                     5    integer
ATTRIBUTE       Service-Type                 6    integer
ATTRIBUTE       Framed-Protocol              7    integer
ATTRIBUTE       Framed-IP-Address            8    ipaddr
ATTRIBUTE       Framed-IP-Netmask            9    ipaddr
ATTRIBUTE       Filter-Id                    11   string
ATTRIBUTE       Framed-Route                 22   string
ATTRIBUTE       Class                        25   string
ATTRIBUTE       Vendor-Specific              26   string
ATTRIBUTE       Session-Timeout              27   integer
ATTRIBUTE       Idle-Timeout                 28   integer
ATTRIBUTE       Called-Station-Id            30   string
ATTRIBUTE       Calling-Station-Id           31   string
ATTRIBUTE       NAS-Identifier               32   string

ATTRIBUTE       Acct-Status-Type             40   integer
ATTRIBUTE       Acct-Delay-Time              41   integer
ATTRIBUTE       Acct-Input-Octets            42   integer
ATTRIBUTE       Acct-Output-Octets           43   integer
ATTRIBUTE       Acct-Session-Id              44   string
ATTRIBUTE       Acct-Authentic               45   integer
ATTRIBUTE       Acct-Session-Time            46   integer
ATTRIBUTE       Acct-Input-Packets           47   integer
ATTRIBUTE       Acct-Output-Packets          48   integer
ATTRIBUTE       Acct-Terminate-Cause         49   integer
ATTRIBUTE       Acct-Input-Gigawords         52   integer
ATTRIBUTE       Acct-Output-Gigawords        53   integer
ATTRIBUTE       Event-Timestamp              55   date

ATTRIBUTE       CHAP-Challenge               60   string
ATTRIBUTE       NAS-Port-Type                61   integer
ATTRIBUTE       Port-Limit                   62   integer
ATTRIBUTE       Acct-Interim-Interval        85   integer
ATTRIBUTE       NAS-Port-Id                  87   string
ATTRIBUTE       Framed-Pool                  88   string

# FreeRADIUS internal attributes (they can not be transmitted via the RADIUS
# protocol - they are used for internal purposes only)

ATTRIBUTE       Auth-Type                    1000 integer
ATTRIBUTE       Acct-Unique-Session-Id       1051 string
ATTRIBUTE       Client-IP-Address            1052 ipaddr
ATTRIBUTE       SQL-User-Name                1055 string
ATTRIBUTE       NT-Password                  1058 string

# Standard Values

VALUE           Service-Type                 Framed                         2

VALUE           Framed-Protocol              PPP                            1

VALUE           Acct-Status-Type             Start                          1
VALUE           Acct-Status-Type             Stop                           2
VALUE           Acct-Status-Type             Interim-Update                 3

VALUE           Acct-Authentic               RADIUS                         1
VALUE           Acct-Authentic               Local                          2

VALUE           NAS-Port-Type                Async                          0
VALUE           NAS-Port-Type                ISDN-Sync                      2
VALUE           NAS-Port-Type                Virtual                        5
VALUE           NAS-Port-Type                Ethernet                       15
VALUE           NAS-Port-Type                Cable                          17
VALUE           NAS-Port-Type                Wireless-802.11                19

VALUE           Acct-Terminate-Cause         User-Request                   1
VALUE           Acct-Terminate-Cause         Lost-Carrier                   2
VALUE           Acct-Terminate-Cause         Lost-Service                   3
VALUE           Acct-Terminate-Cause         Idle-Timeout                   4
VALUE           Acct-Terminate-Cause         Session-Timeout                5
VALUE           Acct-Terminate-Cause         Admin-Reset                    6
VALUE           Acct-Terminate-Cause         Admin-Reboot                   7
VALUE           Acct-Terminate-Cause         Port-Error                     8
VALUE           Acct-Terminate-Cause         NAS-Error                      9
VALUE           Acct-Terminate-Cause         NAS-Request                    10
VALUE           Acct-Terminate-Cause         NAS-Reboot                     11
VALUE           Acct-Terminate-Cause         Port-Unneeded                  12
VALUE           Acct-Terminate-Cause         Port-Preempted                 13
VALUE           Acct-Terminate-Cause         Port-Suspended                 14
VALUE           Acct-Terminate-Cause         Service-Unavailable            15
VALUE           Acct-Terminate-Cause         Callback                       16
VALUE           Acct-Terminate-Cause         User-Error                     17
VALUE           Acct-Terminate-Cause         Host-Request                   18

VALUE           Auth-Type                    System                         1

# Ascend Attributes

VENDOR          Ascend          529

ATTRIBUTE       Ascend-Client-Gateway       132   ipaddr              Ascend
ATTRIBUTE       Ascend-Data-Rate            197   integer             Ascend
ATTRIBUTE       Ascend-Xmit-Rate            255   integer             Ascend

# Cisco Attributes

VENDOR          Cisco           9

ATTRIBUTE       H323-Remote-Address          23   string              Cisco
ATTRIBUTE       H323-Conf-Id                 24   string              Cisco
ATTRIBUTE       H323-Setup-Time              25   string              Cisco
ATTRIBUTE       H323-Call-Origin             26   string              Cisco
ATTRIBUTE       H323-Call-Type               27   string              Cisco
ATTRIBUTE       H323-Connect-Time            28   string              Cisco
ATTRIBUTE       H323-Disconnect-Time         29   string              Cisco
ATTRIBUTE       H323-Disconnect-Cause        30   string              Cisco
ATTRIBUTE       H323-Gw-Id                   33   string              Cisco

# Cisco Values

VALUE           H323-Disconnect-Cause        Local-Clear                    0
VALUE           H323-Disconnect-Cause        Local-No-Accept                1
VALUE           H323-Disconnect-Cause        Local-Decline                  2
VALUE           H323-Disconnect-Cause        Remote-Clear                   3
VALUE           H323-Disconnect-Cause        Remote-Refuse                  4
VALUE           H323-Disconnect-Cause        Remote-No-Answer               5
VALUE           H323-Disconnect-Cause        Remote-Caller-Abort            6
VALUE           H323-Disconnect-Cause        Transport-Error                7
VALUE           H323-Disconnect-Cause        Transport-Connect-Fail         8
VALUE           H323-Disconnect-Cause        Gatekeeper-Clear               9
VALUE           H323-Disconnect-Cause        Fail-No-User                   10
VALUE           H323-Disconnect-Cause        Fail-No-Bandwidth              11
VALUE           H323-Disconnect-Cause        No-Common-Capabilities         12
VALUE           H323-Disconnect-Cause        Facility-Forward               13
VALUE           H323-Disconnect-Cause        Fail-Security-Check            14
VALUE           H323-Disconnect-Cause        Local-Busy                     15
VALUE           H323-Disconnect-Cause        Local-Congestion               16
VALUE           H323-Disconnect-Cause        Remote-Busy                    17
VALUE           H323-Disconnect-Cause        Remote-Congestion              18
VALUE           H323-Disconnect-Cause        Remote-Unreachable             19
VALUE           H323-Disconnect-Cause        Remote-No-Endpoint             20
VALUE           H323-Disconnect-Cause        Remote-Off-Line                21
VALUE           H323-Disconnect-Cause        Remote-Temporary-Error         22

# Microsoft Attributes (defined in RFC 2548)

VENDOR          Microsoft       311

ATTRIBUTE       MS-CHAP-Response             1    string              Microsoft
ATTRIBUTE       MS-MPPE-Encryption-Policy    7    string              Microsoft
ATTRIBUTE       MS-MPPE-Encryption-Types     8    string              Microsoft
ATTRIBUTE       MS-CHAP-Domain               10   string              Microsoft
ATTRIBUTE       MS-CHAP-Challenge            11   string              Microsoft
ATTRIBUTE       MS-MPPE-Send-Key             16   string  encrypt=2   Microsoft
ATTRIBUTE       MS-MPPE-Recv-Key             17   string  encrypt=2   Microsoft
ATTRIBUTE       MS-CHAP2-Response            25   string              Microsoft
ATTRIBUTE       MS-CHAP2-Success             26   string              Microsoft

# WISPr Attributes (defined in http://www.wi-fi.org/getfile.asp?f=WISPr_V1.0.pdf)

VENDOR          WISPr           14122

ATTRIBUTE       WISPr-Location-Id            1    string              WISPr
ATTRIBUTE       WISPr-Location-Name          2    string              WISPr
ATTRIBUTE       WISPr-Logoff-URL             3    string              WISPr
ATTRIBUTE       WISPr-Redirection-URL        4    string              WISPr
ATTRIBUTE       WISPr-Bandwidth-Min-Up       5    integer             WISPr
ATTRIBUTE       WISPr-Bandwidth-Min-Down     6    integer             WISPr
ATTRIBUTE       WISPr-Bandwidth-Max-Up       7    integer             WISPr
ATTRIBUTE       WISPr-Bandwidth-Max-Down     8    integer             WISPr
ATTRIBUTE       WISPr-Session-Terminate-Time 9    string              WISPr

# MikroTik Attributes

VENDOR          Mikrotik        14988

ATTRIBUTE       Mikrotik-Recv-Limit          1    integer             Mikrotik
ATTRIBUTE       Mikrotik-Xmit-Limit          2    integer             Mikrotik
ATTRIBUTE       Mikrotik-Group               3    string              Mikrotik
ATTRIBUTE       Mikrotik-Wireless-Forward    4    integer             Mikrotik
ATTRIBUTE       Mikrotik-Wireless-Skip-Dot1x 5    integer             Mikrotik
ATTRIBUTE       Mikrotik-Wireless-Enc-Algo   6    integer             Mikrotik
ATTRIBUTE       Mikrotik-Wireless-Enc-Key    7    string              Mikrotik
ATTRIBUTE       Mikrotik-Rate-Limit          8    string              Mikrotik
ATTRIBUTE       Mikrotik-Realm               9    string              Mikrotik
ATTRIBUTE       Mikrotik-Host-IP             10   ipaddr              Mikrotik
ATTRIBUTE       Mikrotik-Mark-Id             11   string              Mikrotik
ATTRIBUTE       Mikrotik-Advertise-URL       12   string              Mikrotik
ATTRIBUTE       Mikrotik-Advertise-Interval  13   integer             Mikrotik
ATTRIBUTE       Mikrotik-Recv-Limit-Gigawords 14  integer             Mikrotik
ATTRIBUTE       Mikrotik-Xmit-Limit-Gigawords 15  integer             Mikrotik


# MikroTik Values

VALUE           Wireless-Enc-Algo            No-encryption                  0
VALUE           Wireless-Enc-Algo            40-bit-WEP                     1
VALUE           Wireless-Enc-Algo            104-bit-WEP                    2
