#!/usr/bin/env bash

NB_TASKS=1
SYMFONY_ENV="prod"

TEXT[0]="app/console rabbitmq:consumer ldap_service"
TEXT[1]="app/console rabbitmq:consumer mikrotik_service"
TEXT[1]="app/console rabbitmq:consumer version_service"
TEXT[2]="app/console rabbitmq:consumer google_apps_service"

for text in "${TEXT[@]}"
do

NB_LAUNCHED=$(ps ax | grep "$text" | grep -v grep | wc -l)

TASK="/usr/bin/env php ${text} --env=${SYMFONY_ENV}"

for (( i=${NB_LAUNCHED}; i<${NB_TASKS}; i++ ))
do
  echo "$(date +%c) - Launching a new consumer: $TASK"
  nohup $TASK &
done

done