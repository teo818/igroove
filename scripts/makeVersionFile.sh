#!/usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
FILE=$DIR/../src/Zen/IgrooveBundle/Resources/config/version/versionInstalled.yml
FILE3=$DIR/../src/Zen/IgrooveBundle/Resources/config/version/version.yml

version=`git describe --abbrev=0 --tags`
buildFuture=`git rev-list --count master`
build=`expr $buildFuture - 1`

echo "# This file is generated using script/makeVersionFile.sh" > $FILE
echo "parameters:" >> $FILE
echo "    versionInstalled: $version" >> $FILE
echo "    buildInstalled: $build" >> $FILE


echo "# This file is generated using script/makeVersionFile.sh" > $FILE3
echo "parameters:" >> $FILE3
echo "    versionAvailable: $version" >> $FILE3
echo "    buildAvailable: $build" >> $FILE3