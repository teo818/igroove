

strSheet = "C:\Documents and Settings\Administrator\Desktop\studentidb_def.xlsx"

Set objExcel = CreateObject("Excel.Application")
Set objSpread = objExcel.Workbooks.Open(strSheet)
intRow = 2

'Do Until objExcel.Cells(intRow,1).Value = ""
   strFirst = Trim(objExcel.Cells(intRow, 1).Value)
   strLast = Trim(objExcel.Cells(intRow, 2).Value)
   strSam = Trim(objExcel.Cells(intRow, 3).Value)
   strPWD= Trim(objExcel.Cells(intRow, 4).Value)
   strCF_ID = Trim(objExcel.Cells(intRow, 5).Value)

  ' ' wscript.echo strFirst & " - " & strLast
   call checkUserByUsername(strSam,strLast,strFirst,strCF_ID,strPWD)
    intRow = intRow + 1
'Loop
objExcel.Quit

wscript.echo "Finito!"

sub checkUserByUsername(strSam,strLast,strFirst,strCF_ID,strPWD)
	ON ERROR RESUME NEXT

Set objRootDSE = GetObject("LDAP://RootDSE")
strDNSDomain = objRootDSE.Get("DefaultNamingContext")



	SET cn = CREATEOBJECT("ADODB.Connection")
	SET cmd = CREATEOBJECT("ADODB.Command")
	SET rs = CREATEOBJECT("ADODB.Recordset")

	cn.open "Provider=ADsDSOObject;"

	cmd.activeconnection=cn
	cmd.commandtext="SELECT ADsPath FROM 'LDAP://" & strDNSDomain & _
			 "' WHERE sAMAccountName = '" & strSam & "'"

	SET rs = cmd.EXECUTE

	strCN=strFirst & " " & strLast

	IF err<>0 THEN
		wscript.echo "Error connecting to Active Directory Database:" & err.description
	ELSE
		IF NOT rs.BOF AND NOT rs.EOF THEN
        rs.MoveFirst

     	Set objUser = GetObject(rs(0))
			objUser.givenName = strFirst
            objUser.sn = strLast
            objUser.description = "Creato da igroove"
            objUser.department = strCF_ID
            objUser.SetPassword = strPWD
            objUser.SetInfo
            objUser.AccountDisabled = False
            objUser.SetInfo
		ELSE

			set objRootDSE = GetObject("LDAP://cn=users, dc=test, dc=network")
            Set objUser = objRootDSE.Create("user","cn=" & strCN)
            objUser.sAMAccountName = strSam
            objUser.givenName = strFirst
            objUser.sn = strLast
            objUser.description = "Creato da igroove"
            objUser.department = strCF_ID
            objUser.SetPassword = strPWD
            objUser.SetInfo
            objUser.AccountDisabled = False
            objUser.SetInfo
		END IF
	END IF
	cn.close
END sub