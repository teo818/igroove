' Option Explicit
' on error resume next 
Dim objRootDSE, strDomain, objGroup, objUser, strGroupMembers
Dim folder, webdavfolder, cmd, folderHome, objShortcutLnk
Dim objFSO, objFolder, strDirectory, objShell, objTextFile 
Set objRootDSE = GetObject("LDAP://RootDSE")
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objShell = CreateObject("Wscript.Shell") 
folder="\\srvc\webdav\"
folderHome="\\srvc\home\"




gDN = SearchGroup("studentiIpad")

GetMembers(gDN)
Function GetMembers(gDN)
    Set objGroup = GetObject("LDAP://" & gDN)
    objGroup.GetInfo
    arrMemberOf = objGroup.GetEx("member")
    
    For Each strMember in arrMemberOf
        Set objMember = GetObject("LDAP://" & strMember)
        ObjDisp = objMember.Name
        oDL = Len(ObjDisp) - 3
        ObjDisp = Right(ObjDisp,oDL)
        ObjCatArray = Split(objMember.objectCategory,",")
        oType = ObjCatArray(0)
        oTL = Len(oType) - 3
        oType = Right(oType,oTL)
        ' WScript.Echo "Member:" & ObjDisp & Space(20-Len(ObjDIsp)) &" Type:" & oType
If oType = "Person" Then

webDavFolder=folderHome & objMember.sAMAccountName
If not objFSO.FolderExists(webDavFolder) Then
Set objFolder = objFSO.CreateFolder(webDavFolder)
cmd = webdavfolder & " /G " & objMember.sAMAccountName & ":F ""Domain Admins"":F /Y /T"
objShell.Run ("xcacls.exe " & cmd )
end if
webDavFolder=folder & objMember.sAMAccountName
If not objFSO.FolderExists(webDavFolder) Then
Set objFolder = objFSO.CreateFolder(webDavFolder)
end if
cmd = webdavfolder & " /G " & objMember.sAMAccountName & ":F ""Domain Admins"":F /Y /T"
objShell.Run ("xcacls.exe " & cmd )
Set objShortcutLnk = objShell.CreateShortcut(folderHome & objMember.sAMAccountName & "\webDav-windows.lnk")
objShortcutLnk.TargetPath = webdavfolder
objShortcutLnk.Save
Set objTextFile = objFSO.CreateTextFile(folderHome & objMember.sAMAccountName & "\webDav-macOsX.url", 2 , True)
objTextFile.WriteLine("[InternetShortcut]")
objTextFile.WriteLine("URL=smb://srvc/webdav/" & objMember.sAMAccountName)
objTextFile.WriteLine("")
objTextFile.Close
end if

        If oType = "Group" Then
            GetMembers(strMember)
        End If
        Set objMember = Nothing
    Next
End Function

Public Function SearchGroup(ByVal vSAN)
    Dim oRootDSE, oConnection, oCommand, oRecordSet
    Set oRootDSE = GetObject("LDAP://rootDSE")
    Set oConnection = CreateObject("ADODB.Connection")
    oConnection.Open "Provider=ADsDSOObject;"
    Set oCommand = CreateObject("ADODB.Command")
    oCommand.ActiveConnection = oConnection
    oCommand.CommandText = "<LDAP://" & oRootDSE.get("defaultNamingContext") & _
        ">;(&(objectCategory=Group)(samAccountName=" & vSAN & "));distinguishedName;subtree"
    Set oRecordSet = oCommand.Execute
    On Error Resume Next
    SearchGroup = oRecordSet.Fields("distinguishedName")
    On Error GoTo 0
    oConnection.Close
    Set oRecordSet = Nothing
    Set oCommand = Nothing
    Set oConnection = Nothing
    Set oRootDSE = Nothing
End Function
