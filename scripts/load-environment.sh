#!/bin/sh

if [ "$#" -ne 1 ]; then
    echo "load-senvironment.sh (nome)"
fi
nome=$1

echo [1/4] Caricamento igroove.yml
cp ../configurazioni/$nome/igroove.yml app/config/
cp ../configurazioni/$nome/igroove.sql .

echo [2/4] Caricamento igroove.sql
vagrant ssh-config > vagrant-ssh;
ssh -F vagrant-ssh default "php /vagrant/app/console doctrine:database:drop --force;php /vagrant/app/console doctrine:database:create";
ssh -F vagrant-ssh default "mysql -u root -proot igroove < /vagrant/igroove.sql;exit"
ssh -F vagrant-ssh default "php /vagrant/app/console doctrine:schema:update --force";

echo [3/4] Rimozione file di supporto
rm vagrant-ssh
rm igroove.sql

echo [4/4] Rimozione logs e cache
rm -rf app/cache/*
rm -rf app/logs/*

